<!DOCTYPE html>
<html lang="nn" xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">  

	<meta name="mag-version" content="magv-7.1">  
 
    <?php  
		$url =  "//{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
		$url = str_replace('index.php', '',$url);
	?>
	 
	<base href="<?php echo $url;?>" />
	<meta property="og:url"  content="<?php echo $url;?>" />
	<meta property="og:image"  content="<?php echo $image;?>" />
	<meta property="og:title"  content="<?php echo $title;?>" />
	<meta property="og:description"  content="<?php echo $description;?>" /> 
	
	<!-- PAGE TITLE -->
	<title>Finansfokus</title>

	<!-- style linking -->

	<!-- style font -->		 
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css">
	
	<!-- style general -->		
	<link rel="stylesheet" href="<?=$url;?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=$url;?>css/swiper.min.css">
	<link rel="stylesheet" href="<?=$url;?>css/jquery.fancybox.min.css">
	<link rel="stylesheet" href="<?=$url;?>css/theme.css">
	<!-- CSS LINKS -->

	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet">
 	

 	<style type="text/css">  
 		.largefont{

 			.articles-categorybox, .overlay-bg-caption1{
	 			font-size: 102%;
	    		line-height: 1.3;
	 		}
	 		p, .articles-subtitle, .innholdList .pageNoBox, .innholdList .navTextBox, .fakt-box, .heading-incress {
	 			font-size: 120% ;
	    		line-height: 1.3 ;
	 		}
	 		.images-caption, .articles-textfotobox, .author, .about-heading{
	 			font-size: 110%;
	    		line-height: 1.3;
	 		}
	 		.intro-text, .quate-box {
			    font-size: 160%;
			    line-height: 1.3 ;
			}

			ul li, ol li, .kolofan-section h6, table > tbody > tr > td, .innhold-title, .con-block h4 {
				font-size: 110%;
	    		line-height: 1.3;
			} 
			.kolofan-section h5{
				font-size: 130%;
	    		line-height: 1.3;
			} 
			.innhold-img-navText{
				font-size: 110% ;
	    		line-height: 1.3 ;
			} 
		}
 	</style>
</head>


<!-- PAGE BODY -->
<body> 
	<div id="header" class="show-header header-upgrade">
		<div class="col-sm-12 col-lg-12">
			<div class="row">
				<nav id="menu" class="navbar main-nav">
					<div class="wrapper">
						<div class="navbar-header"> 
							<a id="ghar" class="scroll navbar-brand" page-number="0" href="javascript:;">
								<img class="img-responsive" src="images/elements/header-logo.png" alt="">
								<span class="date font-FlamaSemiconB">02 2024</span>
							</a>
						</div> 
					</div>
				</nav> 
				<br>
			</div>
		</div> 
	</div>

	<!-- nav-sidebar -->
	<div id="nav-sidebar">
		<img class="nav-slider-close" src="images/innhold/close.png">

		<div class="row slider-container"> 
			<div class="col-md-4 slider-thumb-container">
				<span class="indexLinktoPage" page-number="0">
					<img src="images/innhold/innhold-thumb1.jpg">
					<h3>
						Cover
					</h3>
				</span>
			</div>

			<div class="col-md-4 slider-thumb-container">
				<span class="indexLinktoPage" page-number="">
					<img src="images/innhold/innhold-thumb2.jpg">
					<h3>
						Innhold / Leder
					</h3>
				</span>
			</div>  
		</div>

		<hr class="divider-cover-arti">

		<h2>Artikler</h2>

		<div class="row slider-container">
			<div class="col-md-4 slider-thumb-container">
				<span class="indexLinktoPage" page-number="3">
					<img src="images/innhold/innhold-thumb3.jpg" style="border: 1px solid #ddd;">
					<h3>
						<small class="d-block">Kronikk</small>
						Ble avslagsplikten til en avviksfrykt?
					</h3>
				</span>
			</div>

			<div class="col-md-4 slider-thumb-container">
				<span class="indexLinktoPage" page-number="3">
					<a href="#forbundsleder">
						<img src="images/innhold/innhold-thumb4.jpg">
						<h3>
							<small class="d-block">Forbundsleder</small>
							Skal vi jobbe lenger, eller?
						</h3>
					</a>
				</span>
			</div>

			<div class="col-md-4 slider-thumb-container">
				<span class="indexLinktoPage" page-number="4">
					 
					<img src="images/innhold/innhold-thumb5.jpg">
					<h3>
						<small class="d-block">Kommentar</small>
						Enkelt å bytte bank?...Njanei…
					</h3> 
				</span>
			</div>

			<div class="col-md-4 slider-thumb-container">
				<span class="indexLinktoPage" page-number="6">
					<img src="images/innhold/innhold-thumb6.jpg">
					<h3>
						<small class="d-block">Siden sist</small>
						NY KONSERNSJEF I SNN
					</h3>
				</span>
			</div>

			<div class="col-md-4 slider-thumb-container">
				<span class="indexLinktoPage" page-number="8">
					<img src="images/innhold/innhold-thumb7.jpg">
					<h3>
						<small class="d-block">Lønnsomme banker</small>
						NORDENS MEST LØNNSOMME BANK
					</h3>
				</span>
			</div>

			<div class="col-md-4 slider-thumb-container">
				<span class="indexLinktoPage" page-number="9">
					<img src="images/innhold/innhold-thumb8.jpg">
					<h3>
						<small class="d-block">Min arbeidsplass</small>
						50 år i Nordea
					</h3>
				</span>
			</div>

			<div class="col-md-4 slider-thumb-container">
				<span class="indexLinktoPage" page-number="10">
					<img src="images/innhold/innhold-thumb9.jpg">
					<h3>
						<small class="d-block">Nyheter</small>
						Kontoret som destinasjon
					</h3>
				</span>
			</div>

			<div class="col-md-4 slider-thumb-container">
				<span class="indexLinktoPage" page-number="11">
					<img src="images/innhold/innhold-thumb10.jpg">
					<h3>
						<small class="d-block">Portrett</small>
						Kritisk KI-optimist
					</h3>
				</span>
			</div>

			<div class="col-md-4 slider-thumb-container">
				<span class="indexLinktoPage" page-number="12">
					<img src="images/innhold/innhold-thumb11.jpg">
					<h3>
						<small class="d-block">Tech</small>
						Alle må åpne seg
					</h3>
				</span>
			</div>

			<div class="col-md-4 slider-thumb-container">
				<span class="indexLinktoPage" page-number="13">
					<img src="images/innhold/innhold-thumb12.jpg">
					<h3>
						<small class="d-block">Tech</small>
						LÆRER VANSKELIGE SAMTALER MED VR
					</h3>
				</span>
			</div>

			<div class="col-md-4 slider-thumb-container">
				<span class="indexLinktoPage" page-number="14">
					<img src="images/innhold/innhold-thumb13.jpg">
					<h3>
						<small class="d-block">Tech-Nytt</small>
						Alinea – investering for kvinnelige Gen Zetters
					</h3>
				</span>
			</div>

			<div class="col-md-4 slider-thumb-container">
				<span class="indexLinktoPage" page-number="15">
					<img src="images/innhold/innhold-thumb14.jpg">
					<h3>
						<small class="d-block">MEDLEM</small>
						52 år i samme bank
					</h3>
				</span>
			</div>

			<div class="col-md-4 slider-thumb-container">
				<span class="indexLinktoPage" page-number="16">
					<img src="images/innhold/innhold-thumb15.jpg">
					<h3>
						<small class="d-block">MEDLEM</small>
						Partene diskuterer lønnsdannelsen
					</h3>
				</span>
			</div>

			<div class="col-md-4 slider-thumb-container">
				<span class="indexLinktoPage" page-number="17">
					<img src="images/innhold/innhold-thumb16.jpg">
					<h3>
						<small class="d-block">MEDLEM</small>
						Kjemper for økt kjøpekraft
					</h3>
				</span>
			</div>

			<div class="col-md-4 slider-thumb-container">
				<span class="indexLinktoPage" page-number="18">
					<img src="images/innhold/innhold-thumb17.jpg">
					<h3>
						<small class="d-block">MEDLEM</small>
						REGIONLEDERNE KREVER ØKT LØNN
					</h3>
				</span>
			</div>

			<div class="col-md-4 slider-thumb-container">
				<span class="indexLinktoPage" page-number="19">
					<img src="images/innhold/innhold-thumb18.jpg">
					<h3>
						<small class="d-block">MEDLEM</small>
						849 nye medlemmer i DNB
					</h3>
				</span>
			</div>

			<div class="col-md-4 slider-thumb-container">
				<span class="indexLinktoPage" page-number="20">
					<img src="images/innhold/innhold-thumb19.jpg">
					<h3>
						<small class="d-block">MEDLEM</small>
						Gi deg selv en klapp på skulderen
					</h3>
				</span>
			</div>

			<div class="col-md-4 slider-thumb-container">
				<span class="indexLinktoPage" page-number="21">
					<img src="images/innhold/innhold-thumb20.jpg">
					<h3>
						<small class="d-block">MEDLEM</small>
						FINANSFORBUNDET MEDLEM I DIGITAL NORWAY
					</h3>
				</span>
			</div>

			<div class="col-md-4 slider-thumb-container">
				<span class="indexLinktoPage" page-number="22">
					<img src="images/innhold/innhold-thumb21.jpg">
					<h3>
						<small class="d-block">ARBEIDSRETT</small>
						Skummel pensjonsfelle
					</h3>
				</span>
			</div>
		</div>
	</div>

	<div class="inhold-overlay"></div>
	<!-- //nav-sidebar --> 

	<!-- bottom-toolbar -->
	<div id="bootom-toobar">
		<div class="slide-btn">
			<div class="mag-tool-btn home">
				<a href="javascript:void(0);">
					<img src="images/innhold/home.png">
					<span class="text-white">Hjem</span>
				</a>
			</div> 

			<div class="mag-tool-btn inhold">
				<a href="javascript:void(0);">
					<img src="images/innhold/innhold-icon.png">
					<span class="text-white">Innhold</span>
				</a>
			</div> 
			<div class="mag-tool-btn share" data-toggle="modal" data-target="#shareContentModal">
				<a href="javascript:void(0);">
					<img src="images/innhold/share.png">
					<span class="text-white">Del</span>
				</a>
			</div>  
		</div>
	</div>
	<!-- //bottom-toolbar -->


	<!-- Swiper container -->
	<div class="swiper-container">
		<!-- swiper-wrapper -->
		<div class="swiper-wrapper">

			<!-- PAGE COVER-->
			<div id="1" data-history="hjem" class="swiper-slide">
				<title>Finansfokus Cover</title>
				<section id="article">
					<div class="cover-wrapper"> 
						<section id="home" class="cover-img-section" style="position: relative;"> 
							<div class="cover-box"> 
								<img class="img-responsive" src="images/elements/cover.jpg" alt=""> 
							</div>
						</section> 
					</div>
				</section>
			</div>
			<!-- PAGE COVER-->	

			<!-- PAGE ads -->
			<div id="2" data-history="adv-gjensidige" class="swiper-slide">
				<title>Adv Gjensidige</title>
				<section id="article" class="topSpacer">
					<div class="wrapper">
						<div class="container inner_container">
							<!-- articles-container -->
							<div class="articles-container relative-box">  
								<a href="https://gjensidige.no/YS/" target="_blank">
									<figure class="search-thumbs">
										<img src="images/adv/ads1.jpg" alt="ads1">	
									</figure>
								</a>								
							</div>
							<!-- //articles-container -->
						</div>
					</div>
					<!--row-->
				</section> 
				<div class="spcr40"></div>
			</div>
			<!-- //PAGE ads -->

			<!-- PAGE Leder Inhold-->
			<div id="3" data-history="innhold" class="swiper-slide">
				<title>Finansfokus Innhold</title>
				<section id="article" class="topSpacer">
					<div class="wrapper noindent-section">
						<div class="container inner_container">
							<!-- articles-container -->
							<div class="innhold-contentbox">
								<div class="articles-categorybox">
									<span class="font-FlamaL bg-green" style="color: #ffdd8b;">Leder</span>
									<span class="font-FlamaL bg-green">Innhold</span>
									<img src="images/elements/innhold-logo.png"> 
								</div>
								<br>

								<div class="innhold-bignav">
									<div class="row">
										<div class="col-md-8 colBorder">
											<h2 class="articles-heading font-FlamaB font50 color-green">
												Når du blir gammel og ingen vil ha deg...
											</h2>
											<p class="noIndent">
												<span class="drop-txt font-FlamaT color-green">H</span> 
												vordan verdsettes seniorene på din
												arbeidsplass? Opplever du at de bare
												er et hår i suppa som din arbeidsgiver
												egentlig ønsker å bli kvitt så
												raskt som mulig? Eller blir deres kunnskap og
												kompetanse virkelig verdsatt?
											</p>
											<p>
												I festtalene roses gjerne seniorenes betydning
												og innsats opp i skyene, men hva skjer egentlig
												i praksis? Er det bare vakre ord, servert uten
												praktisk betydning?
											</p>
											<p>
												Enten du selv er senior eller har seniorkolleger
												på din arbeidsplass, så har du kanskje lagt
												merke til hvordan enkelte ledere behandler
												seniorene. De første signalene er når interessante
												arbeidsoppgaver forsvinner en etter en uten at
												det er avtalt. Til slutt er pulten kjemisk støvsugd
												for alt som kan minne om en normal arbeidsplass.
												Så kommer dagen og spørsmålet: Skal du
												ikke snart pensjonere deg? Du har jo allerede
												fylt 62 år!
											</p>
											<p>
												Det er for lengst opplest og vedtatt at flere
												må stå lenger i jobb for at vi alle skal ha noe å
												leve av den dagen vi blir pensjonister. Levealderen
												øker, og det blir stadig færre yrkesaktive
												bak hver pensjonist. Senter for seniorpolitikk
												har beregnet at hvis vi alle venter ett år med å
												pensjonere oss, så gir det en årlig samfunnsmessig
												gevinst på mellom 40 og 50 milliarder kroner.
											</p>
											<p>
												Historisk har ansatte i finansnæringen hatt
												gullkantede pensjonsordninger som har gjort
												det enkelt å gå av tidlig. Mange har også fått
												gode sluttpakker når bedriftene har nedbemannet.
												Det har vært bra for dem som ville kaste
												inn håndkleet for å gjøre andre ting, enn bare å
												jobbe livet ut, men det har ikke vært noen god
												løsning for seniorer som egentlig hadde ønsket
												å stå lenger i arbeid.
											</p>
											<p>
												Men hva må egentlig til for at seniorene
												fortsetter å arbeide også etter fylte 62 år? Nylig
												utførte Finansforbundet en undersøkelse blant
												alle medlemmer over 60 år. Her svarte hele 53
												prosent at de ønsker å gå av med tidligpensjon.
											</p>
											<p>
												Mye må gjøres for å få seniorene til å stå
												lenger i arbeid og ikke minst få bedriftene til å
												fremsnakke verdien av seniorenes arbeidsinnsats.
												Vi må anerkjenne at seniorer har stor verdi
												for at bedriftene skal kunne levere gode resultater.
												Seniorene ønsker fleksibel arbeidstid og
												interessante arbeidsoppgaver i et godt arbeidsmiljø.
												De må ha en psykologisk trygghet for 
												arbeidsplassen, slik at de slipper å holde seg
												fast i pulten.
											</p>
											<p>
												Samtidig sitter seniorene selv med noe av
												nøkkelen. Svært mange 62-åringer ønsker å gå
												av med pensjon, selv om det gir store tap i fremtidige
												pensjonsutbetalinger. Det er her bedriftene
												må sette inn støtet for å gjøre det langt mer
												attraktivt å stå i stilling. Da slipper du som
												senior å sette deg på taket og la kråka ta deg!
											</p>
										</div>

										<div class="col-md-4">
											<br class="d-sm-none">
											<div class="redaktor-imgbox"> 
												<figure class="search-thumbs">
													<img class="img-social leder-img" src="images/leder-img.jpg">
												</figure>

												<p class="person-namebox noIndent">Svein Åge Eriksen</p>
												<div class="person-ceontactinfo fakt-box">
													Ansvarlig redaktør<br>
													<a href="mailto:Sae@finansforbundet.no">Sae@finansforbundet.no</a>
													<br><br>
													<span class="font-FlamaB">Mobil:</span> 900 79 547 
												</div>
											</div>
										</div> 
									</div>
									
									<hr style="border-color: #e6e6e6; margin: 30px 0;">

									<div class="row">
										<div class="col-md-12">
											<h2 class="articles-heading font-FlamaB font24 color-green">
												I DENNE UTGAVEN KAN DU LESE:
											</h2>

											<span class="indexLinktoPage d-block mb30" page-number="8">
												<div class="articles-subtitle color-green">MISTET KONTROLLEN OG TOK LIVET</div>
												<p class="noIndent">
													Marius mistet kontrollen over økonomien,
													men var for stolt til å be om hjelp før det var
													altfor sent. Da huset ble tvangssolgt, valgte
													han å henge seg i skogen uten å gi noen
													nærmere beskjed om hvor og hvorfor. Les det
													gripende intervjuet med moren Anita,
													eiendomsmegleren, en professor i psykiatri
													og hva finansnæringen gjør for å forebygge
													selvmord blant kunder med betalingsvansker. 
													<span class="color-green d-block mt10" style="font-family: 'Flama-Bold'; font-size: 16px;">
														LES MER FRA SIDE 10.
													<span>
												</p>
											</span> 

											<span class="indexLinktoPage d-block mb30" page-number="10">	
												<div class="articles-subtitle color-green">VEIKART FOR SIRKULÆRØKONOMI</div>
												<p class="noIndent">
													Finansnæringen spiller en avgjørende rolle i
													omstillingen til en sirkulær økonomi ved å vri
													kapitalen mot bærekraftige og sirkulære
													aktiviteter. Nå kommer et nytt veikart som
													skal inspirere bedriftene i næringen til
													utvikling av sirkulære produkter og tjenester. 
													<span class="color-green d-block mt10" style="font-family: 'Flama-Bold'; font-size: 16px;">
														LES MER FRA SIDE 18
													<span>
												</p>
											</span>

											<span class="indexLinktoPage d-block mb30" page-number="9">
												<div class="articles-subtitle color-green">PORTRETT STEINAR SIMONSEN</div>
												<p class="noIndent">
													Mens de fleste 62-åringer i finans tenker på å
													trappe ned og vurdere hvordan de skal
													bruke pensjonstilværelsen, tok Steinar
													Simonsen steget helt til topps i Eika
													Gruppen.
													<br>

													– Jeg ble litt overrasket over jobbtilbudet.
													Det er et privilegium å få et sånt
													tilbud som 62-åring. 
													<span class="color-green d-block mt10" style="font-family: 'Flama-Bold'; font-size: 16px;">
														LES MER FRA SIDE 30
													<span>
												</p>
											</span>


											<span class="indexLinktoPage d-block mb30" page-number="13">
												<div class="articles-subtitle color-green">KUN 20–25 BANKER IGJEN</div>
												<p class="noIndent">
													Reguleringer, kapitalkrav og ny
													teknologi vil tvinge frem mer
													samarbeid og standardisering av
													norske banker, mener partner i
													PwC Lars Erik Fjørtoft. Han tror
													Norge etter hvert kun vil ha 20–25 banker. I
													dag er det 110 banker i Norge og 18 filialer av
													utenlandske banker. 
													<span class="color-green d-block mt10" style="font-family: 'Flama-Bold'; font-size: 16px;">
														LES MER PÅ SIDE 34
													<span>
												</p>
											</span>


											<span class="indexLinktoPage d-block mb30" page-number="16">	
												<div class="articles-subtitle color-green">INGEN ENDRING HOS TIETOEVRY</div>
												<p class="noIndent">
													 De fleste
													norske banker har kjernesystemer fra
													IT-selskapet Tietoevry, så mange at Finanstilsynet
													kaller det en konsentrasjonsrisiko.
													Planene om å skille ut bankvirksomheten i et
													eget børsnotert selskap, skapte usikkerhet.
													Den er nå avblåst. 
													<span class="color-green d-block mt10" style="font-family: 'Flama-Bold'; font-size: 16px;">
														LES MER FRA SIDE 36
													<span>
												</p>
											</span>

											<span class="indexLinktoPage d-block mb30" page-number="15">
												<div class="articles-subtitle color-green">ALT OM LØNNSOPPGJØRET</div>
												<p class="noIndent">										
													Årets lønnsoppgjør for alle ansatte i
													finansnæringen, Virke, Spekter og Inkasso er
													ferdig forhandlet og godkjent. Det var noen
													tøffe tak underveis, men alle forhandlingene
													kom i mål som planlagt. I finansnæringen
													gjelder den nye lønnstabellen fra 1. mai. 
													<span class="color-green d-block mt10" style="font-family: 'Flama-Bold'; font-size: 16px;">
														LES MER FRA SIDE 42
													<span>
												</p>
											</span>
										</div>
									</div>
								</div>
							</div>
							<!-- //articles-container -->
						</div>
					</div>
					<!--row-->
				</section> 
				<div class="spcr40"></div>
			</div>
			<!-- //PAGE Leder Inhold-->  

			<!-- PAGE -->
			<div id="4" data-history="kronikk" class="swiper-slide">
				<title>Stengte filialer går på tilliten løs</title>
				<input type="hidden" class="social-image" value="images/redaktor-img2.jpg">
				<section id="team" class="topSpacer">
					<div class="wrapper articles-contentsection">
						<div class="container inner_container">
							<!-- articles-container -->
							<div class="leder-contentbox"> 									
								<div class="redaktor-section"> 
									 
									<div class="Leserinnlegg-section">
										<div class="articles-categorybox mb10" id="kronik">
											<div class="font-FlamaB kronikk-adjcate" style="padding:0;">Kronikk</div> 
										</div>
										 
										<figure class="search-thumbs mb0">
											<img src="images/artiP4-banner.jpg" alt="" class="img-social">
										</figure>
										<div class="images-caption mb30">
											ILLUSTRASJONSFOTO: SHUTTERSTOCK
										</div>
										 

										<div class="relative-box"> 
											<div class="articles-heading font-FlamaB font46 mt15" style="color: #0f4646;">
												Stengte filialer går på tilliten løs
											</div> 
										</div> 
										<h3 class="intro-text content-social">
											Hvordan skal vi utvikle norsk
											banksektor i årene som kommer,
											slik at både kundene og bankene
											blir tilfredse? Vi i LOKALBANK har
											et tips: bort med hengelåsen!
										</h3>
										<br> 
 
										<div class="row">
											<div class="col-md-8 colBorder">
												<p class="noIndent">
													<span class="drop-txt font-FlamaT" style="color: #0f4646;">160</span> filialer. Over hele
													Norge. Det er fasit
													når det gjelder
													antall lokale bankkontorer som har
													blitt lukket og låst de siste 12 årene.
													Men har nedleggelses-rushet gavnet
													kundene? Bankene? Trolig er svaret
													et klokkeklart «nei».
												</p>
												<p>
													Konsulentselskapet Accentures
													«Global Banking Consumer Study»,
													der bankkunder globalt blir spurt om
													sitt forhold til banken sin, publiseres
													årlig. Studien viser at kundetilfredsheten
													skranter, og at behovet for den
													tradisjonelle filialen fremdeles er der.
													Kundene liker de nye digitale kanalene,
													men synes ikke de erstatter den
													menneskelige oppfølgingen i en filial.
												</p>
												<p>
													En bekjent av oss, kunde i en av
													de større bankene, hadde nylig en
													opplevelse som viser akkurat dette.
													Han søkte hjelp til en hverdagsfinansrelatert
													oppgave, der banken
													hans mente at deres såkalte chatbot
													burde kunne bistå. Men akkurat i
													dette tilfellet syntes vår bekjent det
													virket mer hensiktsmessig å få hjelp
													fra et faktisk menneske.
												</p>
												<p>
													I sin frustrasjon over at hans analoge
													preferanse ble tvunget inn i en
													digital irrgang, spurte han chatboten
													om nettopp dette: «Hvorfor er det så
													vanskelig å få snakke med en kundebehandler?
													». Svaret han fikk var både
													talende og ironisk: «Jeg forstår dessverre
													ikke spørsmålet ditt. Kan du
													prøve å spørre på en annen måte?»
												</p>
												<p>
													Slik vi i LOKALBANK ser det,
													snur dette fullstendig om på begrepet
													«kundeservice». For er det ikke banken
													din som burde stilt spørsmålet
													på en annen måte overfor deg som
													kunde, dersom noe var uklart? Ville
													ikke dette vært reell service overfor
													kunden?
												</p>
												<p>
													Vi tror det er de bankene som finner
													balansen mellom digitalisering
													og personlig relasjon som i størst
													grad vil lykkes. En gjennomført strategi,
													en dedikert tilnærming til
													kundebehandling og evnen til å være
													en stabil partner gjennom tykt og
													tynt, vil bli avgjørende. Det digitale
													spiller åpenbart også sin rolle.
												</p>
												<p>
													Og det er jo ikke bare kundene
													som avspises ved en slik ensidig og
													fattig tilnærming til bankfaget. Heller
													ikke for de mange flinke rådgiverne
													rundt omkring i bankene, er
													dette rettferdig og tilstrekkelig.
												</p>
												<p>
													Denne gruppen har et genuint
													ønske om å bistå mennesker med
													finansielle utfordringer, og ha muligheten
													til å følge dem tett opp, slik at
													de unngår å havne i det man av
													erfaring vet kan være starten på en
													ond spiral. En slik hjelp gjøre best
													ansikt til ansikt, ganske enkelt fordi
													vi mennesker etablerer de sterkeste
													tillitsforholdene slik.
												</p>
												<p>
													Så kan man selvsagt spørre seg
													om ikke de lokale bankene, som
													fremdeles satser på tett personlig
													oppfølging av kunder, også på et eller
													annet tidspunkt blir nødt til å kutte
													kostnader i konkurranse med resten
													av bransjen. Vil de ikke da redusere
													og justere på kundebehandlingen?
												</p>
												<p>
													Heldigvis er det ikke så enkelt.
													Vår nære forretningsmodell gjør
													nemlig at vi oppnår økt tillit hos våre
													kunder, som igjen fører til at de i
													større grad stoler på oss når det gjelder
													et utvidet produkttilbud. En
													brukskonto og et boliglån er én ting.
													Men hva med alt annet kundene
													trenger? Forsikringer? Sparing? Vår
													erfaring er at kundene våre stoler
													mer på oss når det gjelder bistand
													med slikt, noe som selvsagt påvirker
													inntektsgrunnlaget vårt.
												</p>
												<p>
													Stengte lokale bankfilialer gir ikke
													bare i frustrerte kunder. Heller ikke
													for bankene selv er overdreven bruk
													av nøkkel og hengelås veien å gå.
												</p>
												<p>
													Hva med å satse på den lokale
													bankfilialen? Ikke stenge? Det akter
													i alle fall vi i LOKALBANK å gjøre
													fremover.
												</p>
											</div> 

											<div class="col-md-4">
												<br class="d-sm-none">
												<div class="redaktor-imgbox"> 
													<img src="images/artiP4-sub1.jpg" class="mb15">
													<p class="person-namebox noIndent color-green">Bjørn Asle Hynne,</p>
													<div class="person-ceontactinfo mt0">
														Administrerende banksjef i Aasen
														Sparebank og Bent R. Eidem,
														administrerende direktør i
														LOKALBANK Alliansen.
													</div>
												</div>
											</div>
										</div>  
									</div> 
									<br>  
									
									<div class="bg-green" style="padding: 30px; color: #fff;">
										<div class="articles-categorybox" id="forbundsleder">
											<span class="font-FlamaR bg-green" style="padding: 0px; color: #f7d989;">Forbundsleder</span> 
										</div>
										<div class="redaktor-section Forbundsleder-section">
											<div class="row">
												<div class="col-md-8 col-lg-9 colBorder">
													<h2 class="articles-heading font-opulent font40" style="margin-bottom: 30px">
														Vi må bry oss om hverandre
													</h2>

													<p class="noIndent">
														<span class="drop-txt font-FlamaT" style="color: #fff;">Å</span> 
														rets tariffoppgjør er kommet i
														havn for alle våre avtaler. Det er
														bra. Resultatene gir gode muligheter
														for reallønnsøkning for
														mange. Våre oppgjør er todelte, med et
														sentralt oppgjør som gir de generelle tilleggene,
														og en ramme for de lokale tilleggene
														som nå skal forhandles i den enkelte bedrift.
														Selv om vi i år har fått et godt oppgjør, er
														det dyrtid i samfunnet. Inflasjonen er høy,
														rentene på lån har steget mye, og skatter og
														avgifter har også økt. Det betyr at mange
														har fått dårligere råd enn de hadde tidligere,
														og vi ser at antall inkassosaker øker, men at
														det fremdeles er lite mislighold på lån. Men
														er vi i stand til å se hva som skjer bak tallene?
														Vi ser at sykefraværet stiger, og mye på
														grunn av psykiske sykdom. Kan noe av
														årsakene være knyttet opp mot dårligere
														økonomi? Blir vi sykere hvis vi ikke fikser
														våre økonomiske problemer? Bank2 har
														hatt et prosjekt gående på dette, og sammen
														med undersøkelser gjort av Oslo Met, så har
														de sett at psykisk helse er nært knyttet opp
														til økonomiske forhold.Mange av bedriftene
														har opprettet funksjoner som skal ivareta
														kunder som har det vanskelig økonomisk.
														Det er godt å kunne bruke våre flinke rådgivere
														både i bank og i inkasso, til å veilede
														kunder som har fått det vanskelig. Men hva
														med våre egne kolleger? Vi sier ofte at
														ansatte i bank og finans er et tverrsnitt av
														befolkningen. Det betyr jo også at en del av
														oss vil havne i den samme statistikken. Er
														vi godt rigget for å ta vare på hverandre i
														vår bransje, dersom vi kommer ut for slike
														problemer? Det bør ikke være et dårligere
														tilbud til egne ansatte
														enn til kunder når
														det gjelder å få hjelp
														til dette, heller tvert
														imot. Det viktigste vi
														kan gjøre er å bry
														oss!
													</p> 
													<br>
												</div>

												<div class="col-md-4 col-lg-3">
													<div class="redaktor-imgbox"> 
														<img src="images/redaktor-img3.jpg" class="redaktor-img">
														<div class="person-namebox">VIGDIS MATHISEN</div>
														<div class="person-ceontactinfo">
															Forbundsleder<br> 
															<span class="font-FlamaB">Mobile:</span> 952 36 141<br>
															<a href="mailto:vigdis.mathisen@finansforbundet.no" style="color: #fff;">vigdis.mathisen@finansforbun<br>det.no</a>
														</div>
													</div>
												</div>
											</div>
										</div> 
									</div>
								</div>  
								<!-- //articles-container -->

								<div class="kolofan-section border-block" style="border-color: #124645; border-top: 0;">
									<div class="row">
										<div class="col-md-4">
											<div class="kolofan-leftsection">
												<img class="fagpressen-logo" src="images/elements/fagpressen-logo.png">
												<br>

												<div class="trykt-logopart">
													<img src="images/elements/trykt-logo.jpg">
													<p class="noIndent">Ålgård Offset AS er <br>godkjent som <br>svanemerket bedrift.</p>
												</div>
												<br> 

												<p class="noIndent">
													<span class="font-FlamaSemiconB">GODKJENT OPPLAG: </span>34 985<br>
													<span class="font-FlamaSemiconB">FORSIDEILLUSTRASJON:</span> <br>Benjamin Gjerde / KI-generert<br>
													<span class="font-FlamaSemiconB">REDAKSJONEN AVSLUTTET:</span><br> 28. mai 2024
												</p>
											</div>
										</div>
										<div class="col-md-8 md-mt30">
											<div class="kolofaninfo-list">
												<h5>
													<span class="font-FlamaCondB color-green">FINANSFOKUS 02/24.</span> Magasin for finansforbundet 25. årgang <span class="font-FlamaCondB color-green">issn 1502-0053</span>
												</h5>
												<div class="row">
													<div class="col-sm-4">
														<h6 class="font-FlamaSemiconB">ANSVARLIG REDAKTØR</h6>
														<p class="noIndent">
															Svein Åge Eriksen <br>
															Mobil: 900 79547 <br>
															<a href="mailto:sae@finansforbundet.no">sae@finansforbundet.no</a>
														</p>
														

														<h6 class="font-FlamaSemiconB">JOURNALIST</h6>
														<p class="noIndent">	 
															Sjur Anda<br>
															Mobil: 470 34460 <br>
															<a href="mailto:san@finansforbundet.no">san@finansforbundet.no</a> 
														</p>
														

														<h6 class="font-FlamaSemiconB">ABONNEMENT</h6>
														<p class="noIndent"> 
															Mona Ramberg<br>
															Mobil: 400 66200<br>
															<a href="mailto:mona.ramberg@finansforbundet.no">Mona.ramberg@finansforbundet.no</a>
														</p>
													</div>

													<div class="col-sm-4">
														<h6 class="font-FlamaSemiconB">ANNONSER</h6>
														<p class="noIndent"> 
															Fredrik Selnes Schei<br>
															Mobil: 934 31772<br>
															<a href="mailto:fredrik@finansforbundet.no">fredrik@finansforbundet.no</a>
														</p>
														

														<h6 class="font-FlamaSemiconB">DESIGN</h6>
														<p class="noIndent">	 
															Mediamania AS v/<br> 
															Benjamin Gjerde
														</p> 

														<h6 class="font-FlamaSemiconB">TRYKK</h6>
														<p class="noIndent">	 
															Ålgård Offset AS
														</p> 
														

														<h6 class="font-FlamaSemiconB">DISTRIBUSJON</h6>
														<p class="noIndent">	 
															Helthjem Mediapost AS
														</p> 
														

														<h6 class="font-FlamaSemiconB">UTGIVER</h6>
														<p class="noIndent"> 
															Finansforbundet<br>
															Postboks 9234 Grønland<br>
															0134 OSLO
														</p>
													</div>

													<div class="col-sm-4">  
														<h6 class="font-FlamaSemiconB">FORBUNDSLEDER</h6>
														<p class="noIndent">	 
															Vigdis Mathisen
														</p>


														<h6 class="font-FlamaSemiconB">DIREKTØR</h6>
														<p class="noIndent">	 
															Runar Wilhelm Henriksen
														</p>
													</div>
												</div>
											</div>
										</div>
									</div>
									<hr style="border-color:#0f4646;">
									<div class="redaktor-btmcontent">
										<img src="images/elements/fagpressen-PFU-logo.png" class="mb10">
										 
										<p class="noIndent font-FlamaSemiconL font16">
											Finansfokus er medlem av Fagpressen og opplagskontrolleres
											årlig. Finansfokus redigeres etter Redaktørplakaten og pressens etiske rammeverk av en selvstendig
											og uavhengig redaktør, og i tråd med Finansforbundets
											grunnsyn og formål. Artikler og synspunkter i Finansfokus
											uttrykker derfor ikke nødvendigvis Finansforbundets syn i enkelte spørsmål.
											<br><br>
											Pressens Faglige Utvalg (PFU) er et klageorgan
											oppnevnt av Norsk Presseforbund som behandler
											klager mot pressen i presseetiske spørsmål.
										</p>
									</div>
								</div>
							</div> 
						</div>
						<!--row-->
					</div>
				</section> 
				<div class="spcr40"></div>  
			</div>
			<!-- //PAGE --> 

			<!-- PAGE NYHETER -->
			<div id="5" data-history="abonner-pa-vart-nyhetsbrev!" class="swiper-slide">
				<title>ABONNER PÅ VÅRT NYHETSBREV!</title>  

				<section id="team" class="topSpacer">
					<div class="wrapper articles-contentsection">
						<div class="container inner_container">
							<!-- articles-container -->

							<div style="margin-top: 15px; border-top: 20px solid #3a825f;"> 
								<div style="background: #e0e2e2; padding: 0 30px;">
									<div class="row">
										<div class="col-md-7">
											<div style="padding: auto 30px;">
												<br> 
												<br> 
												<br class="d-none d-md-block">
												<br class="d-none d-md-block">  
												<h2 class="articles-heading font-FlamaB font60" style="color: #0e8760;">
													ABONNER<br class="d-none d-md-block">
													PÅ VÅRT
													NYHETSBREV!
												</h2>
												
												<br class="d-none d-md-block"> 

												<h3 class="intro-text content-social mt30">
													Vil du holde deg oppdatert på
													det som skjer i finansbransjen?
													Da bør du abonnere på vårt
													ukentlige nyhetsbrev! Hver
													fredag sender vi en epost der
													vi går gjennom ukens viktigste
													hendelser og analyserer
													hvordan dette påvirker deg og
													næringen. Klikk på <br class="d-none d-lg-block">QR-koden
													lengst ned for å komme til
													registeringssiden.
												</h3>

												<br class="d-none d-md-block"> 

												<br><br>
												<img src="images/artiP6-sub1.jpg" style="width: 200px;" class="mt30">
												<br><br>
											</div>
										</div>
										<div class="col-md-5">
											<figure class="search-thumbs">
												<img src="images/artiP6-banner.jpg" alt="" class="img-social">
											</figure> 
										</div>
									</div>
								</div>
							</div>  
						</div>
					</div>
					<!--row-->
				</section> 
				<div class="spcr40"></div>
			</div>
			<!-- // PAGE NYHETER -->  

			<!-- PAGE ads -->
			<div id="6" data-history="adv-procon" class="swiper-slide">
				<title>Adv Procon</title>
				<section id="article" class="topSpacer">
					<div class="wrapper">
						<div class="container inner_container">
							<!-- articles-container -->
							<div class="articles-container relative-box">  
								<a href="https://www.procondigital.no/" target="_blank">
									<figure class="search-thumbs">
										<img src="images/adv/ads2.jpg" alt="ads2">
									</figure>		
								</a>							
							</div>
							<!-- //articles-container -->
						</div>
					</div>
					<!--row-->
				</section> 
				<div class="spcr40"></div>
			</div>
			<!-- //PAGE ads -->  

			<!-- PAGE -->
			<div id="7" data-history="siden-sist" class="swiper-slide">
				<title>Siden sist</title> 
				<section id="team" class="topSpacer">
					<div class="wrapper articles-contentsection">
						<div class="container inner_container">
							<!-- articles-container -->

							<div style="margin-top: 15px;">
								<div class="articles-categorybox marbot20">
									<span class="font-FlamaL bg-green" style="color: #ffdd8b;">Nyheter</span> 
									<div class="articategory-content font-FlamaR color-green">Siden sist</div>
								</div> 

								<div class="news-block"> 
									<div style="background: #000000;">
										<div class="row">
											<div class="col-sm-12 col-lg-5"> 
												<div class="imgbox"> 
													<a class="imgpopup" href="images/artiP8-banner.jpg" data-fancybox="" data-caption="">
														<figure style="margin: 0">
															<img src="images/artiP8-banner.jpg" class="img-social">
														</figure>
													</a> 
												</div> 
											</div>

											<div class="col-sm-12 col-lg-7">
												<div class="news-content" style="color: #fff; padding: 40px;"> 
													<br class="d-none d-lg-block">
													<div class="news-subtitle font-FlamaB font20">
														Tidenes bonusfest i SR-Bank
													</div>
													<p class="noIndent content-social">
														Alle ansatte i SR-Bank får full uttelling og får 10 prosent av brutto
														fastlønn i bonus som takk for innsatsen i 2023. Også i DNB og
														SpareBank 1 Gudbrandsdal kan ansatte glede seg over en historisk
														høy bonusutbetaling. I Gjensidige har også alle ansatte fått en
														kollektiv bonus. (12. mars)
													</p>
												</div>
											</div>

											
										</div>
									</div>
									<br>

									<div class="row">
										<div class="col-lg-8">
											<div style="background: #c9b6a4;">
												<div class="row">
													<div class="col-md-7">
														<div class="news-content" style="color: #000; padding: 40px;">  
															<div class="news-subtitle font-FlamaB font20 color-green">
																FLERE VURDERER Å BYTTE HELLIGDAGER
															</div>
															<p class="noIndent">
																Med godkjennelse fra lokale
																fagforeninger, kan ansatte i If nå
																bytte utvalgte helligdager til datoer
																som passer bedre med deres
																religion. Flere vurderer samme
																ordning. (10. april)
															</p>
														</div>
													</div>

													<div class="col-md-5">
														<div class="imgbox"> 
															<a class="imgpopup" href="images/artiP8-sub1.jpg" data-fancybox="" data-caption="">
																<img src="images/artiP8-sub1.jpg">
															</a> 
														</div> 
													</div>
												</div>
											</div>
											<br>

											<div style="background: #000;">
												<div class="row">
													<div class="col-md-5">
														<div class="imgbox"> 
															<a class="imgpopup" href="images/artiP8-sub2.jpg" data-fancybox="" data-caption="">
																<img src="images/artiP8-sub2.jpg">
															</a> 
														</div> 
													</div>

													<div class="col-md-7">
														<div class="news-content" style="color: #fff; padding: 20px;"> 
															<br class="d-none d-lg-block">
															<div class="news-subtitle font-FlamaB font20">
																Du må lære nye ferdigheter
															</div>
															<p class="noIndent">
																90 prosent av medlemmene i
																Finansforbundet tror det blir endringer
																i arbeidshverdagen de to neste årene.
																– Det vil bli stort behov for å lære nye
																ferdigheter i finansbransjen, sier Malin
																Dahl, som forsker på ny kompetanse
																for Finansforbundet. <br class="d-none d-lg-block">(19. april)
															</p>
														</div>
													</div>
												</div>
											</div>	
										</div>

										<div class="col-lg-4 lg-mt30">
											<div class="imgbox"> 
												<a class="imgpopup" href="images/artiP8-sub3.jpg" data-fancybox="" data-caption="">
													<img src="images/artiP8-sub3.jpg">
												</a> 
											</div> 
											<br>

											<div class="news-content">
												<div class="news-subtitle font-FlamaB font20 color-green">
													BANKFUSJONER SVEKKER KONKURRANSEN
												</div>
												<p class="noIndent" style="font-family: 'Flama';" >
													Sjefen for Konkurransetilsynet frykter at
													alle bankfusjonene svekker konkurransen
													ytterligere i boliglånsmarkedet. Samtidig gjør
													reguleringene det vanskelig for nye banker å
													etablere seg. (11. mars). Også Finansforbundet
													advarer mot svekket konkurranse i
													banknæringen i en høringsuttalelse til
													finansmarkedsmeldingen.
												</p>
											</div>
										</div>	
									</div>
									<br>

									<div class="art-ribbon font-FlamaB text-center">
										NYHETER FRA WWW.FINANSFOKUS.NO
									</div>
									<div style="background: #f4f5f5;">
										<div class="row">
											<div class="col-md-12 col-lg-6 order-md-2"> 
												<div class="imgbox"> 
													<a class="imgpopup" href="images/artiP8-sub4.jpg" data-fancybox="" data-caption="">
														<img src="images/artiP8-sub4.jpg">
													</a> 
												</div> 
											</div>
											<div class="col-md-12 col-lg-6 order-md-1 align-self-top">
												<div class="news-content" style="padding: 30px;"> 
													<br> 
													<div class="news-subtitle font-FlamaB font20" style="color: #932021;">
														BRUKER KI I REKRUTTERING
													</div>
													<p class="noIndent mb0">
														DNB bruker kunstig intelligens (KI) til å screene kandidater til jobber der det er mer
														enn 1 000 søkere. Dette gir effektivisering og økt mangfold, mener banken. Andre i
														bransjen er mer nølende med å ta teknologien i bruk på dette området. (15. april)
													</p>
												</div>
											</div>
										</div>
									</div>
								</div>  
							</div>  
						</div>
					</div>
					<!--row-->
				</section> 
				<div class="spcr40"></div>
			</div>
			<!-- //PAGE -->  

			<!-- PAGE ads -->
			<div id="8" data-history="adv-nordea" class="swiper-slide">
				<title>Adv Nordea</title>
				<section id="article" class="topSpacer">
					<div class="wrapper">
						<div class="container inner_container">
							<!-- articles-container -->
							<div class="articles-container relative-box">  
								<a href="https://www.nordea.no/privat/vare-produkter/lan-og-kreditt/billan-forbrukslan-og-andre-lan/billan.html" target="_blank">
									<figure class="search-thumbs">
										<img src="images/adv/ads3.jpg" alt="ads2">
									</figure>		
								</a>							
							</div>
							<!-- //articles-container -->
						</div>
					</div>
					<!--row-->
				</section> 
				<div class="spcr40"></div>
			</div>
			<!-- //PAGE ads --> 

			<!-- PAGE -->
			<div id="9" data-history="marius-tok-sitt-liv-etter-tvangssalg" class="swiper-slide">
				<title>MARIUS TOK SITT LIV ETTER TVANGSSALG</title>
				<section id="team" class="topSpacer">
					<div class="wrapper articles-contentsection">
						<div class="container inner_container">
							<!-- articles-container -->
							<div class="articlesborder-section" style="background: #262322; color: #fff; padding: 30px 30px;"> 	 
								<div class="articles-categorybox">
									<span class="font-FlamaL" style="color: #fff; background: #f69068;">Reportasje</span> 
									<div class="articategory-content font-FlamaR">Gjeld og selvmord</div>
								</div>   
								<br> 

								<h2 class="articles-heading font-opulentB  font50">
									<img src="images/artiP12-sub1.png" style="max-width: 480px; width: 95%;">
								</h2> 
								<br>

								<h3 class="intro-text content-social">
									34 år gamle Marius ble funnet hengende i et tre etter
									at huset hans havnet på tvangssalg. Ingen av hans
									nærmeste fanget opp hans dårlige økonomi og hvordan
									den tæret på ham. Dårlig økonomi er en kjent risikofaktor
									for selvmord. Hvordan skal banker og andre kreditorer
									fange opp skyldnere i risikosonen?
								</h3>
								<div class="articles-textfotobox">
									Tekst: <span class="font-FlamaB">Sjur Anda</span> Illustrasjonsfoto: <span class="font-FlamaB">Shutterstock</span>
								</div>
								<br>

								<div class="imgbox" style="margin: auto -30px;"> 
									<figure class="search-thumbs">
										<img src="images/artiP12-banner.jpg" alt="" class="img-social"> 
									</figure> 	 
								</div> 
								<br>

								<div class="row">
									<div class="col-md-8 colBorder">
										<p class="noIndent">
											<span class="drop-txt font-FlamaT">G</span> 
											<strong class="font-PalatinoB">EITHUS:</strong> 5. oktober 2023. Anita Jahr
											får en telefon fra sin eldste sønn.
											Har hun hørt noe fra broren Marius?
											Han skulle vært på jobb klokken to
											og hadde ikke dukket opp. De hadde prøvd
											å ringe ham uten å få svar. Anita kjenner en
											liten klump i magen. Dette var helt ulikt
											Marius. Hun prøver selv å få tak i ham, men
											får ikke noe svar.
										</p>
										<p>
											– Jeg fikk en følelse av at noe var veldig
											galt, sier Anita. Vi treffer henne hjemme på
											Geithus en vakker vårdag i mai.
											Et par timer senere dro en kompis hjem til
											Marius. Der var det tomt, bortsett fra noen
											papirer fra banken som lå fremme. Der sto
											det at huset skulle på tvangssalg.
										</p>
										<p>
											Anita ringte politiet og meldte ham savnet.
										</p>
										<p>
											– De tok det på alvor med en gang, og de
											var raskt i gang med full leteaksjon.
										</p>
										<p>
											Via sporing fant de ut at telefonen var i
											Noresund et sted, og letingen ble flyttet dit.
											Helikoptre, hundeførere og Røde Kors ble
											satt inn i aksjonen.
										</p>
										<p>
											– Jeg var helt nummen, men visste innerst
											inne at jeg aldri fikk se Marius igjen, forteller
											hun.
										</p>
										<p>
											Rundt halv tre på natten fikk hun telefon
											fra politiet. De hadde funnet bilen. To timer
											senere ble Marius funnet, hengende i et tre.
										</p>
										<p>
											Tilbake satt Anita sammen med sine to
											andre sønner og forsto ingenting.
										</p>
										<p>
											– Det kom som en total bombe på oss. Vi
											trodde alt var på stell. Ingen hadde fanget
											opp at han slet så fælt. Han må ha holdt så
											mye vonde ting inni seg. Det er én time å
											kjøre fra Geithus til Noresund. Der har han
											parkert bilen, tatt med seg et tau og gått langt
											inn i skogen, der han har festet tauet og tatt
											siste steget i livet. Han har hatt god tid til
											å avbryte og tenke seg om. Da er det ikke
											spontant. Hvor vondt har han ikke hatt det
											denne tiden? Det er helt grusomt å tenke på.
										</p> 
									</div> 

									<div class="col-md-4">  
										<div class="quate-box font-FlamaB v-center mb10" style="max-width: 345px;">
											<span style="color: #f69068;">”</span>Jeg fikk en
											følelse av
											at noe var
											veldig galt.<span style="color: #f69068;">”</span> 

											<div class="name-onquate font-FlamaL">
												Anita Jahr
											</div>  
										</div> 
									</div> 

									<div class="col-md-8 colBorder">
										<div class="articles-subtitle">GJELD OG TVANGSSALG</div>
										<p class="noIndent">
											Anita beskriver sønnen som snill, introvert
											og stille og beskjeden.
										</p>
										<p>
											– Han tenkte på alle andre først.
											Samtidig var Marius en ressurssterk
											mann. I mange år var han kjøpmann på 
											en dagligvarebutikk. Dette ble etter
											hvert veldig travelt, og han sluttet der.
											Han fikk raskt jobb på Splitkon, bare
											et steinkast unna huset til Anita. Også
											der ble han raskt en ressursperson som
											bedriften ville satse på. Tilsynelatende
											sto alt bra til. Marius hadde fast jobb og
											god inntekt. Likevel viser historien at
											han ikke fikk økonomien til å gå rundt.
											Etter en gjennomgang av økonomien
											viser det seg at han hadde en gjeld på
											drøyt 1,5 millioner, og at han ikke har
											betalt huslån og andre regninger. Dette
											har gjort at han var i ferd med å miste
											huset.
										</p>
										<p>
											– Jeg aner ikke hvordan økonomien
											har sklidd sånn ut. Det er mye mulig
											han fikk seg en knekk da han sluttet som
											kjøpmann. Det var slitsomt, og det er
											mye mulig han følte at han feilet. Dette
											kan ha ført til en depresjon eller noe
											sånt. Han hadde jo inntekt til å betjene
											gjelden, så det er rart han har latt det gå
											så langt som til tvangssalg, sier Anita.
											Marius var en stolt mann. Da han ga
											seg som kjøpmann og sto uten inntekt,
											hadde han ikke meldt seg arbeidsledig
											på Nav for å få inntekt.
										</p>
										<p>
											– Jeg spurte hva han
											skulle leve av. Han svarte
											han hadde oppsparte
											midler og ville klare seg
											i mange måneder uten
											inntekt. Han var nok for
											stolt til å gå til Nav.
											Nå er huset solgt. Dette
											dekket stort sett all gjeld.
										</p>

										<div class="articles-subtitle">FANGET IKKE OPP</div>
										<p class="noIndent">
											Ruth Helling var eiendomsmegleren
											som sto for salget av huset til Marius.
											Hun fanget ikke opp noen tegn til at det
											sto så ille til.
										</p>
										<p>
											– Noen skjønner du sliter, men
											Marius fanget vi ikke opp i det hele
											tatt. Jeg så den ikke komme, sier Helling.
										</p>
										<p>
											Hun jobber som eiendomsmegler hos
											Dialog Eiendomsmegling i Drammen
											hvor hun i tillegg til vanlig jobb, har
											ansvar for selskapets tvangssalg. Marius
											er dessverre ikke den første saksøkte
											(boligeieren) som har tatt livet sitt.
										</p>
										<p>
											– I snitt har jeg opplevd alvorlig
											trussel om, forsøk på eller fullbyrdet
											selvmord en gang per år. Noen ganger
											varsler jeg AMK eller annet helsepersonell
											med bakgrunn i det jeg opplever i
											kontakt med den som er saksøkt. Noen
											ganger, når jeg har vært bekymret, har
											det vært på hengene håret, sier Helling.
										</p>
										<p>
											Et eksempel på dette er en fravikelse,
											som utkastelse heter på fagspråket, noen
											år tilbake. Da ingen åpnet døren, satt
											låsesmed i gang med å borre opp låsen.
											Inne var det stille. Tilsynelatende var
											det ingen hjemme. Likevel var det noe
											som ikke stemte og noen røde prikker
											på gulvet ledet frem til en låst dør som
											også ble åpnet. Huseier hadde dessverre
											forsøkt å ta livet av seg.
										</p>
										<p>
											– Rask respons fra ambulansepersonell
											berget heldigvis livet hans, sier
											Helling.
										</p>
									</div>

									<div class="col-md-8 colBorder"> 
										<div class="articles-subtitle">FLERE FULLBYRDELSER</div>
										<p class="noIndent">
											Med høye renter og økte kostnader
											skulle man tro at det ble en økning i
											antall tvangssalg.
										</p>
										<p>
											– Oppdragsmengden oppleves som
											relativt stabil. Forskjellen er at det er
											flere fullbyrdede salg. Jeg har gjennomført
											flere til tvangssalg hittil i år enn i
											hele fjor. Det er litt skremmende. Før har
											det vært større mulighet
											for meg å fremforhandle
											løsninger som refinansiering,
											som har vært til
											det beste for både kreditor
											og boligeier. Høye renter
											og økt inflasjon har svekket
											handlingsrommet jeg
											hadde tidligere, sier Helling,
											som også opplever
											stor forskjell i hvor interesserte
											banker og panthavere er i å finne
											løsninger før eiendommen faktisk legges
											ut for salg.
										</p>
										<p>
											– Det er stor forskjell på saksbehandlerne
											jeg møter i de ulike bankene,
											inkassobyråene, Skatteetaten og så
											videre. Noen er mer løsningsorientert
											enn andre. Andre er i større grad opptatt
											av å få dekket inn sine krav så fort som
											mulig. Et krav kan fort øke fra noen
											hundrelapper til flere tusen på grunn
											av blant annet inkassosalær og renter.
											Når det besluttes et tvangssalg legges i
											tillegg rettsgebyr til summen. Dette må
											også innfris av skyldner/saksøkte. Når
											tvangssalg er besluttet på bakgrunn av
											lave krav, oppleves det som urimelig å
											tvinge noen fra boligen sin på grunn av
											dette, sier Helling.
										</p>
									</div> 

									<div class="col-md-4">  
										<div class="quate-box font-FlamaB v-center mb10" style="max-width: 345px;">
											<span style="color: #f69068;">”</span>Det er stor forskjell på saksbehandlerne jeg møter.<span style="color: #f69068;">”</span> 

											<div class="name-onquate font-FlamaL">
												Ruth Helling
											</div>  
										</div> 
									</div>

									<div class="col-sm-12">
										<br>
										<div class="imgbox mb30"> 
											<a class="imgpopup" href="images/artiP12-sub2.jpg" data-fancybox="p12" data-caption="FLERE HENDELSER: Eiendomsmegler Ruth Helling har hatt flere hendelser med gjennomførte selvmord i forbindelse med tvangssalg. (FOTO: SJUR ANDA)">
												<img src="images/artiP12-sub2.jpg" alt="">
											</a> 
											<div class="images-caption">
												<span class="font-FlamaB">FLERE HENDELSER:</span> Eiendomsmegler Ruth Helling har hatt flere hendelser med gjennomførte selvmord i forbindelse med tvangssalg. (FOTO: SJUR ANDA)
											</div>
										</div>
									</div> 

									<div class="col-md-8 colBorder">
										<div class="articles-subtitle mt0">FLERE ÅRSAKER</div>
										<p class="noIndent">
											Hvordan folk havner i en så dårlig
											økonomisk situasjon at boligen må på
											tvangssalg varierer veldig.
										</p>
										<p>
											– Noen sliter bare med dårlig økonomisk
											styring, og vi har derfor også en
											del gjengangere. Dette er saker vi stort
											sett klarer å løse. Ellers kan det være alt
											fra skilsmisse, svindel, det er ektefeller
											som lurer hverandre, spillegjeld, rusrelatert
											gjeld og ferieturer finansiert med
											kredittkort. Det er selvsagt emosjonelt
											verre når folk kommer i økonomiske
											problemer knyttet til sykdom eller tap
											av arbeidsplass. Mange kommer inn i
											en vold sirkel, sier Helling, som har sett
											eksempler der folk føler at de må dra
											utenlands for å kjøpe dyre kreftmedisiner
											som de ikke får her i Norge.
										</p>
										<p>
											– De som havner på mitt bord, kommer
											fra alle samfunnslag. Det er lett å tro
											at denne type saker i hovedsak rammer
											ressurssvake mennesker. Det stemmer
											ikke. I mine mapper er det folk fra alle
											samsfunnlag.
										</p>

										<div class="articles-subtitle">KONVOLUTTSKREKK</div>
										<p class="noIndent">
											For mange blir ikke saken lettere av å
											få brev de kanskje ikke helt forstår, og
											det er ikke primært på bakgrunn av
											språkproblemer. Undersøkelser
											viser at én av tre
											nordmenn sliter med å
											forstå innholdet i brev de
											mottar fra offentlige etater.
										</p>
										<p>
											– Veldig mange har
											pådratt seg «konvoluttskrekk
											» og velger å ikke åpne post.
											Da forsøker jeg andre former for kommunikasjon.
											I mange tilfeller oppnår
											jeg kontakt ved å sende en SMS. Når
											jeg får kontakt,så forsøker jeg å skape
											tillit ved å fortelle at jeg har forståelse
											for at de befinner seg i en vanskelig
											situasjon, og at formålet mitt ikke er
											å ødelegge livene for dem. Vi snakker
											om mulighetene som finnes, for å søke
											andre løsninger enn salg og om at det
											blir enklere å finne løsninger om vi
											samarbeider, sier eiendomsmegleren.
										</p>
									</div> 

									<div class="col-md-4">  
										<div class="quate-box font-FlamaB v-center mb10" style="max-width: 345px;">
											<span style="color: #f69068;">”</span>Mange har pådratt seg konvoluttskrekk.<span style="color: #f69068;">”</span> 

											<div class="name-onquate font-FlamaL">
												Ruth Helling
											</div>  
										</div> 
									</div>


									<div class="col-md-8 colBorder">
										<div class="articles-subtitle">MYE SKAM</div>
										<p class="noIndent">
											Når hun får kontakt, er det mye skam.
										</p>
										<p>
											– Det er skambelagt å ikke gjøre opp
											sine forpliktelser, og det er vanskelig
											for folk å vite hvor de kan få hjelp når
											saken har kommet for langt. Jeg har
											sett flere eksempler på useriøse aktører
											som sier de skal hjelpe, men hvor resultatet
											ender med at folk ender opp med
											enda større problemer enn de hadde i
											utgangspunktet. Det finnes heldigvis
											også mange seriøse
											aktører som
											bidrar til en refinansiering,
											som
											hjelper saksøkte
											ut av sitt uføre.
											Det er ikke alltid
											at vi lykkes med
											en langvarig løsning. Jeg ser dessverre
											at noen folk som har fått refinansiert
											én gang, også kommer tilbake i
											samme situasjon igjen. Før det kreves
											fravikelse (utkastelse) oppsøker jeg saksøkte
											hjemme på eiendommen. Jeg har
											også mulighet til å be Namsfogden om
											bistand til besiktigelse, taksering, foto
											og visning. Jeg opplever Namsfogden
											som en stor trygghet. Det er mennesker
											med stor emosjonell intelligens, forteller
											Helling.
										</p>

										<div class="articles-subtitle">KOMMUNALT ANSVAR</div>
										<p class="noIndent">
											Hvis du sitter med mye gjeld, er det
											mulig å få gjeldsordning. Det finnes
											faktisk også en hjemmel for å få dette
											selv om du eier boligen din.
										</p>
										<p>
											– I praksis blir dette ikke brukt. Kommunene
											har ansvar for å tilby rådgivning
											og veiledning om økonomi. Det
											fungerer dessverre ikke. Jeg savner et
											sted å henvende meg når jeg ser at folk
											ikke har ressurser til å be om hjelp, og
											hvor ting kunne vært løst på en annen
											måte. Det er ofte billigere og bedre
											både for saksøkte og panthaver, hvis
											saksøkte hadde fått hjelp til å håndtere
											situasjonen.
										</p>

										<div class="articles-subtitle">TA KONTAKT</div>
										<p class="noIndent">
											<span class="font-PalatinoI">– Hva skal da banker og andre finansinstitusjoner
											gjør for å unngå at folk havner i
											en så dårlig situasjon at de må selge huset?</span>
										</p>
										<p>
											– Hvis avdragene ikke blir betalt må
											man spørre seg hva årsaken kan være.
											Jeg har sett flere tilfeller av demens der 
											folk verken husker hvor mye de skylder
											eller at de skal betale. Regninger som
											går uten avtalegiro, blir gjerne liggende.
											Å sende kompliserte brev til folk som
											ikke forstår innholdet, har ingen funksjon.
											Alt er riktig juridisk, men systemet
											fungerer ikke etter intensjonen. Ofte kan
											det være lettere å få hjelp i mindre kommuner.
											Kommunen og kontaktpersonen
											i banken på mindre streder kjenner
											gjerne personen det gjelder, og bakenforliggende
											årsaker. Det
											oppleves som
											mye vanskeligere
											å forholde seg til
											rene onlinebanker,
											Skatteetaten
											og gigantiske
											kredittforetak, sier Helling.
										</p>
										<p>
											Ved bekymringsmelding er det likedan.
										</p>
										<p>
											– Jeg har prøvd å få tak i kommunelegen
											i større kommuner for å gi en
											bekymringsmelding. Det er ikke alltid
											like lett. Hver bydel har ulike team. Ofte
											er den som har ansvar, utilgjengelig. Jeg
											prøver å legge igjen beskjed, men blir
											ofte ikke kontaktet tilbake. Det er etter
											min mening ikke godt nok. Ringer du til
											kommuneoverlegen på et mindre sted,
											får du gjerne prate med vedkommende.
										</p>
										<p>
											Det er også en
											utfordring med
											taushetsplikten
											som begrenser dialogen
											blant annet
											med Nav.
										</p>
										<p>
											– Vi kan ikke
											dele ting rundt helseproblematikk
											og økonomi med familie
											og andre aktører som kunne bidratt
											til å løse saken på et tidligere tidspunkt,
											slik at man unngår unødvendig økning
											i salærer, gebyrer og rentekostnader. Jeg
											forstår at det må være slik, men noen
											ganger kunne en samtale bidratt til en
											god løsning.
										</p>
									</div>


									<div class="col-md-8 colBorder">
										<div class="articles-subtitle">SKJØRT LIV</div>
										<p class="noIndent"> 
											<span class="font-PalatinoI">– Hva gjør det med deg å stadig oppleve at
											folk velger å ta sitt eget liv?</span>
										</p>
										<p>
											– Jeg får en respekt for at livet er
											skjørt. Jeg er bevisst og oppmerksom
											på at dem jeg er i kontakt med, er noen
											som potensielt kan stå på kanten, og at 
											det er lite som skal til for at det blir et
											tragisk utfall.
										</p>
										<p>
											<span class="font-PalatinoI">– Når føler du at du gjør en god jobb?</span>
										</p>
										<p>
											– Når jeg klarer å løse saken på en
											måte som gjør at eier av huset (saksøkte)
											føler seg rettferdig behandlet, til tross
											for at de i utgangspunktet ikke hadde
											lyst å ha kontakt med meg, så oppleves
											det som en personlig seier. Det er heldigvis
											også mange som uttrykker takknemlighet
											i ettertid, og som ser at den
											prosessen jeg har styrt, har vært til det
											beste når situasjonen var som den var.
											Det er min motivasjon og som gjør at
											jeg ønsker å jobbe med disse krevende
											sakene.
										</p>
									</div> 

									<div class="col-md-4">  
										<div class="quate-box font-FlamaB v-center mb10" style="max-width: 345px;">
											<span style="color: #f69068;">”</span>Marius var min store stolthet.<span style="color: #f69068;">”</span> 

											<div class="name-onquate font-FlamaL">
												Anita Jahr
											</div>  
										</div> 
									</div>

									<div class="col-md-8 colBorder">
										<div class="articles-subtitle">LEVER I UVISSHET</div>
										<p class="noIndent"> 
											Vi er tilbake i Geithus. Det ble aldri funnet
											noe brev eller noe annet som kunne
											forklare hvorfor Marius tok sitt eget liv.
										</p>
										<p>	
											– Jeg skulle ønske jeg hadde fått en
											pekepinn på hva som var årsaken. Å
											ikke vite helt sikkert er vanskelig, selv
											om jeg egentlig er jeg helt overbevist om
											at dette henger sammen med økonomi
											og skam. Kanskje har brevet om tvangssalg
											fått ham til å tippe over. Det må
											ha vært fryktelig ensomt å stå alene i
											denne situasjonen. Jeg håper at de som
											jobber med denne type ting, er ekstra
											observante på risikoen som ligger der.
											Dårlig økonomi
											og tvangssalg kan
											pushe folk langt.
											Det er kanskje
											ekstra sårt at ikke
											et eneste menneske
											har fanget dette
											opp, og skjønt at
											her var det noe galt, sier Anita.
										</p>
										<p>	
											Hun tenker ofte tilbake til siste gang
											hun så Marius. Hun hadde laget mat i
											en bolle til ham for noen dager siden, og
											han var innom og leverte bollen tilbake.
										</p>
										<p>	
											– Vi pratet om at jeg har lyst å flytte
											til Spania, og jeg spurte hva han tenkte
											om det. Han mente jeg burde kjøre på.
											Det var en god samtale. Men det var
											likevel noe trist over ham den siste
											tiden, når jeg tenker etter. Det er bare
											så ufattelig trist. Marius var min store
											stolthet.
										</p>
									</div>
								</div> 
							</div> 		 
						</div>
					</div>
					<!--row-->
				</section> 
				<div class="spcr40"></div>
			</div>
			<!-- // PAGE --> 

			<!-- PAGE -->
			<div id="10" data-history="kjent-risikofaktor" class="swiper-slide">
				<title>KJENT RISIKOFAKTOR</title>
				<section id="team" class="topSpacer">
					<div class="wrapper articles-contentsection">
						<div class="container inner_container">
							<!-- articles-container -->
							<div class="articlesborder-section" style="background: #262322; color: #fff; padding: 30px 30px;"> 	 
								<div class="articles-categorybox">
									<span class="font-FlamaL" style="color: #fff; background: #f69068;">Reportasje</span> 
									<div class="articategory-content font-FlamaR">Gjeld og selvmord</div>
								</div>   
								<br> 

								<h2 class="articles-heading font-FlamaB font50">
									KJENT RISIKOFAKTOR
								</h2> 
								<br>

								<h3 class="intro-text content-social">
									Om lag 650 mennesker tar sitt eget liv i Norge hvert år. En del av disse sliter med dårlig økonomi, noe som i flere tilfeller vil bidra til at de tar dette valget.
								</h3> 
								<br>   

								<div class="row">
									<div class="col-md-4 order-md-2">  
										<div class="imgbox mb30"> 
											<a class="imgpopup" href="images/artiP15-banner.jpg" data-fancybox="p15" data-caption="VÆR BEVISST: Når du jobber med folk som er i en presset økonomisk situasjon, må du være bevisst på at det finnes en økt risiko for selvmord, mener Lars Mehlum. (FOTO: PRIVAT)">
												<figure class="search-thumbs">
													<img src="images/artiP15-banner.jpg" alt="" class="img-social">
												</figure>
											</a> 
											<div class="images-caption">
												<span class="font-FlamaB">VÆR BEVISST:</span> Når du jobber med folk som er i en presset økonomisk situasjon, må du være bevisst på at det finnes en økt risiko for selvmord, mener Lars Mehlum. (FOTO: PRIVAT)
											</div>
										</div>
										<br>

										<div class="quate-box font-FlamaB v-center" style="max-width: 345px;">
											<span style="color: #f69068;">”</span>Det er når du har for lite penger du oppdager hvor viktige de er.<span style="color: #f69068;">”</span>  
										</div> 
									</div>   
									<div class="col-md-8 colBorder order-md-1">
										<p class="noIndent">
											<span class="drop-txt font-FlamaT">D</span> et er en kjent sammenheng
											mellom økonomiske
											problemer og
											selvmord. Det varierer
											dog veldig fra miljø til miljø. I
											Norge er sammenhengen mindre
											uttalt enn eksempelvis i USA.
											Dette henger sammen med at vi
											her i landet har bedre offentlige
											støttetiltak ved økonomiske problemer.
											I USA kan dårlig økonomi
											ha langt alvorligere konsekvenser
											enn her, sier Lars Mehlum, professor
											i psykiatri og suicidologi ved
											Universitetet i Oslo og senterleder
											ved Nasjonalt senter for selvmordsforskning
											og -forebygging.
										</p>
										<p class="font-PalatinoI">
											Renteoppgang og økte priser har
											gitt mange norske husholdninger en
											sterkt forverret økonomisk situasjon.
											Vil dette gi seg utslag i flere
											selvmord?
										</p>
										<p>
											– Vi har mange eksempler fra
											enkeltpersoner og enkelthendelser
											på at slike økonomiske
											problemer kan øke risikoen for
											selvmord. Dessverre har det vært
											gjort lite forskning på dette feltet
											i Norge, så systematisk viten har
											vi lite av, sier Mehlum.
										</p>
										<p>
											Han tror det er flere mekanismer
											ved dårlig økonomi som
											spiller inn.
										</p>
										<p>
											– Psykisk uhelse eller andre
											helseplager reduserer folks evne
											til å jobbe og dermed også evnen
											til å klare seg økonomisk. Det kan
											også være folk som får dårlig økonomi,
											eksempelvis ved at man mister jobben, der den dårlige
											økonomiske situasjonen leder til dårlig psykisk helse. Det
											er også tøffere å ha det psykisk vanskelig når du har dårlig
											økonomi, sier Mehlum.
										</p>

										<div class="articles-subtitle">TAP OG SKAM</div>
										<p>
											For mange som får forverret økonomi handler det om tap.
											Tap av økonomisk trygghet og tap av materiell basis for
											livet.
										</p>
										<p>
											– Økonomiske problemer kan gi et slag mot selvfølelsen.
											Man spør seg: Hva er galt med meg, hvorfor får jeg det ikke
											til? Man kan føle at man mister anseelse, rykte og omdømme
											og følelsen av å være respektert. Det kan fort lede til tanker
											om at man er verdiløs og at ingen vil savne en hvis man
											blir borte. Eller man tenker: – Jeg er en belastning for alle, 
											kanskje det er best at jeg gir opp.
											Økonomi kan være en av kildene
											til å føle seg som en belastning,
											eller å ha lidd nederlag i livet. Det
											er når du har for lite penger, du
											oppdager hvor viktige de er. At
											man hver kveld lurer på hvordan
											man skal få betalt regninger, er en
											enorm belastning.
										</p>
										<p>
											At man ufrivillig måtte selge
											boligen fordi man ikke har råd til
											å bli boende, vil for de fleste være
											å forsterke nederlagsfølelsen.
										</p>
										<p>
											– Å selge hjemmet sitt vil for
											veldig mange være å rokke i
											grunnvollen i tilværelsen. Særlig
											hvis du har barn, og dette får store
											konsekvenser for hele familien.
											Det kan utløse hjelpeløshet, tap
											av kontroll og skyldfølelse. Det
											er mange kompliserte følelser
											som melder seg, uten at du ønsker
											det og uten at har kontroll, sier
											Mehlum.
										</p> 
									</div>  
								</div>

								<div class="row"> 
									<div class="col-md-8 colBorder">
										<div class="articles-subtitle">BRANSJENS ANSVAR</div>
										<p>
											Finansbransjen kommer jevnlig
											i kontakt med folk som er i
											risikosonen for å ta livet av seg på
											grunn av dårlig økonomi. Her må
											næringen ta et samfunnsansvar
											for å håndtere dette på en god
											måte. Professoren vil ha en satsing
											på forebygging og økonomisk
											rådgivning.
										</p>
										<p>
											– Vi må få på plass gode tiltak
											der folk får råd og hjelp til å styre
											pengene sine bedre. De fleste som
											har økonomiske problemer, tar ikke livet sitt, heldigvis.
											Likevel er det mange som sliter og som kan trenge hjelp.
											Samtidig bør de i næringen og som jevnlig kommer i
											kontakt med folk med dårlig økonomi, ha kunnskap om
											hva som er særlig risikofylt, og det vil være nyttig om de
											kan oppdage tegn på fare.
										</p>
										<p>
											– Man må være bevisst på at denne risikoen finnes. Det
											er gode, enkle kurs i hvordan oppdage selvmordsfare som
											burde være relevante for mange i bransjen. Det vil kunne
											hjelpe å få avklart om det er risiko, og hva man skal gjøre
											hvis man er i kontakt med en som man tror virkelig vil
											kunne ta livet av seg. Det blir på en måte førstehjelpskunnskap
											rundt selvmord. Organisasjonen Vivat Selvmordsforebygging
											tilbyr slike førstehjelpskurs over hele landet,
											så det er enkelt å få tilgang til.
										</p>
									</div>
									<div class="col-md-4">   
										<br>
										<div class="font-FlamaL mb0" style="background: #f69068 !important; padding: 20px; color: #fff; word-wrap: break-word;">
											<span class="font-FlamaB">TRENGER DU NOEN Å SNAKKE MED?</span>
											<br>

											<p class="noIndent font16"> 
												Ved akutt selvmordsfare ring 113<br>
												Legevakt: 116 117<br>
												Mental Helses hjelpetelefon: <br class="d-block d-md-none d-lg-block">116 123<br>
												Kors på halsen (Røde Kors) <br class="d-block d-md-none d-lg-block">800 33 321<br>
												Korspaahalsen.rodekors.no
											</p>
										</div>
									</div>
								</div> 
							</div> 		 
						</div>
					</div>
					<!--row-->
				</section> 
				<div class="spcr40"></div>
			</div>
			<!-- //PAGE -->  

			<!-- PAGE -->
			<div id="11" data-history="sammen-om-a-fjerne-skammen" class="swiper-slide">
				<title>Sammen om å fjerne skammen</title>
				<section id="team" class="topSpacer">
					<div class="wrapper articles-contentsection">
						<div class="container inner_container">
							<!-- articles-container -->
							<div class="articlesborder-section"> 	 
								<div class="articles-categorybox">
									<span class="font-FlamaL" style="color: #fff; background: #005091;">Nyheter</span> 
									<div class="articategory-content font-FlamaR">Gjeld og psykisk helse</div>
								</div>
								<br>  
								
								<h2 class="articles-heading font-FlamaB font50">
									Sammen om å fjerne skammen
								</h2> 
								<h3 class="intro-text content-social">
									En rekke studier viser klare sammenhenger mellom
									uhåndterlig gjeld, redusert livskvalitet, fysiske og
									psykiske helseplager. Å slite økonomisk er på for
									mange det siste tabuet i vårt moderne samfunn. Nå
									vil bankbransjen fjerne skammen.
								</h3>
								<div class="articles-textfotobox">
									Tekst: <span class="font-FlamaB">Helene Kleppe</span> 
								</div>
								<br>

								<figure class="search-thumbs">
									<img src="images/artiP16-banner.jpg" alt="" class="img-social">
								</figure>
								<div class="images-caption">
									<span class="font-FlamaB">VIKTIG Å DELE:</span> Konserndirektør SpareBank 1 SMN, Nelly Sundfør Maske, er opptatt av å dele erfaringer på dette området med bransjen. (FOTO: SPAREBANK 1 SMN)
								</div>  
								<br>

								<div class="row">
									<div class="col-md-8 colBorder">  
										<p class="noIndent">
											<span class="drop-txt font-FlamaT" style="color: #005091;">V</span>åre kunder har i utgangspunktet
											en krevende økonomisk
											situasjon, og inkluderer personer
											med mer dyptliggende
											problemer enn det man umiddelbart
											oppfatter som bank. Dermed spurte vi
											oss om hvordan vi som bank kunne ta
											KYC ett steg videre. Det dreier seg
											egentlig bare om god kundeservice, sier
											Diana Peters i Bank2 til Finansfokus.
										</p>
										<p>
											Tall fra Gjeldsregisteret viser at
											nordmenn i januar i år
											hadde samlet usikret
											forbruksgjeld for 160,5
											milliarder kroner, opp
											6,3 prosent sammenlignet
											med fjorårets
											januar. Om veksten
											blir like høy i år som
											i fjor, vil den samlede
											forbruksgjelden være
											tilbake på samme høye
											nivå som ved etableringen
											av gjeldsregistrene
											i 2019. I tillegg
											kommer sikret gjeld
											i et dramatisk endret
											økonomisk klima: De foreløpig 14
											renteøkningene vi har hatt siden 2021
											har gitt norske husholdninger rentesjokk.
											Etter nær 30 års fravær, er dyrtid
											og betalingsvansker nok en gang
											på agendaen for bankrådgiverne. Og i
											kjølvannet av betalingsvanskene følger
											skam og psykisk press.
										</p>
										<p>
											– Vi har kjørt opplæring på de ansatte,
											slik at de skal lære seg å se etter tegn på
											om kunder har omfattende økonomiske
											utfordringer og som ofte påvirker den
											psykiske helsen. Dersom vi klarer å
											fange opp kunder på et tidlig stadium,
											klarer vi å løse det aller meste, sier
											konserndirektør Nelly Sundfør Maske
											i SpareBank 1 Markets.
										</p>
										<p>
											Bank2, som har spesialisert seg på å
											gi lån til folk som av ulike årsaker har
											havnet i et økonomisk uføre, begynte
											å arbeide bevisst med
											sammenhengene
											mellom mental helse
											og økonomisk helse
											allerede våren 2021.
											Rådgiverne er drillet
											i å fange opp tegn på
											at kunden sliter, og de
											trener på samtaler med
											folk som har et veldig
											høyt stressnivå. Flere
											kommer nå etter, og i
											SpareBank 1 SMN har
											samtlige rådgivere
											gått gjennom to opplæringsmoduler
											som
											omfatter dette temaet. SMN har også
											et eget team med spisskompetanse på
											oppfølging av engasjement med mislighold.
										</p> 
									</div> 

									<div class="col-md-4 md-mt30">   
										<div class="quate-box font-FlamaB mt15 mb0"> 
											<span style="color: #005091;">”</span>Rådgiverne
											som har fått
											opplæring,
											opplever økt
											trygghet i møte
											med kunder
											som strever.<span style="color: #005091;">”</span> 
											<div class="name-onquate font-FlamaL">
												Diana Peters, Bank 2
											</div>
										</div> 
									</div>

									<div class="col-md-8 colBorder">
										<div class="articles-subtitle">PSYKOLOGHJELP STÅR KLAR</div>
										<p class="noIndent">
											Både Peters og Sundfør Maske understreker
											at rådgiverne ikke skal opptre
											som psykologer. Til de vanskeligste
											casene har begge bankene eksterne
											ekspert-team der gjeldsrådgiver og
											psykolog står klar til å lose kunden
											trygt gjennom.
										</p>
										<p>
											– Hittil har vi hatt rundt ti kunder
											som har fått hjelp av det eksterne rådgiverteamet.
											Vi ser at oppfølgingen har
											vært til hjelp, og synes det er veldig
											gledelig, sier Sundfør Maske.
										</p>
										<p>
											Nøkkelen til å fange opp kundene
											som sliter, er imidlertid god opplæring
											av rådgiverne. Erfaringene viser også at
											rådgiverne som har fått opplæring på
											denne tematikken, opplever økt trygghet
											i møte med kunder som strever.
										</p>
										<p>
											– Vi skal ha kunnskap om dette,
											vite hvordan vi skal ta de vanskelige
											samtalene og vite når vi skal henvise
											videre. Det er så tette sammenhenger
											mellom helse og økonomi, at her må
											vi bygge bro mellom våre bransjer, slår
											Peters fast.
										</p>
									</div>

									<div class="col-sm-12">
										<br>
										<div class="imgbox mb30"> 
											<a class="imgpopup" href="images/artiP16-sub1.jpg" data-fancybox="p16" data-caption="TIDLIG UTE: Leder Marked og Samfunnsansvar Diana Peters i Bank2 satte fokus på sammenhengene mellom mental helse og økonomisk helse våren 2021. (FOTO: BANK 2)">
												<img src="images/artiP16-sub1.jpg" alt="">
											</a> 
											<div class="images-caption">
												<span class="font-FlamaB">TIDLIG UTE:</span> Leder Marked og Samfunnsansvar Diana Peters i Bank2 satte fokus på sammenhengene mellom mental helse og økonomisk helse våren 2021. (FOTO: BANK 2)
											</div>
										</div>
									</div>

									<div class="col-md-8 colBorder">
										<div class="articles-subtitle mt0">VIL FJERNE SKAMMEN</div>
										<p class="noIndent">
											– Det er mye skam knyttet til økonomiske
											problemer, og vi ser ofte at kunden ikke
											søker hjelp før det er for sent. Derfor
											trener vi på å stille disse spørsmålene
											så tidlig som mulig. Jeg har snakket med
											rådgivere som har sittet i møter der hele
											korthuset ramler sammen, partneren er
											kanskje ikke informert om hele bildet,
											og rådgiver har vært redd for hva som
											kan skje når kunden går hjem, sier Nelly
											Sundfør Maske.
										</p>
										<p>
											Hun er opptatt av at bransjen i dette
											viktige arbeidet må dele erfaringer med
											hverandre, og ser det som ekstra aktuelt
											i tiden vi nå er inne i.
										</p>
										<p>
											– Vi har 30 år med oppgangstider
											bak oss, og de fleste rådgiverne i bankene
											har aldri tidligere opplevd dyrtid,
											slik vi ser nå. Vi som bransje må
											snakke mer om rådgivningen vi kan
											tilby, for vi klarer å finne løsninger for
											de aller fleste. Bankene kan hjelpe med
											avdragsfrihet, refinansiering og annet. 
											Men det handler mye om å få bukt med
											skammen for å komme tidlig i dialog
											med kundene, sier Sundfør Maske.
										</p>
										<p>
											Viktige bransjeorganisasjoner svarer
											også på utfordringene som oppstår
											for både forbrukere og bankansatte
											når kundene kommer på «glattisen».
											Siv Seglem, som er administrerende
											direktør i Finansnæringens autorisasjonsordninger
											(FinAut), bekrefter til
											Finansfokus at det å møte kunder med
											betalingsvansker, er høyt prioritert i
											arbeidet de gjør med å sikre høyt nivå
											på både kunnskap og etikk i bransjen.
										</p>
										<p>
											– Vi har over 10 000 autoriserte rådgivere
											som må ta oppdatering hvert år.
											Dette er et så viktig emne at vi kommer
											til å legge det inn i oppdateringen allerede
											i år. Alt læringsmateriale er ferdig
											1. mai, sier Seglem.
										</p> 
									</div>	

									<div class="col-md-4 md-mt30">   
										<div class="quate-box font-FlamaB mb10"> 
											<span style="color: #005091;">”</span>Ti kunder har fått hjelp av
											det eksterne rådgiverteamet.<span style="color: #005091;">”</span> 
											<div class="name-onquate font-FlamaL">
												Nelly Sundfør Maske, SpareBank 1 SMN
											</div>
										</div> 
									</div>

									<div class="col-md-8 colBorder">
										<div class="articles-subtitle">MØTER KUNDER MED EMPATI</div>
										<p class="noIndent">
											Kommunikasjon er et nøkkelverktøy
											for å nå ut til kunder som sliter. Purrebrev
											fra banken har vanligvis en streng
											form. Bank2 har vært bevisst på å gjøre
											purrebrevet mer empatisk. Resultatet
											av at de har lagt om kommunikasjonsstilen,
											er at flere tar kontakt når de får
											purring. Dermed får kunderådgiverne
											raskere tatt tak i problemene, som igjen
											gir større muligheter for en god løsning.
											Et annet eksempel der Bank 2 bevisst
											har lagt om kundekommunikasjonen,
											er i avslagsbrevene:
										</p>
										<p>
											– De aller fleste som søker lån hos
											oss får avslag, og de har
											allerede hatt en reise før
											dette avslaget. I avslagsbrevet
											forklarer vi alltid
											hvorfor en søker får
											avslag på lånesøknaden,
											hvordan de kan få bedre
											kontroll på økonomien, hva slags hjelp
											som finnes der ute og hva de har krav
											på. Det kan være Nav, Husbanken eller
											andre relevante instanser som kan være
											til hjelp for dem, sier Peters som er opptatt
											av at kommunikasjonen skal være
											empatisk og ivaretagende.
										</p>
										<p>
											Peters påpeker at det kan være mange
											årsaker til at kunder sliter. Angst og
											depresjon er ofte en faktor. Om det er
											depresjon eller dårlig økonomi som kommer
											først, er uvisst. Andre ganger er det
											livskriser som kan utløse problemene.
										</p>
										<p>
											– Økonomiske problemer er gjerne
											noe som rammer i kortere perioder.
											Ofte kan det være noe som har skjedd,
											en livskrise, som gjør at folk sliter med
											å administrere økonomien. Der kan vi
											som bransje hjelpe. Vi må forstå og ha
											empati for hvor vanskelig det er å stå i
											kriser, og vi må lære oss å snakke med
											folk. Det å bli møtt på en varm og ivaretakende
											måte, kan være det som gir
											mot til å ta telefonen til neste instans,
											sier hun.
										</p>
									</div>
								</div>
							</div> 			 
						</div>
					</div>
					<!--row-->
				</section> 
				<div class="spcr40"></div>
			</div>
			<!-- //PAGE -->

			<!-- PAGE -->
			<div id="12" data-history="sinker-i-det-sirkulaere-skiftet" class="swiper-slide">
				<title>Sinker i det sirkulære skiftet</title>
				<section id="team" class="topSpacer">
					<div class="wrapper articles-contentsection">
						<div class="container inner_container">
							<!-- articles-container -->
							<div class="articlesborder-section"> 	 
								<div class="articles-categorybox">
									<span class="font-FlamaL" style="color: #fff; background: #007e8c;">Bærekraft</span> 
									<div class="articategory-content font-FlamaR">Sirkulær økonomi</div>
								</div>
								<br>  
								
								<h2 class="articles-heading font-FlamaB font50" style="color: #007e8c;">
									Sinker i det sirkulære skiftet
								</h2> 
								<h3 class="intro-text content-social">
									Over halvparten av bedriftene i finansnæringen
									mangler konkrete mål for sirkulær økonomi. Bare én
									av tre virksomheter har inkludert sirkulærøkonomi
									som en del av sin strategi. Det viser en ny
									spørreundersøkelse fra Sirkulær Finanskoalisjon.
								</h3>
								<div class="articles-textfotobox">
									Tekst: <span class="font-FlamaB">Svein Åge Eriksen</span> Foto: <span class="font-FlamaB">Morten Brakestad</span>
								</div>
								<br>

								<figure class="search-thumbs">
									<img src="images/artiP18-banner.jpg" alt="" class="img-social">
								</figure>
								<div class="images-caption">
									<span class="font-FlamaB">TAR TID:</span> – Vi er vant til å forholde oss til en lineær økonomi, og da er det ikke gjort over natten å endre hele kretsløpet til en sirkulær økonomi, sier nestleder Arne Fredrik Håstein.
								</div>  
								<br>

								<div class="row">
									<div class="col-md-8 colBorder">  
										<p class="noIndent">
											<span class="drop-txt font-FlamaT" style="color: #007e8c;">D</span>et er et godt stykke igjen før
											bedriftene i finansnæringen tar
											steget fullt ut i en sirkulær økonomi.
											En undersøkelse i 20
											bedrifter viser at finansnæringen sitter
											på gjerdet i den sirkulære utviklingen.
											Det er fortsatt mye som gjenstår, ikke
											minst når det gjelder kompetansebygging.
											Hele 45 prosent av bedriftene i
											undersøkelsen har ikke gjennomført
											obligatoriske opplæringsprogrammer
											om sirkulær økonomi. Samtidig tror
											hele 85 prosent av bedriftene at
											finansnæringen i stor grad kan bidra til
											å fremme en sirkulær økonomi. Nå kommer
											et verktøy som gjør dette mulig.
										</p>  
									</div> 

									<div class="col-md-4 md-mt30">   
										<div class="quate-box font-FlamaB mt15 mb0"> 
											<span style="color: #007e8c;">”</span>Det er
											avgjørende at
											alle gode krefter
											samarbeider.<span style="color: #007e8c;">”</span>  
										</div> 
									</div>

									<div class="col-md-8 colBorder">
										<div class="articles-subtitle">VEIKART INSPIRERER</div>
										<p>
											Circular Norway har i samarbeid med
											Finansforbundet og Verdens Naturfond,
											WWF, laget et veikart for sirkulær
											finansnæring 2.0. Formålet med veikartet
											er å skape inspirasjon og motivasjon
											hos bedriftene i omstillingen til sirkulær
											økonomi.
										</p>
										<p>
											– Det er flere år siden vi første gang
											slo fast at finansnæringen har en avgjørende
											rolle i omstillingen til en sirkulær
											økonomi ved å vri kapitalen mot
											bærekraftige og sirkulære aktiviteter.
											I dag er det store forskjeller på hvordan
											bedriftene arbeider med sirkulær
											økonomi. Noen få er ganske gode og
											er godt i gang, mens mange andre ikke
											har en gang greid å implementere sirkulær
											økonomi i strategisk plan. En
											del europeiske finansinstitusjoner har
											kommet lenger enn det vi har i Norge,
											påpeker Arne Fredrik Håstein, nestleder
											i Finansforbundet
										</p>
										<p>
											Den største hindringen
											på finansnæringens
											vei mot sirkulær
											økonomi, er
											mangelen på data
											på bærekraftige
											forretningsmodeller
											og mangel på
											kompetanse. I veikartet
											blir det også
											understreket at det
											ikke finnes en klar oppskrift for hvordan
											finansnæringen skal integrere sirkulær
											økonomi i sin virksomhet. Mye er under
											utvikling, men det tar tid.
										</p>

										<div class="articles-subtitle">UTFORDRER MYNDIGHETENE</div>
										<p class="noIndent">
											– Hvis finansnæringen skal lykkes med
											omstillingen til sirkulærøkonomi, er det
											avgjørende at alle gode krefter samarbeider
											og myndighetene spiller på lag.
											Derfor har vi seks klare anbefalinger til
											myndighetene, sier Håstein.
										</p>
										<p>
											Han trekker frem to ting spesielt:
											Først og fremst må myndighetene sette
											ambisiøse og tydelige mål for effektiv
											omstilling til sirkulær økonomi. Finanssektoren
											har potensial til å være en katalysator
											for sirkulær omstilling i ulike
											sektorer, men da må myndighetene definerte
											mål og retning for dette arbeidet.
										</p>
										<p>
											Videre må myndighetene fremme
											sirkulære markeder ved å sette krav til
											offentlige anskaffelser. Med en offentlig
											innkjøpskraft på om lag 740 milliarder
											kroner årlig, er det avgjørende at offentlige
											anskaffelser stimulerer til utvikling
											av sirkulære markeder.
										</p>

										<div class="articles-subtitle">TRENGER DATA</div>
										<p class="noIndent">
											– En betydelig utfordring for finansnæringen
											er mangelen på tilgjengelig data
											for å vurdere i hvor stor grad et selskap
											eller aktivitet er sirkulær, og hvordan
											dette kan påvirke
											risikovurderingen.
											For å ta informerte
											beslutninger om
											effekten av sirkulære
											strategier, er
											det avgjørende å ha
											tilgang til kvalitetsdata.
											I tillegg trenger
											vi kompetanse
											og gode risiko- og
											prisingsmodeller på sirkulære forretningsmodeller
										</p>
										<p>
											Håstein vil gjerne at regjeringen
											inviterer finansnæringen inn i et klimapartnerskap
											med myndigheter og sektorspesifikke
											næringsorganisasjoner.
										</p>
										<p>
											– Det vil gjøre det mulig å bli enige
											om nødvendige tiltak og prioriterte
											områder. Konkret må vi vite hvilke data
											som må samles inn, hvilken effekt ulike
											tiltak vil ha, og hvilke virkemidler de
											ulike aktørene kan bidra med, avslutter
											nestlederen i Finansforbundet.
										</p>
									</div> 

									<div class="col-md-4 md-mt30">
										<div class="arti-listbox mb30" style="border-color: #007e8c;">
											<div class="arti-listheading redbeforebox" style="border-color: #007e8c; color: #007e8c;">
												Veikart
											</div> 
											<p class="noIndent font-FlamaR">	
												Rapporten: Veikart for sirkulær
												finansnæring 2.0 ble lansert på
												Den store sirkulærkonferansen
												i Asker kulturhus 14. mai.
												Veikartet er ment å inspirere
												bank, skadeforsikring og
												investeringer i omstillingen til
												en sirkulær økonomi. Veikartet
												inneholder fem pilarer og
												handlingspunkter som skal
												sette en retning for arbeidet.
												Rapporten er utarbeidet Circular
												Norway, Finansforbundet og
												Verdens naturfond, WWF
											</p> 
										</div>

										<div class="arti-listbox mt15 mb30" style="border-color: #007e8c;">
											<div class="arti-listheading redbeforebox" style="border-color: #007e8c; color: #007e8c;">
												Sirkulær koalisjon
											</div> 
											<p class="noIndent font-FlamaR">	
												Sirkulær finanskoalisjon er en
												samarbeidsarena med deltakere
												fra bank, forsikring og investering.
												De opparbeider seg kompetanse
												for å utvikle sirkulære produkter,
												tjenester og verktøy tilpasset klimaog
												miljømål og EUs taksonomi.
												Sirkulær finanskoalisjon ble etablert
												av Circular Norway i samarbeid med
												kunnskapspartnerne Finansforbundet
												og Verdens naturfond (WWF).
												Deltakere fra finansnæringen
												er: DNB, Fremtind, SpareBank1,
												Sparebanken Sør og Tryg.
											</p> 
										</div>

										<div class="arti-listbox mt15" style="border-color: #007e8c;">
											<div class="arti-listheading redbeforebox" style="border-color: #007e8c; color: #007e8c;">
												Sirkulær pådriver
											</div> 
											<p class="noIndent font-FlamaR">	
												Rapporten: Finansnæringen som en
												pådriver for en sirkulær økonomi ble
												lansert i april 2022 av Circular Norway
												og Finansforbundet: Denne rapporten
												inneholdt et sirkulært veikart med
												20 konkrete råd til finansnæringen.
												Én av anbefalingene var å etablere
												en sirkulær finanskoalisjon med
												representanter fra bank, forsikring
												og investering. I Finansfokus nr. 0222
												kan du lese alt du trenger å vite om
												sirkulærøkonomi. Her kan du se video
												om hva dette er og hvordan det kan
												påvirke din arbeidsplass.
											</p> 
										</div>
									</div>
								</div>
							</div> 			 
						</div>
					</div>
					<!--row-->
				</section> 
				<div class="spcr40"></div>
			</div>
			<!-- //PAGE -->

			<!-- PAGE -->
			<div id="13" data-history="sirkulaere-muligheter-i-finans" class="swiper-slide">
				<title>Sirkulære muligheter i finans</title>
				<section id="team" class="topSpacer">
					<div class="wrapper articles-contentsection">
						<div class="container inner_container">
							<!-- articles-container -->
							<div class="articlesborder-section"> 	 
								<div class="articles-categorybox">
									<span class="font-FlamaL" style="color: #fff; background: #007e8c;">Bærekraft</span> 
									<div class="articategory-content font-FlamaR">Sirkulær økonomi</div>
								</div>
								<br>  
								
								<h2 class="articles-heading font-FlamaB font50" style="color: #007e8c;">
									Sirkulære muligheter i finans
								</h2> 
								<h3 class="intro-text content-social">
									Finansnæringen har en enorm påvirkningskraft på veien
									mot en sirkulær økonomi, men kan ikke gjøre jobben alene.
									Myndighetene må også bidra for å legge til rette for bedre
									rammebetingelser og insentiver.
								</h3>
								<div class="articles-textfotobox">
									Tekst og foto: <span class="font-FlamaB">Svein Åge Eriksen</span>
								</div>
								<br>

								<figure class="search-thumbs">
									<img src="images/artiP20-banner.jpg" alt="" class="img-social">
								</figure>
								<div class="images-caption">
									<span class="font-FlamaB">VIKTIG PANELDEBATT:</span> Hvordan vri kapitalen mot en grønn, sirkulær økonomi? Det var overskriften på en paneldebatt etter at Veikart for sirkulær økonomi ble presentert. Fra venstre: Jostein Damminger, Tryg, Simen Kristiansen, SpareBank 1, Karoline Andaur, WWF og Arne Fredrik Håstein, Finansforbundet.
								</div>  
								<br>

								<div class="row">
									<div class="col-md-12">  
										<p class="noIndent">
											<span class="drop-txt font-FlamaT" style="color: #007e8c;">A</span> <span class="font-PalatinoB">SKER:</span> Den største utfordringen i
											dag er at det mangler klare mål for
											en sirkulær økonomi som gjelder alle
											bransjer. Men ikke alle er like begeistret
											for tanken om å redusere forbruket og
											materialfotavtrykket. Dette var konklusjonen
											i en paneldebatt der Veikart for sirkulær
											finansnæring 2.0 ble presentert. (Se side 18)
										</p>  

										<div class="articles-subtitle">PÅVIRKER KUNDENE</div>
										<p>
											–Tryg er Skandinavias største skadeforsikringsselskap
											og kjøper inn varer og tjenester
											for flere 10-talls milliarder i løpet av året. Vi
											har en enorm påvirkningskraft overfor hele
											vår verdikjede av leverandører og kunder,
											sier Jostein Damminger i Tryg.
										</p>
										<p>
											Det mest spennende er hvordan Tryg kan
											påvirke adferden til selskapets 5,7 millioner
											kunder. Det gjør de gjennom utforming av
											sine produkter og vilkår og hvilke intensiver
											de gir for å påvirke adferden til kundene. Det
											inspirerer og gir en god driv i det arbeidet
											forsikringsselskapet gjør innenfor sirkulærøkonomi.
										</p>
										<p class="font-PalatinoI">
											– Har du ett eksempel?
										</p>
										<p>
											Hele forsikringsnæringen handler om
											verdier og hvordan unngå skade. For 10-15 år
											siden fikk du sikkerhetsrabatt, hvis du parkerte
											bilen din i en garasje. Årsaken til at du fikk
											den rabatten, var tyverifare. Så forsvant tyverifaren
											og rabatten ble fjernet. Men nå vurderer
											vi å gjeninnføre sikkerhetsrabatten. Ikke på
											grunn av tyveri, men på grunn av klima. Hvis
											du parkerer bilen i garasjen, så er det mindre
											sannsynlig at du får en klimaskade på bilen.
										</p>
										<p>
											I skadeoppgjørene jobber også Tryg mye
											med hvordan øke gjenbruk av brukte bildeler,
											men det har fått en heller treg mottakelse hos
											kundene. For hva skjer med garantien på bilen
											ved bruk av brukte bildeler? Gjelder den fortsatt?
											Her er noen hindringer som må avklares.
										</p>

										<div class="articles-subtitle">80 000 M2 PARKETT</div>
										<p class="noIndent">
											–Parkett er ett annet eksempel. Bare i Norge
											legger Tryg 80 000 kvadratmeter parkett i året.
											Det er en god del av parketten som gjenbrukes
											siden vi oppfordrer våre leverandører til å ha
											en sirkulær tilnærming, forteller Damminger.
										</p>
										<p class="font-PalatinoI">
											– Tryg er et danskeid selskap. Er det noen forskjell
											på at Danmark er 4 prosent sirkulært, mens
											Norge bare er 2, 4 prosent sirkulært?
										</p>
										<p>
											– Det er Tryg i Norge som har kommet
											lengst i tenkingen og jobbingen mot en sirkulær
											økonomi, sier Damminger.
										</p>

										<div class="articles-subtitle">LITT FREMMED</div>
										<p class="noIndent">
											Simen Kristiansen, SpareBank 1 forteller at sirkulærøkonomi
											først og fremst først og fremst
											handler om bevisstgjøring, kompetanse og en
											helhetlig tilnærming. For å lykkes må både
											styret, konsernledelsen og de ansatte jobbe
											med sirkulærøkonomi.
										</p>
										<p>
											– Fortsatt er sirkulærøkonomi et umodent
											tema hos mange av våre små- og mellomstore
											bedrifter. Det er også litt fremmed for privatkundene
											å ta de riktige bærekraftige valgene.
											Men vi prøver å dytte våre kunder i riktig
											retning. Som rådgivere kan vi som bank bygge
											kompetanse og bevissthet å drive utviklingen
											på den måten. EUs regelverk og taksonomi
											er også viktige drivere for den videre bærekraftige
											utviklingen.
										</p>

										<div class="articles-subtitle">FORBRUKET MÅ NED</div>
										<p class="noIndent">
											Karoline Andaur fra Verdens naturfond,
											WWF, viste til at produksjon av varer og
											tjenester står for 90 prosent av naturtapet.
											Hun er tydelig på at forbruket må ned, men
											spørsmålet blir hvordan lykkes? Mange har
											ikke skjønt kraften i sirkulærøkonomi ved å
											redusere materialfotavtrykket ved ombruk.
											Andaur viser til at vi kan halvere utslippene
											våre hvis vi legger om til sirkulære løsninger.
										</p>
										<p>
											– Utfordringen er hvordan vi kan få en god
											samfunnsdebatt om hvordan vi kan skape
											verdi ved å forbruke mindre, uten at vi går
											ned i levestandard. Det er en lang vei å gå
											for å få til de nødvendige systemendringene
											for å kunne komme oss over i en reell sirkulærøkonomi.
											Finansnæringen kan sette
											krav til sine leverandører og hvordan man
											kan påvirke myndighetene gjennom den nye
											finanskoalisjonen som nå er etablert, understreker
											Andaur.
										</p>
										<p>
											Hun sier det er positivt at finansnæringen
											endrer praksis på hvordan de ser på sirkulærøkonomi,
											men tror vi trenger en større samtale
											om hvorfor er sirkulærøkonomi viktig. Litt for
											ofte pekes det på hva vi som forbrukere kan
											gjøre, uten å tenke på hvilke store systemer vi
											trenger for å få samfunnet dit vi vil.
										</p>
										<p>
											– Norge trenger å sette mål for konkrete
											mål for reduserte materialfotavtrykk. Myndighetene
											må sette like spilleregler for alle bransjer,
											inkludert finans. Det krever mot, fordi det
											handler om å få ned forbruket. Hvis vi skal
											leve innenfor planetens tåleevne, så krever det
											redusert fotavtrykk. Sirkulærøkonomi er et
											helt konkret verktøy for å oppnå det.
										</p>

										<div class="articles-subtitle">FOR FEIGE</div>
										<p class="noIndent">
											Arne Fredrik Håstein i Finansforbundet peker
											på et viktig poeng i slutten av paneldebatten.
										</p>
										<p>
											–Det jeg kanskje savner litt er at vi ikke
											greier å sette tydelige nok mål for hvor vi skal
											med sirkulærøkonomien. Vi er for feige til å
											ta det siste skrittet fullt ut. Her må vi i finans
											gå foran med et godt eksempel. Det har vi alle
											muligheter til å gjøre. Vi har kapital som er
											en superkraft som påvirker alt næringsliv og
											ikke minst, samfunnet for øvrig. Kompetanse
											er nøkkelen for å få det til.
										</p>
										<p>
											Han trekker tre ting som vil kunne få fart
											på sirkulærøkonomien. Det første er at det
											må settes tydelig mål og krav til næringslivet
											som myndighetene kan gjøre noe med. Det
											andre er at det vi må få litt mer fart på de
											740 milliardene vi bruker til offentlig innkjøp
											stille virkelig krav. Det tredje er nøkkelen til
											kompetanse både sirkulær og teknologiske
											kompetanse. Det har vi fått til gjennom bransjeprogrammet.
											Nå løfter vi hele laget inn i
											fremtiden.
										</p>
									</div> 
								</div>
							</div> 			 
						</div>
					</div>
					<!--row-->
				</section> 
				<div class="spcr40"></div>
			</div>
			<!-- //PAGE -->

			<!-- PAGE -->
			<div id="14" data-history="klimarisiko-kan-gi-sprik-i-boliglansmarkedet" class="swiper-slide">
				<title>Klimarisiko kan gi sprik i boliglånsmarkedet</title>
				<section id="team" class="topSpacer">
					<div class="wrapper articles-contentsection">
						<div class="container inner_container">
							<!-- articles-container -->
							<div class="articlesborder-section"> 	 
								<div class="articles-categorybox">
									<span class="font-FlamaL" style="color: #fff; background: #005091;">Bærekraft</span> 
									<div class="articategory-content font-FlamaR">Klimarisiko boliglån</div>
								</div>
								<br>  
								
								<h2 class="articles-heading font-opulent font50">
									Klimarisiko kan gi sprik i boliglånsmarkedet
								</h2> 
								<h3 class="intro-text content-social">
									Nytt EU-direktiv kan tvinge norske boligeiere
									til kostbar oppussing for å gjøre boligene mer
									energieffektive. Dette kan skape et stort sprik
									i boligmarkedet, mellom eldre, uisolerte
									boliger og nybygg.
								</h3>
								<div class="articles-textfotobox">
									Tekst: <span class="font-FlamaB">Hasse Farstad</span> 
								</div>
								<br>

								<figure class="search-thumbs">
									<img src="images/artiP22-banner.jpg" alt="" class="img-social">
								</figure>
								<div class="images-caption">
									<span class="font-FlamaB">ØKT KREDITTRISIKO:</span> – EU-reglene kan medføre en økning i kredittrisiko generelt, sier Hilde Nordbø i Handelsbanken.
								</div>  
								<br>

								<div class="row">
									<div class="col-md-8 colBorder">  
										<p class="noIndent">
											<span class="drop-txt font-FlamaT" style="color: #005091;">H</span> vordan dette vil se ut i Norge
											er vanskelig å forutsi, men det
											vi vet er at eiere av boliger og
											næringseiendom vil måtte
											investere i forbedringstiltak, sier Linn
											Kristine Gjesdal, leder for bærekraft i
											Sparebanken Vest.
										</p>
										<p>
											Hun legger til at banken i sin rådgiverrolle
											må å ta høyde for dette. Samtidig
											vet man at det er mer bærekraftig
											å ta vare på det man har, enn å bygge
											nytt. Størsteparten av dagens boliger,
											skal fremdeles bebos i 2050.
										</p>
										<p>
											Seniorøkonom Sara Midtgaard i Handelsbanken
											Capital Markets kom i fjor
											høst med en advarsel om et fremtidig
											stort sprik i det norske boligmarkedet.
											I en kronikk i DN 21. august viste hun
											blant annet til EU-direktivets krav om
											rehabilitering av bygningsmassen og at
											alle bygg skal være klimanøytrale innen
											2050.
										</p>
										<p>
											– Det er viktig for bankene å følge
											med på endringer i det regulatoriske
											landskapet. I mars 2023 vedtok Europaparlamentet
											det nye bygningsdirektivet,
											og i desember kom Europarådet til enighet.
											I dette reviderte bygningsdirektivet
											ser vi nå at det legges opp til en rekke
											nasjonale tilpasninger, sier Linn Kristine
											Gjesdal.
										</p> 

										<div class="articles-subtitle">FOKUS PÅ BEDRIFTSPORTEFØLJEN</div>
										<p class="noIndent">
											Hilde Nordbø, direktør for bærekraft i
											Handelsbanken, sier at de kommende
											EU-reglene kan medføre en økning i
											kredittrisiko generelt, ettersom for
											eksempel bygningsenergidirektivet setter
											strengere krav til bygg, og at det må
											store investeringer til for å oppgradere
											bygg med svakt energimerke.
										</p>
										<p>
											– Når det gjelder klimarisiko og bankens
											utlånsportefølje, har vi hatt størst
											fokus på bedriftsporteføljen, sier Hilde
											Nordbø til Finansfokus.
										</p>
										<p>
											Å få inn energiattesten har vært ett
											av bankens virkemidler for å kunne
											vurdere hva slags klimarisiko banken
											står overfor, selv om dette ikke gir et
											helhetlig bilde. Nordbø viser også til at
											Handelsbanken gjennom sin rådgiverrolle
											kan guide kunder som bor i boliger
											med oppgraderingsbehov, til ulike
											støtteordninger fra det offentlige.
										</p>
									</div> 

									<div class="col-md-4 md-mt30">   
										<div class="imgbox mb30"> 
											<a class="imgpopup" href="images/artiP22-sub1.jpg" data-fancybox="p22" data-caption="">
												<img src="images/artiP22-sub1.jpg" alt="">
											</a> 
										</div>
										<div class="quate-box font-FlamaB mt15 mb0"> 
											<span style="color: #005091;">”</span>Økte energipriser
											vil føre til økte
											prisforskjeller på
											energieffektive og
											energiineffektive
											boliger.<span style="color: #005091;">”</span> 
											<div class="name-onquate font-FlamaL">
												Kristian Ruth, Finans Norge.
											</div>
										</div> 
									</div>

									<div class="col-md-8 colBorder">
										<div class="articles-subtitle">FRIHETSGRADER FOR NORGE</div>
										<p class="noIndent">
											Kristian Ruth, direktør for bærekraft i
											Finans Norge, viser til at EUs bygningsenergidirektiv
											har fått en litt annen form
											enn det som forelå tidligere i prosessen.
										</p>
										<p>
											– I endelig versjon er det større frihetsgrader
											for medlemslandet for hvordan
											det skal nå energieffektiviseringsmålene.
											Hvordan norske politikere
											kommer til å innrette politikken for at
											vi i Norge skal nå målene, ønsker jeg
											ikke å spekulere i, sier Kristian Ruth.
										</p>
										<p>
											Sparebanken Vest opplever ifølge
											Gjesdal store endringer med hensyn til
											hvordan banken vurderer klimarisikoen
											ved lån til eldre boliger som må oppgraderes
											– særlig i et porteføljeperspektiv.
										</p>
										<p>
											Hun betviler at klimarisiko og nye
											EU-krav til bygninger vil gi noen direkte
											eller indirekte stimulans til bygging av
											nye boliger i et to–tre-års perspektiv.
										</p>
									</div> 
								</div>

								<div class="row">
									<div class="col-md-4 order-md-2"> 
										<br>  
										<div class="imgbox mb30"> 
											<a class="imgpopup" href="images/artiP22-sub2.jpg" data-fancybox="p22" data-caption="">
												<img src="images/artiP22-sub2.jpg" alt="">
											</a> 
										</div>
										<div class="quate-box font-FlamaB mt15 mb0"> 
											<span style="color: #005091;">”</span>Eiere av boliger og
											næringseiendom vil
											måtte investere i
											forbedringstiltak.<span style="color: #005091;">”</span> 
											<div class="name-onquate font-FlamaL">
												Linn Kristine Gjesdal, Sparebanken Vest.
											</div>
										</div> 
									</div>

									<div class="col-md-8 colBorder order-md-1">
										<div class="articles-subtitle">KOSTNADER OGSÅ FOR NYE BYGG</div>
										<p class="noIndent">
											Fordi reguleringene gir økte kostnader
											for eksisterende bygg, må kundene
											investere i energieffektiviserende tiltak,
											men også i nybygg, for å oppfylle en
											rekke krav for å være innenfor kriteriene
											til nybygg i EUs taksonomi.
										</p>
										<p>
											– Vi tror at det vil bli større fokus på
											energimerker ved salg av boliger. Det
											vil være naturlig, siden energimerke og
											strømforbruk henger tett sammen – og
											man merker det på lommeboken, sier
											Linn Kristine Gjesdal.
										</p>
										<p>
											Kristian Ruth sier at økte strømpriser
											fra 2022 har vært med på å rette søkelyset
											mot samfunnsøkonomisk gunstig
											energieffektivisering av eksisterende
											bygningsmasse, både for privatpersoner,
											profesjonelle byggeiere og banker.
										</p>
										<p>
											– Dersom man forutsetter rasjonelle
											aktører i et marked, vil økte energipriser
											føre til økte prisforskjeller på energieffektive
											og energiineffektive boliger, sier
											Ruth.
										</p>

										<div class="articles-subtitle">BEDRE BETINGELSER</div>
										<p class="noIndent">
											For å forberede seg på utviklingen også
											etter at EU-direktivet trer i kraft i Norge,
											har Sparebanken Vest i løpet av de siste
											to årene lansert to nye produkter: et
											grønt oppgraderingslån, som er et produkt
											for dem som ønsker å gjøre større
											energieffektiviserende tiltak på boligene
											sine, og energilån, som er et produkt for
											dem som ønsker å gjøre mindre energieffektiviserende
											tiltak.
										</p>
										<p>
											– Dette gjør kundene våre i stand til
											å ta bærekraftige valg på sine egne premisser,
											slik at de står bedre rustet i møte
											med strengere regulering i omstillingen
											til et lavutslippssamfunn, sier Gjesdal.
										</p>
										<p>
											Finans Norge-direktør Ruth viser til
											at landets banker tar egne strategiske
											beslutninger: – Men vi ser at en god del
											banker bygger mye kompetanse på
											energieffektivisering. Mange har også
											produkter med bedre betingelser for å
											stimulere energieffektivisering, sier han.
										</p>
									</div>
								</div>
							</div> 			 
						</div>
					</div>
					<!--row-->
				</section> 
				<div class="spcr40"></div>
			</div>
			<!-- //PAGE -->

			<!-- PAGE -->
			<div id="15" data-history="kvinnehelse-kan-ikke-lenger-gronnvaskes" class="swiper-slide">
				<title>Kvinnehelse kan ikke lenger grønnvaskes</title>
				<section id="team" class="topSpacer">
					<div class="wrapper articles-contentsection">
						<div class="container inner_container">
							<!-- articles-container -->
							<div class="articlesborder-section"> 	 
								<div class="articles-categorybox">
									<span class="font-FlamaL" style="color: #fff; background: #005091;">Nyheter</span> 
									<div class="articategory-content font-FlamaR">Kvinnehelse</div>
								</div>
								<br>  
								
								<h2 class="articles-heading font-FlamaB font50">
									Kvinnehelse kan ikke lenger grønnvaskes
								</h2> 
								<h3 class="intro-text content-social">
									Når vi lar kvinnehelse seile under radaren taper
									norsk næringsliv penger - og kvinner seiler akterut
									karrieremessig. Et regjeringsoppnevnt utvalg og et
									EU-direktiv blir en gamechanger.
								</h3>
								<div class="articles-textfotobox">
									Tekst: <span class="font-FlamaB">Helene Kleppe</span> 
								</div>
								<br>   
								<br>

								<div class="row">
									<div class="col-md-8 colBorder">  
										<p class="noIndent">
											<span class="drop-txt font-FlamaT" style="color: #005091;">V</span> i liker å tro vi er gode på likestilling,
											og vi er gode på tradisjonell
											likestilling, men i min karriere
											har jeg lært meg å snakke
											«mannsk». Da glemmer vi kanskje at
											litt av styrken vår også er den feminine
											siden. Når vi har jobbet oss oppover på
											karrierestigen synes vi kanskje det er
											flaut å snakke om ting som oppleves
											sårbart. Jeg møter mange ledere som
											sier de ikke vil fronte kvinnehelse selv.
											Det er fortsatt et stigma rundt dette, sier
											Anita Hegge. Hun er gründer av konsulentselskapet
											Hvild, og bistår bedrifter
											med å inkludere kvinnehelse i ESGmålene.
										</p>
										<p>
											– Kvinnehelse er en del av bærekraftsmålene.
											Frem til nå har det nok
											vært mer ord enn handling, men med
											EU direktivet CSRD (Corporate Sustainability
											Reporting Directive) må det skje
											en endring. Nå må man rapportere på
											dette. Og mange ser kanskje at de har
											et høyere fravær, spesielt blant de eldre
											kvinnene, der må man nå gjøre en jobb,
											sier Hegge.
										</p>
										<p>
											På oppfordring fra Kvinnehelseutvalget
											har Regjeringen satt ned et utvalg
											som skal se på kvinners arbeidshelse. I
											tillegg får kvinnehelse i arbeidslivet også
											viktig drahjelp av EUs CSRD-direktiv og
											de nye ESRS-rapporteringsstandardene.
											To av bærekraftsmålene er relatert til
											likestilling, og bedrifter som velger disse
											bærekraftsmålene vil også måtte se på
											kvinnehelse. En rekke internasjonale
											rapporter dokumenterer nemlig at det
											er knyttet til både likestilling gjennom
											å styrke jenters og kvinners stilling i
											samfunnet, og til det å fremme varig,
											inkluderende og bærekraftig økonomisk
											vekst, full sysselsetting og anstendig
											arbeid for alle.
										</p>
										<p>
											– Kvinnehelse kan ikke lenger
											grønnvaskes. Det må settes inn tiltak i
											forbindelse med sykmeldinger. Kvinnehelse
											må inn i HR-strategien, og bedriftene
											må finne kvantifiserbare mål de
											skal jobbe med, sier Hegge.
										</p> 

										<div class="articles-subtitle">FINANSBRANSJEN PÅ BANEN</div>
										<p class="noIndent">
											I forbindelse med markeringen av
											kvinnedagen, løftet Gjensidige frem
											viktigheten av kvinnehelse både gjennom
											arrangementer internt og gjennom
											ulike artikler ut mot kundene.
										</p>
										<p>
											– Det interne arrangementet hadde
											særlig fokus på overgangsalder og et
											forskningsprosjekt i regi av Universitetet
											i Bergen. Vi oppfordrer både ledere og
											medarbeidere til å
											tørre å snakke om
											kvinnehelse. Skal
											vi som arbeidsgiver
											kunne tilrettelegge
											der det
											måtte være behov
											for det, krever det
											også at medarbeidere
											snakker med
											lederen sin. Dette
											gjelder all form for
											tilrettelegging, ikke
											bare knyttet opp til
											kvinnehelse, sier konserndirektør People
											i Gjensidige, Siri Langangen.
										</p>
										<p>
											Gjensidige har også utarbeidet informasjonssider
											om kvinnehelse hvor de
											både gir informasjon og oppfordrer
											ledere og medarbeidere til åpen og god
											dialog rundt temaet. Her vises det også
											til aktuelle podkaster, bøker og artikler.
										</p>
										<p>
											– Vi skal videreutvikle informasjonssidene
											våre, og planlegger flere interne
											arrangementer, blant annet knyttet til
											syklisk helse og infertilitet. Her blir det
											også viktig å sette våre ledere i stand til
											å snakke med medarbeidere om temaet
											og avdekke mulige behov for tilrettelegging,
											sier Langangen om satsingen.
										</p>
										<p>
											Konserndirektør for People i Storebrand,
											Tove Selnes, forteller Finansfokus
											i en e-post at de innen kvinnehelse
											allerede lenge har hatt fokus på å legge
											til rette for medarbeidere under svangerskapet
											og i, under og etter foreldrepermisjon.
											I likhet med flere andre større
											aktører vil også Storebrand i løpet av
											året som kommer sette overgangsalderen
											på agendaen.
										</p>
									</div> 

									<div class="col-md-4 md-mt30">   
										<div class="imgbox mb30"> 
											<a class="imgpopup" href="images/artiP24-banner.jpg" data-fancybox="p24" data-caption="HINDRER KARRIEREUTVIKLING: Plager relatert til overgangsalderen går på selvtilliten løs. Hvild-gründer Anita Hegge viser til at rundt en tredjedel av kvinnene som har svart på deres kvinnehelseundersøkelse har unnlatt å søke seg videre med karrieren på grunn av slike plager. Omtrent like mange vurdert å slutte i sin nåværende stiling som følge av symptomer fra overgangsalder.">
												<figure class="search-thumbs">
													<img src="images/artiP24-banner.jpg" alt="" class="img-social">
												</figure>
											</a> 
											<div class="images-caption">
												<strong>HINDRER KARRIEREUTVIKLING:</strong> Plager relatert til overgangsalderen går på selvtilliten løs. Hvild-gründer Anita Hegge viser til at rundt en tredjedel av kvinnene som har svart på deres kvinnehelseundersøkelse har unnlatt å søke seg videre med karrieren på grunn av slike plager. Omtrent like mange vurdert å slutte i sin nåværende stiling som følge av symptomer fra overgangsalder.
											</div>
										</div>
										<div class="quate-box font-FlamaB mt15 mb0"> 
											<span style="color: #005091;">”</span>Ved plager som hjernetåke, mister man selvtilliten og tør ikke søke forfremmelse eller drømmejobben.<span style="color: #005091;">”</span> 
											<div class="name-onquate font-FlamaL">
												Anita Hegge
											</div>
										</div> 
									</div>

									<div class="col-md-8 colBorder">
										<div class="articles-subtitle">TØRSTER ETTER KUNNSKAP</div>
										<p class="noIndent">
											– I Norge er vi ennå der at vi jobber med
											kunnskap om dette, og vi har begynt å
											se hvordan vi skal ta tak i kvinnehelse,
											sier Hegge.
										</p>
										<p>
											I løpet av tiden som har gått fra i fjor
											sommer og frem til i dag har Hegge merket
											seg at det har kommet et klimaskifte
											i debatten. Selv om vi ligger langt etter
											land som Sverige og Storbritannia på
											dette området, opplever hun at nysgjerrigheten
											rundt kvinnehelse og arbeidsliv
											er stor.
										</p>
										<p>
											– Når vi har
											et temamøte om
											kvinnehelse på en
											arbeidsplass opplever
											vi ofte at folk
											lurer på om det
											kommer noen. De
											tenker gjerne at det
											kanskje kommer
											30 stykker - men så
											sitter det plutselig
											150 i salen. Det er
											ikke bare kvinner i
											alle aldre, menn er
											også nysgjerrige på dette.
										</p>
										<p>
											Noen av de større arbeidsgiverne er
											allerede i gang med å innlemme kvinnehelse
											i hms-arbeidet. Og der Hvild
											engasjeres som konsulent starter jobben
											med å øke kompetansen og bevisstheten
											blant de ansatte. Derfra er det ledertreninger
											og sertifisering av verneombud.
										</p>
										<p>
											– Det går på alt fra å gjøre seg opp
											tanker om hva inkluderende lederskap
											er, til å skape det trygge rommet der
											lederne er komfortable med å inkludere
											kvinnehelse i samtalene.
										</p>

										<div class="articles-subtitle">KARRIERER STOPPER OPP</div>
										<p>
											– Utfordringene er at vi har snakket lite 
											om fertilitetsløp og overgangsalder for
											eksempel. Begge deler har symptomer
											som i høyeste grad påvirker en kognitivt
											og mentalt, og i en kompetansekrevende
											jobb blir det krevende, påpeker Hegge.
										</p>
										<p>
											Hvild-undersøkelsen viser at kvinner
											stort sett ikke snakker med arbeidsgiver
											om overgangsalderen og plager relatert
											til dette. Og av de som har valgt åpenhet
											opplever færre enn to av ti at arbeidsgiver
											tar plagene på alvor. På kontoret
											til fastlegen er de fleste kvinner åpne.
											Likevel opplever kun halvparten at de
											har fått en bedre arbeidshverdag etter å
											ha fulgt legens råd. Nær 20 prosent av
											de som har snakket med legen sin om
											slike plager fikk ikke råd i det hele tatt.
										</p>
										<p>
											– Som konsekvens av plager som hjernetåke,
											mister man selvtilliten og tør ikke
											søke den forfremmelsen eller drømmejobben,
											sier Hegge og viser til at det
											gjelder nesten en tredjedel av kvinnene
											som har svart på Hvild-undersøkelsen.
										</p>
										<p>
											I tillegg til de mange som ikke tør
											søke, har omtrent like mange vurdert
											å slutte i sin nåværende stiling som
											følge av symptomer fra overgangsalder.
											Halvparten av respondentene har hatt
											fravær på jobb på grunn av plager fra
											overgangsalder.
										</p>
									</div> 

									<div class="col-md-4">
										<br>   
										<div class="imgbox mb30"> 
											<a class="imgpopup" href="images/artiP24-sub1.jpg" data-fancybox="p24" data-caption="LIKE MULIGHETER: – Vi har som mål å ha en inkluderende bedriftskultur hvor alle skal oppleve å bli behandlet med respekt og likeverd. I dette ligger at vi ikke bare skal snakke om inkludering, men vise at vi bryr oss om hverandre og praktiserer inkludering i arbeidshverdagen. Det å behandle alle likt er ikke et mål i seg selv. Dette fordi vi er så forskjellige, sier Siri Langangen som er konserndirektør People hos Gjensidige.">
												<img src="images/artiP24-sub1.jpg" alt="">
											</a> 
											<div class="images-caption">
												<strong>LIKE MULIGHETER:</strong> – Vi har som mål å ha en inkluderende bedriftskultur hvor alle skal oppleve å bli behandlet med respekt og likeverd. I dette ligger at vi ikke bare skal snakke om inkludering, men vise at vi bryr oss om hverandre og praktiserer inkludering i arbeidshverdagen. Det å behandle alle likt er ikke et mål i seg selv. Dette fordi vi er så forskjellige, sier Siri Langangen som er konserndirektør People hos Gjensidige.
											</div>
										</div>
										<div class="quate-box font-FlamaB mt15 mb0"> 
											<span style="color: #005091;">”</span>Vi oppfordrer
											både ledere og
											medarbeidere til
											å tørre å snakke
											om kvinnehelse.<span style="color: #005091;">”</span> 
											<div class="name-onquate font-FlamaL">
												Siri Langangen
											</div>
										</div> 
									</div>

									<div class="col-sm-12"> 
										<br>
										<div class="arti-listbox mt30" style="border-color: #005897;">
											<div class="arti-listheading redbeforebox" style="border-color: #005897; color: #005897;">
												FAKTA KVINNEHELSE
											</div> 
											<ul style="padding-inline-start: 40px;" class="font-FlamaL">
												<li>
													I 2023 var sykefraværet
													for kvinner på 8,8
													prosent, mot 5,4 prosent
													hos menn. Forskjellen
													i sykefravær mellom
													kjønnene er bakgrunnen
													for at Kvinneutvalget
													ba regjeringen sette
													ned et utvalg som skal
													gi mer kunnskap og
													foreslå tiltak for å bedre
													kvinners arbeidshelse
													og tilknytning til
													arbeidslivet. Dette
													utvalget ble lansert 8.
													mars.
												</li>
												<li>	
													I undersøkelsen
													“Kvinnehelse på jobb” i
													regi av Hvild, svarer 73
													prosent svarer at de ikke
													tror arbeidsgiver har en
													kvinnehelsestrategi og
													27 prosent svarer at det
													er de usikre på. Det vil si
													at null prosent sier at de
													tror deres arbeidsgiver
													har en strategi på dette.
												</li>
												<li>	
													Norske bedrifter er i
													kunnskapsfasen på å
													innlemme kvinnehelse
													i ESG-målene. I løpet
													av det siste året har
													oppmerksomheten rundt
													temaet økt kraftig.
												</li>
												<li>	
													En rapport fra Axa
													Health og Centre
													of Economics and
													Business Research
													(CEBR) anslår at det å
													ignorere kvinnehelse
													på arbeidsplassen
													kan koste den britiske
													økonomien 20,2
													milliarder pund årlig. Det
													finnes ikke tilsvarende
													norske tall.
												</li>
											</ul>
										</div>
									</div>
								</div> 
							</div> 			 
						</div>
					</div>
					<!--row-->
				</section> 
				<div class="spcr40"></div>
			</div>
			<!-- //PAGE -->

			<!-- PAGE -->
			<div id="15" data-history="skal-skape-endring" class="swiper-slide">
				<title>Skal skape endring</title>
				<section id="team" class="topSpacer">
					<div class="wrapper articles-contentsection">
						<div class="container inner_container">
							<!-- articles-container -->
							<div class="articlesborder-section"> 	 
								<div class="articles-categorybox">
									<span class="font-FlamaL" style="color: #fff; background: #e36e59;">Min arbeidsplass</span> 
									<div class="articategory-content font-FlamaR">Forretningsutvikler</div>
								</div>
								<br>  
								
								<h2 class="articles-heading font-FlamaB font50">
									Skal skape endring
								</h2> 
								<h3 class="intro-text content-social">
									Forretningsutvikler er en til av disse diffuse
									titlene som egentlig ikke sier så mye hva
									ansatte driver med. Så jeg tok kontakt
									med Juni Lynngård som jobber som
									forretningsutvikler i Tryg, for å bli litt klokere.
								</h3>
								<div class="articles-textfotobox">
									Tekst: <span class="font-FlamaB">Sjur Anda</span> 
								</div>
								<br>

								<figure class="search-thumbs">
									<img src="images/artiP26-banner.jpg" alt="" class="img-social">
								</figure>
								<div class="images-caption">
									<span class="font-FlamaB">BÆREKRAFT I ALT:</span> Juni Lynngård jobber mye med bærekraft. – Vi må tenke på bærekraft også når vi jobber med andre områder.
								</div>  
								<br>

								<div class="row">
									<div class="col-md-8 colBorder">  
										<p class="noIndent">
											<span class="drop-txt font-FlamaT" style="color: #e36e59;">B</span> <span class="font-PalatinoB">ERGEN:</span> Vi spoler tilbake
											til 2020. Juni hadde akkurat
											levert sin masteroppgave i
											sammenlignende politikk
											ved Universitetet i Bergen. Etter
											sommerferien startet hun som trainee
											i Tryg via TraineeVest, Bergens
											næringsråds traineeprogram. Hun
											havnet i forretningsområde Skade
											i avdelingen for Strategi og Endring.
										</p>
										<p>
											– Jeg kom da de holdt på å formulere
											strategi for 2021–2024 og ble
											involvert i det arbeidet. Jeg har fått
											være med på alle stegene i prosessen
											og være med på å iverksette dette i
											hele organisasjonen, forteller Lynngård
											om sine første år i Tryg. Nå
											er de i slutten av strategiperioden.
										</p>
										<p>
											– Det er gøy å måle resultatene
											av de initiativene vi har jobbet med.
										</p> 

										<div class="articles-subtitle">JOBBER MED ENDRING</div>
										<p class="noIndent">
											For å være forretningsutvikler
											handler om endring og utvikling.
										</p>
										<p>
											– Vi finner gode initiativer
											sammen og jobber for å få skadebehandlere
											til å arbeide på en ny
											måte. Det er alt fra små endringer
											på arbeidsmetode til innføring
											av nytt skadesystem. Det siste er
											kanskje den største endringen, der
											alle skal inn i et nytt system og må
											ha nok kompetanse til å håndtere
											dette, sier Lynngård og fortsetter:
										</p>
										<p>
											– Innføring av et nytt system krever
											mye endring i måten man jobber
											på. Ansatte kjenner det gamle
											systemet og er trygge der, og ikke
											alle føler like stor trygghet rundt
											digitale endringer. Vi brukte mye
											tid på at de ansatte skulle få eierskap
											til systemet.
										</p>
									</div> 

									<div class="col-md-4 md-mt30">    
										<div class="quate-box font-FlamaB mt15 mb0"> 
											<span style="color: #e36e59;">”</span>Vi må ha på bærekrafthatten.<span style="color: #e36e59;">”</span> 
											<div class="name-onquate font-FlamaL">
												Kristian Ruth, Finans Norge.
											</div>
										</div> 
									</div>

									<div class="col-md-8 colBorder">
										<div class="articles-subtitle">BÆREKRAFTIGE ENDRINGER</div>
										<p class="noIndent">
											Lynngård har også jobbet mye med
											bærekraft, men da på et mer overordnet
											nivå.
										</p>
										<p>
											– Det handler om å skape
											bevissthet i organisasjonen. Alle
											har hørt om bærekraft, men det er
											veldig forskjellig
											hva man legger i
											det. Spørsmålet vi
											stilte var hva det
											er for oss i Tryg og
											oss i Skade. Hvor
											kan vi gjøre en forskjell,
											og hvordan
											skal det integreres
											i beslutninger vi tar? Vi må ha på
											bærekrafthatten når vi jobber med
											utvikling på andre områder. Mange
											er bekymret for at bærekraftige
											løsninger ikke er like gode, og at
											man får et dårligere resultatet. Det
											er ikke min erfaring.
										</p>
										<p>
											Lynngård har blant annet ansvar
											for en gruppe med bærekraftagenter.
										</p>
										<p>
											– Dette er folk fra ulike områder
											innen skade, fra bil til næringsbygg
											og reise. Vi har fått et godt samarbeid
											med markedsføringsavdelingen
											for få ut gode forebyggende
											budskap om forebygging. Det er
											mye kunnskap her på huset om
											hvilke skader som skjer, og hvordan
											de kunne vært unngått. Vi har
											eksempelvis fått på plass korte
											videoer på Instagram og Facebook
											som har blitt veldig bra.
										</p>

										<div class="articles-subtitle">VARIERT ARBEIDSDAG</div>
										<p class="noIndent">
											Lynngårds arbeidsdager er varierte,
											men arbeidsprosessen ligner litt på
											den vitenskapelige metoden. Man
											har en hypotese, og gjennom tester
											får vi sjekket om de stemmer eller
											ikke.
										</p>
										<p>
											– Det er en prøve- og feilekultur
											her. Det liker jeg godt. Samtidig er
											det tilfredsstillende å se resultatene
											av ting vi har jobbet med. Til
											syvende og sist handler det om å
											få til forandringer som gjør Tryg
											bedre. Vi har stor
											frihet til å finne
											gode løsninger,
											sier Lynngård.
										</p>
										<p class="font-PalatinoI">
											– Hva er det
											verste med jobben?
										</p>
										<p>
											– Jeg elsker
											jobben min. Den
											er veldig prosjektorientert,
											der ting blir avsluttet.
											Samtidig har jeg gode kolleger å
											spille på. Det er alltid noen jeg kan
											spørre om hjelp. Av og til skulle jeg
											ønske at jeg kunne sende tankene
											mine rett inn i PC-en og få ut ferdige
											powerpointer. Jeg gleder meg
											til kunstig intelligens blir litt bedre
											og kan hjelpe meg med dette.
										</p>
									</div> 

									<div class="col-md-4">
										<br>
										<div class="font-FlamaL mb30" style="background: #e36e59 !important; padding: 20px; color: #fff; word-wrap: break-word;">
											<span class="font-FlamaB font22">FAKTA:</span>
											<br>

											<p class="noIndent font16">
												Juni Lynngård
												<br>

												<span class="font-FlamaB">Tittel:</span>
												Forretningsutvikler
												<br>

												<span class="font-FlamaB">Alder:</span> 28
												<br>

												<span class="font-FlamaB">Utdanning:</span> Master
												i sammenlignende
												politikk
											</p>
										</div>
									</div>
								</div> 
							</div> 			 
						</div>
					</div>
					<!--row-->
				</section> 
				<div class="spcr40"></div>
			</div>
			<!-- //PAGE -->

			<!-- PAGE -->
			<div id="16" data-history="ansatte-i-sparebanken-vest-testet-digitalt-utenforskap-pa-egen-kropp" class="swiper-slide">
				<title>ANSATTE I SPAREBANKEN VEST: Testet digitalt utenforskap på egen kropp</title>
				<section id="team" class="topSpacer">
					<div class="wrapper articles-contentsection">
						<div class="container inner_container">
							<!-- articles-container -->
							<div class="articlesborder-section"> 	 
								<div class="articles-categorybox">
									<span class="font-FlamaL" style="color: #fff; background: #005091;">Nyheter</span> 
									<div class="articategory-content font-FlamaR">Digitalt utenforskap</div>
								</div>
								<br>  
								
								<h2 class="articles-heading font-opulent font50" style="color: #005091;">
									<span class="font-FlamaB d-block font20 mb10" style="color: #000;">ANSATTE I SPAREBANKEN VEST:</span>
									Testet digitalt
									utenforskap på
									egen kropp
								</h2> 
								<h3 class="intro-text content-social">
									Ansatte i IT-avdelingen på hovedkontoret til
									Sparebanken Vest testet digitalt utenforskap på
									egen kropp. Gjennom tolv stasjoner fikk de personlig
									oppleve ulike barrierer som kunder kan ha, mot
									å bruke mobil og pc til banktjenester. Målet var å
									motivere til forbedringer. Aha-opplevelsene sto i kø.
								</h3>
								<div class="articles-textfotobox">
									Tekst og foto: <span class="font-FlamaB">Rune Solheim</span> 
								</div>
								<br>

								<figure class="search-thumbs">
									<img src="images/artiP28-banner.jpg" alt="" class="img-social">
								</figure>
								<div class="images-caption">
									<span class="font-FlamaB">PÅ KROPPEN:</span> På en temadag om digital inkludering nylig, fikk IT-ansatte i Sparebanken Vest teste hvordan det kan oppleves å være fysisk hemmet på ulikt vis. Her spises det uten syn. (FOTO: SPAREBANKEN VEST)
								</div>  
								<br>

								<div class="row">
									<div class="col-md-8 colBorder">  
										<p class="noIndent">
											<span class="drop-txt font-FlamaT" style="color: #005091;">B</span> <span class="font-PalatinoB">ERGEN:</span> Systemutviklerne Janne
											Ringdal og Tord Grytting deltok på
											temadagen nylig der universell
											utforming av de digitale tjenestene
											sto i sentrum. Primus motor for prosjektet
											var UX-researcher Vibeke Storvik.
										</p>
										<p>
											– Universell utforming av digitale tjenester
											er noe vi har hørt om helt siden vi satt
											på skolebenken, sier Tord Grytting.
										</p>
										<p>
											– Nå fikk vi oppleve de barrierene som
											kundene har, helt personlig, og det ga oss
											et annet perspektiv og en annen innsikt i de
											utfordringene som mennesker kan ha i møte
											med digitale tjenester. Vi fikk mye større
											sympati for dem, og det økte også graden
											av hvor seriøst vi tar tematikken i jobben
											vår, sier han.
										</p>
										<p>
											– Alle tolv øvelsene gjorde inntrykk på
											meg, men aller mest den hvor vi skulle illudere
											leddvansker/artrose. Vi måtte ta på oss
											hansker som ga støt i hendene, samtidig som
											vi skulle prøve å åpne en konvolutt. Det var
											svært vanskelig, og da kan man jo se for seg
											hvordan det er å navigere på en smarttelefon
											med en slik lidelse.
										</p>
										<p>
											De ansatte fikk prøve seks forskjellige briller
											som illustrerte ulike synshemninger.
										</p>
										<p>
											– Det å registrere at man så vidt
											ser skjermen, gir jo et sterkt inntrykk
											av hvordan enkelte har
											det foran pc-en, sier Grytting.
										</p>  
									</div> 

									<div class="col-md-4 md-mt30">   
										<div class="imgbox mb30 mx-auto" style="width: 140px; border-radius: 100%; overflow: hidden;"> 
											<img src="images/artiP28-sub1.jpg" alt="">
										</div>
										<div class="quate-box font-FlamaB mt15 mb0"> 
											<span style="color: #005091;">”</span>Jeg skjønte virkelig
											i hvilken grad jeg tar
											synet mitt for gitt.<span style="color: #005091;">”</span> 
											<div class="name-onquate font-FlamaL">
												Janne Ringdal
											</div>
										</div> 
									</div>

									<div class="col-md-8 colBorder">
										<div class="articles-subtitle">ALLE KAN BLI RAMMET</div>
										<p class="noIndent">
											Janne Ringdal synes det var
											tankevekkende at de
											ulike hindrene de testet
											ut, faktisk kunne
											ramme henne selv, om
											enn for en kortere tid.
										</p>
										<p>
											– Alle og enhver
											kan bli syk eller skadet
											i en begrenset periode,
											brekke en arm eller
											miste synet midlertidig.
											At det også kan skje meg, var en virkelig
											aha-opplevelse, sier hun.
										</p>
										<p>
											Ringdal synes det var skremmende å bli
											helt blindet med tørkle foran øynene, og så
											skulle spise.
										</p>
										<p>
											– Da skjønte jeg virkelig i hvilken grad
											jeg tar synet mitt for gitt. At en så helt hverdagslig
											øvelse som å spise og drikke blir
											så vanskelig, ga veldig stort inntrykk.
											Jeg følte meg hjelpeløs og
											ga fort opp å bruke kniv og
											gaffel, for eksempel. Det var
											tryggere å bruke fingrene, da
											kjente jeg i hvert fall hva jeg
											puttet i munnen.
										</p>
										<p>
											IT-teamet har regelmessige
											møter hvor
											digital inkludering blir
											diskutert, og en ansatt
											har fått dette som spesialområde.
										</p>
										<p>
											– Som en direkte
											konsekvens av temadagen,
											har vi blitt
											enda flinkere til å lage
											gode testrutiner for digital inkludering.
											Miljøene som skal teste våre produkter, gjør
											det nå med retningslinjer om at løsningene
											skal kunne brukes av alle, på ulike enheter
											som pc, nettbrett og mobil, sier Ringdal.
										</p>
										<p>
											Vibeke Storvik forteller om en ansatt som 
											sto frem som dyslektiker, og bidro til mye
											kunnskap om den gruppen.
										</p>
										<p>
											– Vi fikk mye informasjon om
											hvor vanskelig det er å komme
											seg gjennom lange tekster, og
											hvor fysisk sliten man kan bli
											av det. Nå tenker vi nøye gjennom
											hvorvidt det er nødvendig
											med en lang tekst, for
											eksempel i e-post og
											tekstmeldinger til kunder,
											sier Storvik.
										</p>
									</div> 

									<div class="col-sm-12">
										<br>
										<div class="imgbox mb30"> 
											<a class="imgpopup" href="images/artiP28-sub2.jpg" data-fancybox="p28" data-caption="BRILLER: De IT-ansatte i Sparebanken Vest fikk teste briller som skulle illudere ulike synshemninger, alt fra grå stær til tunnelsyn. (FOTO: SPAREBANKEN VEST)">
												<figure class="search-thumbs">
													<img src="images/artiP28-sub2.jpg" alt="" class="img-social">
												</figure>
											</a> 
											<div class="images-caption">
												<strong>BRILLER:</strong> De IT-ansatte i Sparebanken Vest fikk teste briller som skulle illudere ulike synshemninger, alt fra grå stær til tunnelsyn. (FOTO: SPAREBANKEN VEST)
											</div>
										</div> 
									</div>
								</div>

								<div class="row"> 
									<div class="col-md-8 colBorder">
										<div class="articles-subtitle mt0">SAMARBEID OM SIKKERHET</div>
										<p class="noIndent">
											Vibeke Storvik brenner
											for universell utforming
											av digitale tjenester og
											står i spissen for prosjektet
											Digital inkludering i banken.
										</p>
										<p>
											Nå etterlyser hun mer samarbeid blant
											tjenesteleverandører innen bank, forsikring,
											helsetjenester, skatt, ja kort sagt alt som mennesker
											i Norge er nødt til å ha tilgang på.
											Hun ønsker også retningslinjer for
											sikkerhetsløsninger som legges oppå ellers
											veldig gode nyvinninger for digitale brukere
											med barrierer. Økende grad av
											svindel forsterker bruken av slike
											sikkerhetslag.
										</p>
										<p>
											– Det finnes eksempler på
											at oppstartsselskaper har laget
											utmerkete løsninger som øker
											tilgangen for grupper med
											digitalt utenforskap.
											Men det hjelper lite når
											man legger et lag oppå
											med visuelle sikkerhetsløsninger
											som blir
											en sperre igjen. Hvis du
											har nedsatt syn og skal
											gjennom oppgaver ala
											«finn bildene med trafikklys
											på», så er du like
											langt, påpeker Storvik med et sukk.
										</p> 
									</div>
									<div class="col-md-4"> 
										<br>  
										<div class="imgbox mb30 mx-auto" style="width: 140px; border-radius: 100%; overflow: hidden;"> 
											<img src="images/artiP28-sub3.jpg" alt="">
										</div>
										<div class="quate-box font-FlamaB mt15 mb0"> 
											<span style="color: #005091;">”</span>At man så vidt ser skjermen, gjør sterkt inntrykk.<span style="color: #005091;">”</span> 
											<div class="name-onquate font-FlamaL">
												Tord Grytting
											</div>
										</div> 
									</div>

									<div class="col-md-8 colBorder">
										<div class="articles-subtitle">BEHOLDER KONTORER</div>
										<p class="font-PalatinoI">
											– Er det langsiktige målet en hel-digital Sparebanken
											Vest?
										</p>
										<p>
											– Nei, i konsernet vårt har vi Bulder Bank
											som er hel-digital, men for Sparebanken Vest
											sin del kommer det aldri til å skje. Vi har 36
											lokalkontorer i dag, hvor kundene kan ha
											fysiske møter med rådgiver, og man kan
											også ringe inn. Vi vil fortsette å være en bank
											for kundene våre i alle livssituasjoner.
										</p>
										<p>
											– Like fullt ønsker vi at alle som er i stand
											til det, tar i bruk digitale tjenester.
										</p>
										<p>
											De fleste klarer vi å hjelpe til å klare seg
											digitalt.
										</p>
										<p>
											– Universell utforming av de digitale
											tjenestene har vi arbeidet med siden retningslinjene
											kom i 2013. Men i løpet av det
											siste halve året har vi jobbet bredere og gjort
											mer, med blant annet bevisstgjøring av alle
											ansatte. Nå har vi en digital inkluderingssupporter
											i hvert team, og forbedringene
											kommer på løpende bånd, sier Storvik.
											<br><br>

											Les mer om digitalt utenforskap her:
											<br>

											<a href="https://www.finansfokus.no/2021/12/20/600-000-lever-i-digitaltutenforskap/" target="_blank" style="word-wrap: break-word;">https://www.finansfokus.no/2021/12/20/600-000-lever-i-digitaltutenforskap/</a>
										</p>
									</div>
								</div>
							</div> 			 
						</div>
					</div>
					<!--row-->
				</section> 
				<div class="spcr40"></div>
			</div>
			<!-- //PAGE -->

			<!-- PAGE -->
			<div id="17" data-history="peker-ut-ny-retning-for-eika" class="swiper-slide">
				<title>Peker ut ny retning for Eika</title>
				<section id="team" class="topSpacer">
					<div class="wrapper articles-contentsection">
						<div class="container inner_container">
							<!-- articles-container -->
							<div class="articlesborder-section"> 	 
								<div class="articles-categorybox">
									<span class="font-FlamaL" style="color: #fff; background: #728ebe;">Portrett</span> 
									<div class="articategory-content font-FlamaR">Steinar Simonsen</div>
								</div>
								<br>  
								
								<h2 class="articles-heading font-opulent font50">
									Peker ut ny retning for Eika
								</h2> 
								<h3 class="intro-text content-social">
									Et ublidt møte med gammelt sprengstoff, ga Eikas nye toppsjef en
									erfaring han gjerne skulle vært foruten. Resultatet er at Steinar
									Simonsen peker retningen med halve pekefingeren.
								</h3>
								<div class="articles-textfotobox">
									Tekst: <span class="font-FlamaB">Sjur Anda</span> Foto: <span class="font-FlamaB">Sverre Chr. Jarild</span> 
								</div>
								<br>

								<figure class="search-thumbs">
									<img src="images/artiP30-banner.jpg" alt="" class="img-social">
								</figure>  
								<br>

								<div class="row">
									<div class="col-md-8 colBorder">  
										<p class="noIndent">
											<span class="drop-txt font-FlamaT" style="color: #728ebe;">M</span> ens de fleste 62-åringer i finans tenker
											på å trappe ned og hvordan de skal
											bruke pensjonstilværelsen, tok Steinar
											Simonsen steget helt til topps i Eika
											Gruppen.
										</p>
										<p>
											– Jeg ble litt overrasket over jobbtilbudet. Det
											er et privilegium å få et sånt tilbud som 62-åring.
											Men som styreleder sa: 62 eller 42 spiller ingen
											rolle, så lenge man fremdeles er opptatt av å utvikle
											virksomheten. Man lærer mye gjennom et
											langt liv og som litt eldre tar jeg med meg livs- og
											yrkeserfaring. Jeg tror jeg gjør en bedre jobb nå,
											enn jeg ville gjort da jeg var 42, sier Steinar Simonsen.
											Vi treffer ham i Eika Gruppens lokaler sentralt
											i Oslo. Han er lang og bøyer nakken litt når han
											hilser for å komme ned på mitt nivå. En stri sideskill
											gir et litt speideraktig inntrykk.
										</p>
										<p>
											For Simonsen har det ikke vært aktuelt å trappe
											ned.
										</p>
										<p>
											– Det ligger ikke i min natur. Jeg er en litt rastløs
											type som liker å få til ting. Det er stimulerende
											og gjør at jeg fremdeles utvikler meg. Klare
											tilbakemeldinger fra kunder er kjempegøy.
										</p>
										<p>
											18. januar var første dag som konsernsjef. Men
											han har allerede vært ti år i ledergruppen, der han
											før var konserndirektør med ansvar for teknologi.
											Så hvordan har den første tiden vært?
										</p>
										<p>
											– Jeg kjenner selskapet godt, så det har ikke
											vært noen store overraskelser. Jeg visste godt hva
											Eika er, og hva rollen innebærer. Jeg har fått litt ny
											oppmerksomhet og andre oppgaver. Det er stimulerende,
											morsomt og motiverende.
										</p>  

										<div class="articles-subtitle">BESØKER FLEST MULIG</div>
										<p class="noIndent">
											Som teknologidirektør har han først og fremst
											jobbet internt. Nå må han i mye større grad jobbe
											utadrettet.
										</p>
										<p>
											– Jeg prøver å være godt synlig internt. Vi har
											månedlige infomøter, og jeg går gjerne rundt i
											huset og prater med folk. Bankene er kundene våre
											og jeg prøver å besøke flest mulig. Er jeg på ferie
											på Vestlandet og kjører forbi Sogn Sparebank,
											stikker jeg innom. Å bruke enhver mulighet til å
											snakke med dem som er i bankene eller folk her
											på huset er viktig. Jeg får alltid vite noe nytt. Aktiv
											bruk av ulike kommunikasjonsmåter er jeg veldig
											opptatt av.
										</p>
										<p>
											Et googlesøk på Steinar Simonsen gir ikke så
											mange spennende treff. Pressemeldingen om at
											han tok over som sjef samt en artikkel på finansfokus.
											no der han prater om compliance-gevinst
											ved innføring av nytt kjernebanksystem, er egentlig
											det med mest substans. På Proff.no får vi vite
											at han er styreleder i Forsvarets Høgskole, og at
											han har 535 aksjer i Borregaard ASA.
										</p>
										<p>
											Heller ikke sosiale medier skriver mye om
											62-åringen. Dette er delvis bevisst.
										</p>
										<p>
											– Jeg har vært ganske mye i USA og sett på
											digital transformasjon. I 2016 var jeg blant annet
											innom Facebook. Da ble jeg litt skremt over å se
											hvordan de nesten ønsket et globalt herredømme
											på informasjonsområdet, der de samlet mest mulig
											data om brukerne for å utnytte dette kommersielt.
											Etter dette har jeg vært tilbakeholden på sosiale
											medier.
										</p>
									</div> 

									<div class="col-md-4 md-mt30">   
										<div class="imgbox mb30"> 
											<a class="imgpopup" href="images/artiP30-sub1.jpg" data-fancybox="p30" data-caption="SPRENGT FINGER: Et lite heldig eksperiment med krutt som barn gjorde at tuppen av pekefingeren forsvant."> 
												<img src="images/artiP30-sub1.jpg" alt="">
											</a> 
											<div class="images-caption">
												<strong>SPRENGT FINGER:</strong> Et lite heldig eksperiment med krutt som barn gjorde at tuppen av pekefingeren forsvant.
											</div>
										</div>

										<div class="imgbox mb30"> 
											<a class="imgpopup" href="images/artiP30-sub2.jpg" data-fancybox="p30" data-caption="VIKTIG STANDARDISERING: – Standardisering av IT-systemer er noe av de viktigste vi har gjort for bankene, sier Steinar Simonsen."> 
												<img src="images/artiP30-sub2.jpg" alt="">
											</a> 
											<div class="images-caption">
												<strong>VIKTIG STANDARDISERING:</strong> – Standardisering av IT-systemer er noe av de viktigste vi har gjort for bankene, sier Steinar Simonsen.
											</div>
										</div> 
									</div>

									<div class="col-md-8 colBorder">
										<div class="articles-subtitle">KREVENDE REGELVERK</div>
										<p class="noIndent">
											Bankene har de siste årene blitt bombardert
											av ulike mer og mindre nye
											regelverk. Compliance, eller regeletterlevelse,
											øker stadig. Her kreves høy
											kompetanse, noe som kan være utfordrende
											for mindre banker i Eika.
										</p>
										<p>
											– Det kan være krevende for bankene
											å forholde seg til det som kommer. Her
											har alliansen en viktig rolle å spille.
											Reglene er der, og vi må forholde oss til
											dem på en positiv måte. Vi har miljøer
											som bistår bankene i dette. Samtidig er
											jeg opptatt av likebehandling, der det
											på kravene til egenkapitaldekning er
											skeivheter. Standardmetodebankene må
											ha vesentlig mer kapital bak hver utlånt
											krone enn IRB-bankene. Vi har hatt flere
											møter med Finansdepartementet, og det
											kommer nye regler på dette området.
											Det er jeg spent på, sier Simonsen.
										</p>
										<p class="font-PalatinoI">
											– Hvis du fikk endre eller fjerne en regulering,
											hva ville du gjort da?
										</p>
										<p>
											– Jeg ville innført mest mulig like
											kapitalkrav og gjennom det like «spilleregler
											», det er det viktigste for bankene.
										</p>

										<div class="articles-subtitle">NYTT KJERNEBANKSYSTEM</div>
										<p class="noIndent">
											De siste par årene har Eika Gruppen
											byttet kjernebanksystem for alle bankene
											i alliansen. Bankene har gått fra
											danske SDC til TietoEvry. Med overgangen
											er Eika nå på samme system som
											de fleste andre norske banker.
										</p>
										<p>
											– Overgangen har gått over all forventning.
											Jeg er stolt over den innsatsen
											som ble gjort i bankene og her hos oss
											i Eika og TietoEvry. Med tanke på oppgavens
											kompleksitet, har det vært svært
											få stopp i bankenes drift. Vi får en standardisering
											som vil forenkle og heve
											kvaliteten. Det er enklere å være på en
											norsk plattform. Vi får en fleksibilitet
											og det blir lettere å drive utviklingsarbeid.
											Det har vært litt mer tungvint
											med en partner som opererer fra Danmark.
											Sluttkunden merker ikke dette i
											morgen, men det vil på sikt gi bedre
											digitale tjenester. Ikke minst vil det føre
											til betydelig lavere kostnader for bankene,
											sier Simonsen, som tror at overgangen
											kan ha bidratt til at Haugesund
											Sparebank gikk til Eika i fjor høst.
										</p>

										<div class="articles-subtitle">ANNERLEDES EIKA</div>
										<p class="noIndent">
											Eika er i stadig endring. Eika Forsikring
											og Fremtind slår seg sammen i løpet av
											året. Fusjonerer gjør også Eika Kredittbank og SpareBank 1 Kreditt.
										</p>
										<p>
											– Eika kommer til å se litt annerledes
											ut. Vi har nå startet en strategiprosess
											hvor vi ser på hvordan vi tror fremtidens
											lokalbank vil se ut. Når det er ferdig, vil
											vi se hva det innebærer for Eika og hva
											vi skal gjøre annerledes for at bankene
											skal lykkes også fremover.
										</p>
										<p class="font-PalatinoI">
											– Hvilke tanker har du rundt dette?
										</p>
										<p>
											– Jeg skal være litt forsiktig med å
											mene noe mens prosessen er i gang. Men
											parametere som kunstig intelligens,
											endret kundeadferd, våre egne systemer
											og spørsmål rundt hvordan vi kan jobbe
											mer effektivt vil være momenter som
											påvirker. Uansett må medarbeiderne i
											bankene og i Eika utvikle seg med hensyn
											til hva ny teknologi vil gi av muligheter.
											Det er fascinerende å se hvordan
											kundeadferden har endret seg, men jeg
											tror uansett det vil være behov for å ha
											mennesker og rådgivere tilgjengelig.
											Samtidig vil oppgavene, rollen og måten
											vi jobber på forandres.
										</p> 
									</div> 

									<div class="col-sm-12">
										<br>
										<div class="imgbox mb30"> 
											<a class="imgpopup" href="images/artiP30-sub3.jpg" data-fancybox="p30" data-caption="NY STRATEGI: Kunstig intelligens og endret kundeadferd er ting som påvirker Eikabankene fremover. Det endrer også måten Eika Gruppen jobber på. – Eika kommer til å se annerledes ut, og vi har startet en strategiprosess for å se hvordan vi skal jobbe fremover, sier Steinar Simonsen.">
												<figure class="search-thumbs">
													<img src="images/artiP30-sub3.jpg" alt="" class="img-social">
												</figure>
											</a> 
											<div class="images-caption">
												<strong>NY STRATEGI:</strong> Kunstig intelligens og endret kundeadferd er ting som påvirker Eikabankene fremover. Det endrer også måten Eika Gruppen jobber på. – Eika kommer til å se annerledes ut, og vi har startet en strategiprosess for å se hvordan vi skal jobbe fremover, sier Steinar Simonsen.
											</div>
										</div> 
									</div>
								</div>

								<div class="row"> 
									<div class="col-md-8 colBorder"> 
										<div class="articles-subtitle mt0">BEGRENSEDE RESSURSER</div>
										<p class="noIndent">
											Eika Gruppen består av snaut 50 selvstendige
											banker, som er både kunder og
											eiere samtidig. Å ha en slik eierstruktur
											kan føre til at man kan bli dratt i ulike
											retninger, ut fra at ulike banker har ulike
											interesser.
										</p>
										<p>
											– Det er bankene som prioriterer og
											bestemmer hvor mye penger vi skal
											bruke, og hva vi skal gjøre. De er våre
											premissleverandører. Samtidig kan det
											være ulike interesser i bankene, selv om
											det i hovedsak er mer som samler enn
											som skiller bankenes prioriteringer. Det
											er viktig å ha gode prosesser for å få
											riktige prioriteringer og for å forankre
											og skape forståelse for hva vi velger å
											gjøre. Da må man akseptere at man ikke
											alltid får viljen sin.
										</p>
										<p>
											En gang i måneden møtes Eika sentralt
											og åtte banksjefer, som representerer
											hver sin krets av små, store og mellomstore
											banker.
										</p>
										<p>
											– Da får de komme med sine prioriteringer
											og avklaringer. Det gir en raskere
											og bedre gjennomføring. Det er helt
											avgjørende at det er bankene som prioriterer
											hva vi skal levere. Det er de som
											kjenner kundene sine og bankens behov
											best. Vi har ideer og kompetanse og gir
											våre innspill, sier Simonsen, og fortsetter:
											– Samspillet mellom Eika sentralt
											og bankene er særdeles viktig. Det gir 
											oss en nærhet til våre kunder.
										</p>
										<p class="font-PalatinoI">
											– Har du noe eksempel på at dette har
											fungert godt?
										</p>
										<p>
											– Det viktigste og beste vi har gjort,
											er standardisering. Å ha 50 ulike nett- og
											mobilbanker blir håpløst å utvikle og
											forvalte. Bankene har nå likt kjernebankoppsett,
											og de har tilnærmet like nettog
											mobilbankløsninger, samtidig som
											det er viktig at bankene får profilert sin
											logo og sin grafiske profil. Vi måler også
											jevnlig hvor fornøyde bankene er med
											våre tjenester, slik vet vi om vi er på rett
											spor.
										</p>
									</div>
									<div class="col-md-4 md-mt30">
										<div class="quate-box font-FlamaB mt15 mb0"> 
											<span style="color: #728ebe;">”</span>Eika kommer
											til å se litt
											annerledes ut.<span style="color: #728ebe;">”</span>  
										</div> 
									</div>

									<div class="col-md-8 colBorder">
										<div class="articles-subtitle">FÆRRE BANKER</div>
										<p>
											Eikabankene har blitt stadig færre. Allikevel
											øker forvaltningskapitalen og
											bankene tar markedsandeler. Dette
											henger selvfølgelig sammen med en 
											jevn strøm av fusjoner. Resultatene viser
											også at de fusjonerte bankene vokser
											mest. Hvordan påvirker dette Eika
											Gruppen?
										</p>
										<p>
											– Om vi håndterer 50 eller 55 banker,
											gjør ikke noen stor forskjell. Vi bidrar
											og hjelper de som vil fusjonere. Eika har
											hatt en god utvikling og vokser mer enn
											markedet og konkurrentene. Mens Eikas
											rolle er å bidra til at alle bankene kan
											drive best mulig, er det selvfølgelig opp
											til bankene å bestemme hvordan man
											driver. Samtidig ser vi at kompetanse
											er en kritisk suksessfaktor. Da blir utvikling
											av de ansatte veldig viktig, sier
											Simonsen, og puster dypt før han fortsetter.
										</p>
										<p>
											– Spørsmålet er hvor folkene skal
											komme fra. Det blir en kamp om de
											unge. Prognosene er at mange sektorer
											som helsevesenet og Forsvaret vil trenge
											mange folk fremover. Å få tak i kompetent
											arbeidskraft blir en utfordring. Da
											er det viktig å ha et godt verdigrunnlag,
											slik at de velger din virksomhet.
										</p>

										<div class="articles-subtitle">SPRENGT FINGER</div>
										<p>
											I et portrett skal man
											gjerne få frem litt
											personlige ting. Steinar
											Simonsen er
											sønn av en yrkesoffiser
											og har bodd
											rundt i landet. Han
											fulgte i sine fars fotspor
											og tok befalsskolen, før han utdannet
											seg til sivilingeniør ved NTNU. På
											fritiden liker han å være ute og å være
											med familien. Han har dog ingen spesielle
											hobbyer han dyrker. Ikke så mye
											å henge seg opp i her akkurat.
										</p>
										<p>
											Mer interessant er det at han som
											fjortenårig militærunge bodde Nord-
											Norge. I området var det en del
											etterlatenskaper etter tyskerne, som var
											uimotståelig for unge Simonsen og
											vennene hans. Noe av dette var sprengstoff,
											og med et smell
											forsvant halve venstre
											pekefinger.
										</p>
										<p>
											– Det var en dyrekjøpt
											erfaring, man skal
											vite hva man gir seg ut
											på – kompetanse er
											med andre ord nøkkelen.
											Dette kunne gått
											mye verre, det satt mye splinter fast i
											jakken. Det rare er at det ikke gjorde
											vondt der og da. Men det er ikke en
											erfaring jeg anbefaler, avslutter han tørt.
										</p>
									</div>
								</div>
							</div> 			 
						</div>
					</div>
					<!--row-->
				</section> 
				<div class="spcr40"></div>
			</div>
			<!-- //PAGE -->

			<!-- PAGE -->
			<div id="18" data-history="kun-20-25-banker-igjen" class="swiper-slide">
				<title>Kun 20–25 banker igjen</title>
				<section id="team" class="topSpacer">
					<div class="wrapper articles-contentsection">
						<div class="container inner_container">
							<!-- articles-container -->
							<div class="articlesborder-section"> 	 
								<div class="articles-categorybox">
									<span class="font-FlamaL" style="color: #fff; background: #eb6067;">TECH</span> 
									<div class="articategory-content font-FlamaR">Færre banker</div>
								</div>  								
								<br> 

								<h2 class="articles-heading font-opulent font50">
									Kun 20–25 banker igjen
								</h2> 
								<h3 class="intro-text content-social">
									Reguleringer, kapitalkrav og ny teknologi vil tvinge frem
									mer samarbeid og standardisering, mener Lars Erik
									Fjørtoft, som tror Norge etter hvert vil ha 20–25 banker.
								</h3>
								<div class="articles-textfotobox">
									Tekst og foto: <span class="font-FlamaB"> Sjur Anda</span>
								</div>
								<br>

								 <figure class="search-thumbs">
									<img src="images/artiP34-banner.jpg" alt="" class="img-social">
								</figure>
								<div class="images-caption mb30">
									<span class="font-FlamaB">DIGITAL SPÅMANN:</span> Lars Erik
									Fjørtoft ser at teknologi,
									kapitalkrav og reguleringer vil
									påvirke finans kraftig fremover.
									– Disse elementene forsterker
									og påvirker hverandre, sier
									Fjørtoft.
								</div>

								<div class="row">
									<div class="col-md-8 colBorder"> 
										<p class="noIndent">
											<span class="drop-txt font-FlamaT" style="color: #eb6067;">D</span> et er dyrt å drive bank. Spørsmålet
											bransjen hele tiden spør seg, er hvordan
											vi kan hente ut mer synergier?
										</p>
										<p>
											Vi er på DNs Penger og teknologikonferanse.
											PWCs Lars Erik Fjørtoft har fått
											i oppdrag å se i spåkulen og komme med
											noen velbegrunnede tanker om hvordan
											finansbransjen vil påvirkes av ny teknologi
											og reguleringer fremover.
										</p>
										<p>
											– Det er veldig mange fusjoner nå, ikke
											bare blant bankene. Vi ser at også flere felleseide
											selskaper slås sammen. Rundt om i
											landet spør bankledere seg om hvordan de
											skal kunne samarbeide for å ta ut større
											gevinster. Kapitalkrav, stadig flere reguleringer
											og ny teknologi driver kostnadene i
											været. Løsning er mer standardisering på
											hvordan man betjener kunder. Mobilbankene
											er svært like hverandre. De har samme innloggingsløsninger,
											efakturaer går på tvers
											av bankene. Det er lik teknologi som ligger
											bak. Også regulatorisk stilles det de samme
											kravene til alle. Det er dyrt og vanskelig å
											være annerledes og nyskapende, sier Fjørtoft,
											som venter at fusjonene fortsatt vil prege
											bransjen.
										</p>
										<p>
											– Blir enden på visa at SpareBank 1 og
											Eika slås sammen, og blir det en Sparebanken
											Norge. Det vet vi ikke, sier Fjørtoft, som tror
											det til slutt vil være 20–25 banker i Norge.
										</p>
										<p>
											I dag er det 110 banker i Norge og 18
											filialer av utenlandske banker, ifølge en oversikt
											fra Finans Norge.
										</p>

										<div class="articles-subtitle">FELLES TEKNOLOGI</div>
										<p class="noIndent">	
											De aller fleste banker har nå sine kjernebanksystemer
											hos Tieto Evry. Det er effektivt for
											næringen.
										</p>
										<p>
											– Det gjør det lettere å ta inn compliance
											og nye krav som kommer. Samtidig er det
											skummelt at stort sett hele bransjen er avhengig
											av én leverandør, sier Fjørtoft, som ble
											overrasket da Evrys planer om å skille ut
											bankoperasjonsdelen og børsnotere denne,
											ble stoppet.
										</p>
										<p>
											– Her kunne vi fort sett at bankene gikk
											sammen for å kjøpe operasjonsdelen av Evry.
											Det kunne vært en naturlig utvikling der
											banken selv fikk kontroll over denne infrastrukturen.
											Alt handler om å få ned transaksjonskostnadene.
											Én løsning er mer felles
											infrastruktur og flere sammenslåinger, sier
											Fjørtoft.
										</p> 
									</div> 

									<div class="col-md-4 md-mt30"> 
										<div class="quate-box font-FlamaB mt15 mb0"> 
											<span style="color: #eb6067;">“</span>Mobilbankene er svært like hverandre.<span style="color: #eb6067;">”</span>  
										</div>
									</div> 

									<div class="col-md-8 colBorder">
										<div class="articles-subtitle">KI-TILSYN</div>
										<p class="noIndent">	
											Ingen tek-spåmann kommer utenom kunstig
											intelligens (KI).
										</p>
										<p>
											– KI kommer for fullt. Så spørs det hvordan
											det tas opp av markedet. Men det vil
											påvirke de fleste deler av virksomheten. Fra
											svindelhåndtering til onboarding. Det vil
											massivt påvirke hvordan tjenester blir levert
											i markedet. Og det blir til det bedre, sier
											Fjørtoft, og viser til 13. mars som en ny
											merkedag. Da vedtok EU en AI-act, som skal
											regulere bruken av kunstig intelligens. Disse
											kravene skal være virksomme i løpet av et
											par år.
										</p>
										<p>
											– Denne reguleringen vil treffe finans
											veldig hardt. Hvilket organ som skal føre
											tilsyn blir viktig for næringen. Det vil være
											en stor fordel at Finanstilsynet tar seg av 
											dette. Jeg håper og tror vi kan få et finanstilsyn
											som er offensive på næringens vegne i
											å ta i bruk ny teknologi. Datatilsynet vil
											kjempe en annen sak, sier Fjørtoft.
										</p>

										<div class="articles-subtitle">MER REGULERINGER</div>
										<p class="noIndent">	
											Fjørtoft tror også vi får store endringer på
											betalingsområdet.
										</p>
										<p>
											– Apple åpner kanskje opp sin digitale
											lommebok. Det kan gjøre mye for Vipps.
											Samtidig kommer PSD3, der politikerne skal
											tvinge frem innovasjon. Her skal man sikre
											at betalingssystemene åpnes i en helt annen
											grad enn nå. Dette vil påvirke konkurransesituasjonen
											i Europa. Det er også på gang et
											helt nytt oppgjørssystem i regi av den europeiske
											sentralbanken, som vil sikre sømløst
											og umiddelbart oppgjør mellom ulike valutaer.
											Jeg håper at også Norge henger seg på
											dette. Det vil fjerne hindre som ulike valutaer
											innebærer og åpne for utfordrere som Vipps.
										</p>
									</div>
								</div>    
							</div> 						 
						</div>
					</div>
					<!--row-->
				</section> 
				<div class="spcr40"></div>
			</div>
			<!-- //PAGE -->

			<!-- PAGE -->
			<div id="19" data-history="tietoevry-banking-ikke-pa-bors" class="swiper-slide">
				<title>TIETOEVRY BANKING IKKE PÅ BØRS</title>
				<section id="team" class="topSpacer">
					<div class="wrapper articles-contentsection">
						<div class="container inner_container">
							<!-- articles-container -->
							<div class="articlesborder-section"> 	 
								<div class="articles-categorybox">
									<span class="font-FlamaL" style="color: #fff; background: #eb6067;">TECH</span> 
									<div class="articategory-content font-FlamaR">Kjernebanksystemer</div>
								</div>  								
								<br> 

								<h2 class="articles-heading font-FlamaB font50">
									TIETOEVRY BANKING IKKE PÅ BØRS
								</h2> 
								<h3 class="intro-text content-social">
									De fleste norske banker har kjernesystemer fra ITselskapet
									Tietoevry, så mange at Finanstilsynet kaller
									det en konsentrasjonsrisiko. Planene om å skille
									ut bankvirksomheten i et eget børsnotert selskap,
									skapte usikkerhet. Den er nå avblåst.
								</h3>
								<div class="articles-textfotobox">
									Tekst: <span class="font-FlamaB">Claude R. Olsen</span>
								</div>
								<br>

								 <figure class="search-thumbs">
									<img src="images/artiP36-banner.jpg" alt="" class="img-social">
								</figure>
								<div class="images-caption mb30">
									<span class="font-FlamaB">UENDRET STRATEGI:</span> – Vi fortsetter med uforminsket styrke å utvikle oss som en spesialisert virksomhet fokusert på banksektoren, sier Klaus Andersen I Tietoevry Banking. (FOTO: TIETOEVRY)
								</div> 

								<div class="row">
									<div class="col-md-8 colBorder"> 
										<p class="noIndent">
											<span class="drop-txt font-FlamaT" style="color: #eb6067;">I</span> februar i år varslet styret i Tietoevry at
											selskapet skulle skille ut bankvirksomheten
											med sikte på børsnotering 1. juli
											i år. Bakgrunnen var ønsket om å øke
											verdiene for aksjonærene. I slutten av
											april kom kontrabeskjeden. Bankvirksomheten
											fortsetter som en enhet i Tietoevry.
										</p>
										<p>
											Bankvirksomheten i Tietoevry hadde
											i 2023 en omsetning på rundt 6,4 milliarder
											norske kroner og om lag 3 500 ansatte
											hvorav 1 500 i Norge. Den siste store
											bankgruppen som tok i bruk Tietoevrys
											systemer, skjedde høsten 2023 da 52 lokalbanker
											i Eika Alliansen fullførte overgangen.
										</p>

										<div class="articles-subtitle">ØKT KONSENTRASJONSRISIKO</div>
										<p class="noIndent">	
											Finanstilsynet har vurdert Tietoevrys
											dominerende stilling. I sin Risiko- og
											sårbarhetsanalyse (ROS) 2023 skrev
											Finanstilsynet at «Overgangen til Tietoevry
											for Verdipapirsentralen AS og bankene
											i Eika Alliansen innebærer isolert
											sett økt konsentrasjonsrisiko, siden flere
											foretak i finanssektoren allerede benyttet
											Tietoevry som driftsleverandør.»
										</p>
										<p>
											Finansfokus har spurt Finanstilsynet
											om hvor stor konsentrasjonsrisikoen er 
											og hva finansinstitusjonene må gjøre for
											å redusere risikoen ved bare å ha én leverandør,
											men har fått til svar at Finanstilsynet
											ikke har ytterligere kommentarer
											til dette.
										</p>  

										<div class="articles-subtitle">STOLER PÅ KONTROLLORGANENE</div>
										<p class="noIndent">
											Marius Furulund, konserndirektør IT i
											Eika Gruppen, sier Tietoevry har en veldig
											sentral rolle i det norske samfunnet.
										</p>
										<p>
											– Dersom deres tjenester ikke er oppe,
											treffer det bredt når betalinger stopper.
											Dette er samfunnskritisk infrastruktur
											på linje med strøm og telefon, sier han.
										</p>
										<p>
											Bankene i Eika Alliansen har hver for
											seg inngått avtale med Tietoevry, mens
											deres felles selskap Eika Gruppen håndterer
											og administrerer kontakten med
											Tietoevry.
										</p>
										<p class="font-PalatinoI">
											– Tietoevry har nesten alle norske banker
											på kundelisten. Hvordan vurderer dere risikoen
											når alle bruker samme system?
										</p>
										<p>
											– For våre banker er det ikke økt risiko
											om alle har samme leverandør. Vår vurdering
											er at det er lavere risiko for oss.
											Risikoen ligger mer på Norge på samfunnsnivå,
											siden en større andel av transaksjonene
											og løsningene innenfor bank
											er avhengig av at kjernesystemene kjører,
											sier Furulund.
										</p>
										<p>
											Han stoler på at de ulike tilsynene gjør
											jobben.
										</p>
										<p>
											– Vi er opptatt av at de kontrollorganene
											som finnes, fortsetter å være tett på
											Tietoevry som en viktig samfunnsaktør,
											sier Furulund.
										</p>
									</div> 

									<div class="col-md-4 md-mt30"> 
										<div class="imgbox mb30"> 
											<a class="imgpopup" href="images/artiP36-sub1.jpg" data-fancybox="p36" data-caption="SAMFUNNSKRITISK INFRASTRUKTUR: Marius Furulund i Eika Gruppen er opptatt av at kontrollorganene i Norge fortsetter å være tett på Tietoevry som en viktig samfunnsaktør. (FOTO: EIKA GRUPPEN)"> 
												<img src="images/artiP36-sub1.jpg" alt="">
											</a> 
											<div class="images-caption">
												<strong>SAMFUNNSKRITISK INFRASTRUKTUR:</strong> Marius Furulund i Eika Gruppen er opptatt av at kontrollorganene i Norge fortsetter å være tett på Tietoevry som en viktig samfunnsaktør. (FOTO: EIKA GRUPPEN)
											</div>
										</div>
										<br>

										<div class="quate-box font-FlamaB mb0"> 
											<span style="color: #eb6067;">“</span>Dette er
											samfunnskritisk
											infrastruktur på
											linje med strøm
											og telefon.<span style="color: #eb6067;">”</span>  
											<div class="name-onquate font-FlamaL">
												Marius Furulund, <br>Eika Gruppen.
											</div>
										</div>
									</div> 

									<div class="col-md-8 colBorder"> 
										<div class="articles-subtitle">LOVER KONTINUITET</div>
										<p class="noIndent">	
											Klaus Andersen som leder Tietoevry
											Banking, sier at de vil fortsette som en
											spesialisert virksomhet fokusert på banksektoren.
										</p>
										<p>
											– Etter den strategiske gjennomgangen,
											har styret i Tietoevry besluttet at vi
											fortsatt skal være en del av konsernet.
											Tietoevry Banking har fortsatt gode
											muligheter til å utvikle seg, og å nå våre
											strategiske mål. Denne beslutningen har
											derfor ingen direkte innvirkning på organisasjonen
											eller leveranser til våre kunder,
											sier han.
										</p>
									</div>
								</div>    
							</div> 						 
						</div>
					</div>
					<!--row-->
				</section> 
				<div class="spcr40"></div>
			</div>
			<!-- //PAGE -->

			<!-- PAGE -->
			<div id="20" data-history="enklere-a-jobbe-med-krypto" class="swiper-slide">
				<title>ENKLERE Å JOBBE MED KRYPTO</title>
				<section id="team" class="topSpacer">
					<div class="wrapper articles-contentsection">
						<div class="container inner_container">
							<!-- articles-container -->
							<div class="articlesborder-section"> 	 
								<div class="articles-categorybox">
									<span class="font-FlamaL" style="color: #fff; background: #eb6067;">TECH</span> 
									<div class="articategory-content font-FlamaR">Kryptoreguleringer</div>
								</div>  								
								<br> 

								<h2 class="articles-heading font-FlamaB font50">
									ENKLERE Å JOBBE MED KRYPTO
								</h2> 
								<h3 class="intro-text content-social">
									Fra 2025 blir det lettere for norske
									finansinstitusjoner å kunne ta imot kunder med
									kryptovaluta og tilby tjenester innenfor krypto, uten
									å komme på kant med hvitvaskingsloven.
								</h3>
								<div class="articles-textfotobox">
									Tekst: <span class="font-FlamaB">Claude R. Olsen</span>
								</div>
								<br>

								 <figure class="search-thumbs">
									<img src="images/artiP38-banner.jpg" alt="" class="img-social">
								</figure>
								<div class="images-caption mb30">
									<span class="font-FlamaB">KRYPTO I BANKER:</span> Når EU-forordningen om markeder for kryptoeiendeler (MiCA) trer i kraft ved utgangen av året, venter Mads Ribe i EY å se mer innovasjon i bankene og i kryptobransjen. (FOTO: EY)
								</div> 

								<div class="row">
									<div class="col-md-8 colBorder"> 
										<p class="noIndent">
											<span class="drop-txt font-FlamaT" style="color: #eb6067;">D</span> ette følger av EUs forordning om
											markeder for kryptoeiendeler
											(MiCA) som trer i kraft ved årsskiftet
											2024/2025, en forordning
											som også Norge vil slutte seg til gjennom
											EØS-avtalen. Finansdepartementet har
											sendt ut et forslag til norsk tilpasning
											med frist 1. juni i år.
										</p>

										<div class="articles-subtitle">BESKYTTE FORBRUKERE OG INVESTORER</div>
										<p class="noIndent">	
											Med MiCA-regelverket vil EU beskytte
											forbrukerne mot noen av risikoene ved
											investeringer i kryptoaktiva, og redusere
											faren for svindel. Reglene skal samtidig
											bidra til å fremme innovasjon, investorbeskyttelse
											og markedsintegritet, samt
											sikre finansiell stabilitet.
										</p>
										<p>
											MiCA regulerer ulike typer kryptoeiendeler
											(tokens):
										</p>

										<ul>
											<li>
												EMT (e-money token): Kryptoeiendeler
												som er utformet for å opprettholde
												en stabil verdi ved å referere
												til én offisiell valuta som dollar eller
												euro.
											</li>
											<li>	
												ART (asset referenced token): Kryptoeiendeler
												som er utformet for å opprettholde
												en stabil verdi ved å referere
												til flere valutaer, råvarer eller andre
												kryptoaktiva, eller en kombinasjon av
												slike aktiva.
											</li>
											<li>	
												Andre kryptoeiendeler.
											</li>
										</ul>   
									</div> 

									<div class="col-md-4 md-mt30">  
										<div class="quate-box font-FlamaB mb30"> 
											<span style="color: #000;">“</span>Det blir spennende
											å se om MiCA fører
											til at bankene i større
											grad begynner å tilby
											tjenester selv.<span style="color: #000;">”</span>  
											<div class="name-onquate font-FlamaL">
												Mads Ribe, EY
											</div>
										</div>
									</div>

									<div class="col-md-8 colBorder">
										<p style="text-indent: 25px;">
											De nye reglene vil både gjelde kryptoselskaper
											som utsteder egne tokens, og
											selskaper som er tjenestetilbydere i kryptomarkedet,
											for eksempel vekslere og
											lagringstilbydere (custodians). Disse
											selskapene må sende søknad til Finanstilsynet
											i sitt hjemland. Godkjenningen
											vil derfra gjelde i hele EU.
										</p>
										<p>
											Første del av MiCA trer i kraft i EU
											30. juni, mens hele pakken trer i kraft 31.
											desember 2024.
										</p>
										<p>
											Til nå har bankene
											enten holdt
											seg helt unna alt
											som har med
											krypto å gjøre,
											eller de har forsiktig
											tilnærmet
											seg kryptoaktører
											som allerede
											er omfattet av
											hvitvaskingsreglene.
										</p>
										<p>
											EU håper at et
											mer regulert kryptomarked vil gjøre
											finansinstitusjonene tryggere på kryptotjenestene
											og føre til økt innovasjon.
										</p>

										<div class="articles-subtitle">BANKENE SLIPPER Å SØKE</div>
										<p class="noIndent">
											Mads Ribe som leder teknologisatsingen
											i EY, jobber med hvordan MiCA vil
											påvirke norske aktører, både kryptoaktører
											og finansinstitusjoner. Han sier
											fokuset i bankene til nå har vært å forstå
											kryptokundene sine.
										</p>
										<p>
											– Bankene er veldig forskjellige. Noen
											er mer fremoverlente og har over lengre
											tid hatt kunder som holder på med dette,
											og som har gode hvitvaskingsrutiner,
											mens andre er mye mer konservative.
											Det blir spennende å se om MiCA fører
											til at bankene i større grad begynner å
											tilby tjenester selv. Men det stiller krav
											til bankene. Har de tilstrekkelig kunnskap
											for å tilby tjenester innenfor kryptoindustrien?
											spør han.
										</p>
										<p>
											Banker får en enklere vei inn i krypto
											enn kryptoselskapene selv, fordi de slipper
											den ordinære søknadsprosess som
											de andre må forholde seg til. Bankene er
											gjennom sin konsesjon allerede underlagt
											hvitvaskingskontoll. De har etablerte
											systemer og prosesser på plass for å
											håndtere risiko.
											Bankene trenger
											derfor bare sende
											en notifikasjon til
											Finanstilsynet
											om at nå begynner
											de med tjenester
											som ligger
											under MiCA.
										</p>
										<p>
											For bankene
											blir det også
											enklere å forholde
											seg til
											kryptoselskaper
											som har fått godkjenning under MiCAreglene,
											enten de er norske eller fra et
											annet EØS-land.
										</p>
										<p>
											– Når det nå kommer et regelverk som
											gjør det mer forutsigbart og tryggere for
											selskapene å teste blokkjedeteknologi,
											vil det kunne akselerere innovasjonen i
											bransjen, sier Ribe.
										</p>
										<p>
											Han håper at MiCA vil bli implementert
											i Norge mot slutten av året samtidig
											som resten av MiCA skal gjelde for hele
											EU.
										</p>
									</div>

									<div class="col-md-4 md-mt30">  
										<div class="arti-listbox" style="border-color: #005897;">
											<div class="arti-listheading redbeforebox" style="border-color: #005897; color: #005897;">
												FAKTA OM MiCA
											</div> 
											<ul style="padding-inline-start: 40px;" class="font-FlamaL">
												<li>
													MiCA (Markets in Crypto Assets Regulation) inneholder
													felleseuropeiske regler om utstedelse, offentlig tilbud og
													opptak til handel av kryptoeiendeler, ytelse av tjenester
													knyttet til kryptoeiendeler og om markedsmisbruk i
													kryptoeiendelsmarkedet. Reglene skal blant annet bidra til å
													fremme innovasjon, investorbeskyttelse og markedsintegritet,
													samt sikre finansiell stabilitet.
												</li>
												<li>	
													Samtidig med MiCA kommer en forordning som pålegger
													ytere av kryptoeiendelstjenester, plikt til å gi opplysninger ved
													overføring av kryptoeiendeler, tilsvarende det som gjelder ved
													pengeoverføringer.
												</li>
												<li>
													Høring – gjennomføring av regler om kryptoeiendeler og
													pengeoverføringer (MiCA og TFR II)
												</li>
											</ul>
										</div>
									</div>  
								</div>    
							</div> 						 
						</div>
					</div>
					<!--row-->
				</section> 
				<div class="spcr40"></div>
			</div>
			<!-- //PAGE -->

			<!-- PAGE -->
			<div id="21" data-history="tech-nytt" class="swiper-slide">
				<title>TECH-NYTT</title>
				<section id="team" class="topSpacer">
					<div class="wrapper articles-contentsection">
						<div class="container inner_container">
							<!-- articles-container -->
							<div class="articlesborder-section border-block" style="border-color: #ffdd8b; padding: 0px !important;">  
								<div class="redbg-headingbox">
									<h2 class="articles-heading font-FlamaB font70" style="margin-bottom: 5px;">
										TECH-NYTT
										<span class="articles-textfotobox martop0 tech-textperson">
											Tekst: <span class="font-FlamaB">Nils Elmark</span> 
										</span>
									</h2>	 
								</div> 				

								<div style="padding: 30px;">    
									<div class="row"> 
										<div class="col-lg-6">
											<figure class="search-thumbs">
												<img src="images/tech-img1.jpg" alt="" class="mb10 img-social"> 
											</figure> 
										</div> 

										<div class="col-lg-6">  
											<h2 class="font-FlamaR font26 marbot20">
												Vil øke Gen X-investeringene
											</h2> 
											<p class="noIndent font-FlamaL font15">
												<span class="content-social">De to unge kvinnelige gründerne Avion Gray og Samantha Rosenberg har
												nettopp hentet inn tilsvarende 40 millioner kroner i startkapital til sin nye
												investeringsplattform Belong. Målgruppen er millennials – generasjon X.
												Ideen med den nye Fintech er at unge mennesker kan øke investeringene
												sine med 100 prosent. Invester £ 1 000, lån det samme fra Belong, og du
												har en fremtidig investering på £ 2 000. Renten for lånet ditt er 5–6 prosent
												og kan gjøres opp over 5 år. I tillegg til de nye pengene, har de to gründerne
												fått støtte fra en rekke erfarne venturefond, deriblant Nick Hungerford. Det
												startet investeringsroboten Muskatnøtt, som i 2021 ble kjøpt av J.P. Morgan
												for 10 milliarder kroner.
												</span>
											</p>
										</div>   
									</div> 

									<br>
									<hr>									
									<br> 

									<div style="background: #7e3e98; color: #fff; padding: 30px;">
										<div class="row">
											<div class="col-lg-7 align-self-center">
												<h2 class="font-bely font26 marbot20" style="line-height: 1.2 !important;">
													NU Bank nå med 100 millioner kunder
												</h2>

												<p class="noIndent font-FlamaL font15"> 
													Verdens mest suksessrike neobank har doblet kundetallet på to år og har nå
													100 millioner kunder. I 2023 ga den 10 år gamle banken et overskudd på én
													milliard dollar. Banken er nå i ferd med å ekspandere til Colombia og Mexico,
													hvor for øvrig britiske Revolut nettopp har fått sin banklisens. NU Bank er ikke
													alene. Det er mange neobank-konkurrenter i Brasil og omkringliggende land.
													Faktum er at neobankene i Sør-Amerika er på vei til å bli den “nye normalen”.
												</p>
											</div>

											<div class="col-lg-5">
												<br>
												<img src="images/tech-img2.png" alt="" style="position: relative; right: -30px;">
											</div> 
										</div>
									</div>

									<br>
									<hr>									
									<br> 

									<div class="row">
										<div class="col-lg-6 colBorder">
											<div>
												<img src="images/tech-img3.jpg" alt="">
												<br>

												<h2 class="font-FlamaR font26 marbot20" style="line-height: 1.2 !important;">
													<span class="font-FlamaB">SimpleClosure</span> – lukker selskapet ditt raskt
												</h2>

												<p class="noIndent font-FlamaL font15"> 
													Forretningsutvikling drives i stor grad av gründere som starter eget
													selskap. Men konkursraten er tilsvarende høy. 9 av 10 startups klarer
													det ikke, men å stenge et selskap er ikke bare et spørsmål om å overlevere
													nøklene til utleier. Det er en juridisk og økonomisk prosess, som
													tar i gjennomsnitt 9 måneder. Her må eieren løsrive seg fra avtaler og
													forpliktelser, rapportere til myndigheter og partnere. Et nytt amerikansk
													venturefinansiert selskap, SimpleClosure, har digitalisert denne prosessen
													ved hjelp av mye kunstig intelligens og har kuttet oppgjørstiden
													ned til to til tre uker. Via Internett samler det nye systemet all nødvendig
													informasjon, gjennomgår oppgavene som skal løses og avgjør dem,
													slik at det ikke er noen løse tråder og entreprenøren kan gå videre i
													livet sitt.
												</p>
											</div>
										</div>

										<div class="col-lg-6">
											<img src="images/tech-img4.jpg" alt="">
											<br>
											
											<h2 class="font-FlamaR font26 marbot20" style="line-height: 1.2 !important;">
												<span class="font-FlamaB">NUKE</span> – slett alt på mobilen
											</h2>

											<p class="noIndent font-FlamaL font15"> 
												Mange er redd for å miste mobilen, som i tillegg til å være et betalingsmiddel,
												også er fylt med praktisk talt all informasjon i livene våre. Du er bare
												ett tyveri unna å få din digitale identitet hacket. Dette er bakgrunnen for en
												ny app – NUKE (som i atombombe.) Den gjør det mulig fra hvilken som helst
												telefon å slette alle betalingskort, å blokkere SIM-kortet, samt sikre e-poster,
												sosiale medier og andre kontoer som kan misbrukes av kriminelle.
											</p>
										</div>
									</div>

									<br>
									<hr>									
									<br> 

									<div class="row"> 
										<div class="col-md-5 colBorder">
											<div>
												<img src="images/tech-img5.jpg" alt="">
												<br>

												<h2 class="font-FlamaR font26 marbot20">
													NSAVE – konto for personer med høy risiko
												</h2> 
												<p class="noIndent font-FlamaL font15">
													Amer Beraoudi og Abdallah AbuHasham har nettopp fått
													rundt 60 millioner kroner for neste steg mot realiseringen
													av bankappen sin. Den skal sikre folk i urolige og risikable
													land et trygt sted å oppbevare sparepengene sine. Baraoudi
													har selv opplevd hvordan familien hans i Syria mistet
													alt fordi de ikke kunne sette pengene sine på en sikker
													konto. Og i nabolandet Libanon sliter folk med 90 prosent
													inflasjon, uten mulighet, som de rike, til å sette pengene
													sine i en stabil valuta i for eksempel en sveitsisk bank. Så
													det er det de to gründerne har skapt: en sveitsisk bankkonto
													for alle de millioner av fattige og sårbare mennesker som
													ellers ikke har mulighet til å offshore bank. Bankappen
													‘nsave’ har satt i gang et omfattende system for å forhindre
													hvitvasking av penger, svindel og terrorisme.
												</p>
												<br><br>

												<img src="images/tech-img6.jpg" alt="">
												<br>

												<h2 class="font-bely font24 marbot20">
													Ukuelige fintechgründere
												</h2> 
												<p class="noIndent font-FlamaL font15">
													I 2023 måtte Jes Hennig, grunnleggeren av den tyske
													bankappen for tenåringer Ruuky, gi opp. Til tross for 250
													000 unge kunder, klarte ikke økonomien å takle hyperinflasjon,
													skyhøye energipriser, krig i Ukraina, pessimisme
													og nedgang i ventureinvesteringer. Men det har ikke holdt
													igjen Jes Hennig. På Berlins Fintech Festival FIBA var
													Hennig igjen på scenen, og denne gangen pitchet han sin
													nye oppstart Forget Finance. Det nye fintech-selskapet
													vil hjelpe unge par med å spare sammen – typisk for
													kortsiktige mål og med små løpende betalinger. Bakgrunn:
													Analyser viser at ett av to unge par krangler om pengesaker
													– derfor «Glem finans» – sparepengene dine løper
													automatisk i bakgrunnen.
												</p> 
											</div>
										</div> 

										<div class="col-md-7 md-mt30">  
											<img src="images/tech-img7.jpg" alt="" style="box-shadow: 0px 0px 5px #ddd;">
											<br>

											<h2 class="font-FlamaR font24 marbot20">
												<span class="font-FlamaB" style="color: #df195a;">Monzo</span> – på vei til Amerika
											</h2> 
											<p class="noIndent font-FlamaL font15">
												Den britiske neobanken Monzo er på vei til USA. Banken er angivelig verdt $ 5 milliarder,
												har 9 millioner britiske personlige kontoer og 400 000 bedriftskontoer, og har nettopp
												samlet inn $ 400 i ny risikokapital, inkludert fra Google. Neobanken er enormt populær
												blant britiske kunder og de fleste nye kunder, blir med via jungeltelegrafen. Til tross for
												at banken nylig har begynt å tilby lån og investeringer, balanserer banken, noe som
												understreker styrken i den digitale online forretningsmodellen. De nye pengene fra
												Google skal blant annet brukes til å utvikle flere nye produkter og bringe Monzo inn i det
												amerikanske markedet. De prøvde å etablere seg på den andre siden av Atlanterhavet i
												2021, men ga opp. Nå prøver de igjen.
											</p>
											<br><br>

											<div style="background: #f69068;">
												<div class="pad20" style="color: #fff;">
													<h2 class="font-bely font26 marbot20">
														Det er penger i stemmerett
													</h2> 
													<p class="noIndent font-FlamaL font15">
														Shareholder Vote Exchange er verdens første markedsplass for stemmerett.
														Det er en online plattform hvor investorer kan kjøpe, selge og handle stemmerett.
														Passive aksjonærer som ikke planlegger å stemme på generalforsamlingen,
														kan øke utbyttet ved å overlate stemmeretten til aktive investorer
														som ønsker mer innflytelse på selskapets strategi. Bare 29 prosent av private
														equity-institusjonene stemmer, mens 82 prosenr av institusjonelle gjør det.
														Ifølge grunnleggerne av Shareholder Vote Exchange, har det stort økonomisk
														potensial. Stemmer er lik innflytelse og makt, og det er penger i det.
													</p>
												</div>
											</div>
											<br><br>

											<img src="images/tech-img8.jpg" alt="">
											<br>

											<h2 class="font-FlamaR font24 marbot20">
												<span class="font-FlamaB">Lightbringer</span> – AI sikrer rask og billig patentsøknad
											</h2> 
											<p class="noIndent font-FlamaL font15">
												Alle som har forsøkt å patentere sine gode ideer har måttet innrømme at det er en
												langvarig og kostbar prosess, som lett kan komme opp i 100 000 kroner. Nå har den
												svenske oppstartsbedriften Lightbringer tatt i bruk kunstig intelligens, noe som har
												redusert kostnadene – ingen dyre advokatregninger – og har fremskyndet prosessen.
												Det som pleide å ta uker, gjøres nå på én time av Lightbringers algoritme. Og det som
												også er interessant: patentsøknadene som ChatGPT designer, er av bedre kvalitet med
												færre feil enn de menneskeskapte.
											</p>
										</div>   
									</div>  
								</div> 
							</div> 
							<!-- //articles-container -->
						</div>
					</div>
					<!--row-->
				</section> 
				<div class="spcr40"></div>
			</div>
			<!-- //PAGE -->

			<!-- PAGE -->
			<div id="22" data-history="80-prosent-far-mer-enn-minimum" class="swiper-slide">
				<title>80 prosent får mer enn minimum</title>  
				<section id="team" class="topSpacer">
					<div class="wrapper articles-contentsection">
						<div class="container inner_container">
							<!-- articles-container --> 
							<div class="articlesborder-section pad30 relative-box border-block" style="border-color: #124645;">  
								<div class="articles-categorybox">
									<span class="font-FlamaB bg-green font14" style="color: #fddd8f;">MEDLEM</span>  
									<div class="articategory-content font-FlamaR">Lønnsoppgjøret</div>
								</div> 
								<br>

								<h2 class="articles-heading font-opulent font56">
									80 prosent får mer enn minimum
								</h2>  
								<h3 class="intro-text content-social">
									1. mai fikk 80 prosent av de ansatte i finansnæringen et høyere generelt tillegg enn minimumstillegget på 15 000 kroner. I år ble hele 53 prosent av lønnsrammen gitt som generelt tillegg. Nå starter de lokale lønnsforhandlingene.
								</h3>

								<div class="articles-textfotobox">
									Tekst:<span class="font-FlamaB"> Svein Åge Eriksen</span>
								</div>  
								<br> 
								<br>	

								<div class="row">
									<div class="col-md-12"> 
										<p class="noIndent">
											<span class="drop-txt font-FlamaT color-green">R</span> esultatet av årets lønnsoppgjør lå marginalt under resultatet av fjorårets oppgjør. I år ble det gitt et generelt tillegg til alle på 2,65 prosent, det samme som i fjor. Mens minimumstillegget i 2023 var 18 000 kroner, ble dette 15 000 kroner i år.
										</p>
										<p>	
											– Utgangspunktet for lønnsoppgjørene i Norge er den lønnsrammen som blir forhandlet frem av konkurranseutsatt industri i frontfaget. I år ble rammen på 5,2 prosent. Fra lønnsoppgjøret i fjor fikk vi med et stort overheng på 1,8 prosent. Med større overheng i år var mulighetsrommet litt mindre for det generelle tillegget, når vi også ville ha rom til gode lokale oppgjør. Det vi endte opp med, var et generelt tillegg på 1,8 prosent på årsbasis, som utgjør 2,65 prosent per 1. mai, forklarer sjeføkonom Sven Eide i Finansforbundet, som legger til at anslaget for lønnsglidning er på 1,6 prosent.
										</p> 

										<div class="articles-subtitle">PRISENE ER JOKEREN</div>
										<p class="noIndent font-PalatinoI">
											I flere år har de fleste arbeidstakere hatt en reallønnsnedgang, siden prisene og rentene har steget mye mer enn lønningene. Derfor var det et helt klart mål i årets lønnsforhandlinger at flest mulig av de ansatte skulle få en merkbar reallønnsøkning. Men det forutsetter at prisene i år ikke øker mer enn 4,1 prosent, slik Teknisk beregningsutvalg (TBU) har lagt til grunn før årets lønnsoppgjør.
										</p>
										<p>
											– Det er mange faktorer som påvirker prisene i løpet av året. Den svake kronekursen kan føre til at vi får mer importert inflasjon. Dette er en hodepine for Norges Bank når de skal fastsette rentene, men det er også en utfordring hva andre sentralbanker foretar seg. Hva Norges Bank gjør med renten, er avhengig av rentesettingen ute og ikke minst hva som skjer i USA. I mai valgte Norges Bank å beholde styringsrenten uendret, samtidig som de signaliserte at høstens rentenedgang kan komme senere enn tidligere varslet. Derfor ligger det ikke an til noen rentenedgang fra Norges Bank tidlig til høsten, sier Eide.
										</p>
										<p class="font-PalatinoI">
											– Hva skjer hvis TBU igjen bommer på prisveksten i år?
										</p>
										<p>
											– Da skylder arbeidsgiverne oss penger. Prisveksten er jokeren i lønnsoppgjøret. Først neste år får vi vite om årets prognoser holder. Det blir veldig kjedelig hvis TBU nok en gang bommer på prisprognosen, siden det ikke er mye rom for å lande en høyere lønnsramme i frontfaget. Det er viktig å huske på at vi ikke får ned prisveksten med en enda høyere lønnsvekst. Derfor må vi hele tiden passe på at vi ikke får en lønns- og prisspiral. For å få bukt med den høye prisveksten, må økonomien avkjøles og kronekursen stabiliseres. Her ligger det et stort ansvar hos dem som forhandler frontfaget, og hvordan det påvirker prisveksten i Norge.
										</p>

									  	<div class="articles-subtitle">FÅR MER LOKALT</div>
										<p class="noIndent">
											Nå starter de lokale lønnsoppgjørene i bedriftene. Her skal de tillitsvalgte og ledelsen i felleskap fordele det som er igjen etter at det generelle tillegget er gitt. I år ligger det an til en forventet lønnsglidning lokalt på 1,6 prosent.
										</p>
										<p>	
											– Utgangspunktet for de lokale lønnsforhandlingene er at bedriftens lønnsomhet og inntjening skal, sammen med samfunnsøkonomiske hensyn, være retningsgivende for lønnsdannelsen, sier sjeføkonom Sven Eide.
										</p>
										<p>	
											Det er ingen hemmelighet at bedriftene i finansnæringen, og bankene spesielt, leverte svært gode resultater i 2023. I mange bedrifter ble det også delt ut tidenes bonus, som blant annet i SR-Bank der alle ansatte fikk 10 prosent av brutto lønn. Men det har ikke lagt noen demper på forventningene hos medlemmene ute i bedriftene.
										</p>

									  	<div class="articles-subtitle">MER TIL LAVTLØNTE</div>
									  	<p class="noIndent font-PalatinoI">
									  		–Hvordan kommer de lavest lønte ut i årets lønnsoppgjør?
										</p>
										<p>	
											– De som ligger lavest på lønnsregulativet, får opp mot 3,8 prosent i lønnsøkning. Det er fordi innretningen av det sentrale tillegget og måten de lokale lønnstrinnene fordeles, gir en bedre utvikling for dem med lavere lønn. I fjor fikk hele 80 prosent av de ansatte i finansnæringen et individuelt lønnstillegg i bedriften der de arbeider.
										</p>
										<p>	
											– Hva er det viktigste de tillitsvalgte kan gjøre i de lokale forhandlingene for at flest mulig skal få mest mulig i lokalt lønnstillegg? Eller har bedriftene delt ut “alt” på forhånd gjennom rause bonusutbetalinger?
										</p>
										<p>	
											– Bonuser er jo ikke med i regnestykke når lokale potter skal tildeles, så det skal ikke ha noe å si. De tillitsvalgte må forberede seg godt, ha en god prosess i bedriften og avtale gode kjøreregler i forveien for hvordan man skal gå frem. Avklar hvordan det går med bedriften, lønnsomheten, lønnsnivået med sammenlignbare bedrifter i næringen, avslutter Sven Eide.
									  	</p>
									  	<p>
									  		<br>
									  		Les alt om lønn på Finansforbundets nettside:<br>
									  		<a href="https://www.finansforbundet.no/lonn/" target="_blank">https://www.finansforbundet.no/lonn/</a>
									  	</p>
									</div>  
								</div> 
							</div>
						</div>
					</div>
					<!--row-->
				</section> 
				<div class="spcr40"></div>
			</div>
			<!-- //PAGE -->

			<!-- PAGE -->
			<div id="23" data-history="24%-til-ansatte-i-inkasso" class="swiper-slide">
				<title>2,4 % til ansatte i inkasso</title>  
				<section id="team" class="topSpacer font-FlamaL">
					<div class="wrapper articles-contentsection">
						<div class="container inner_container">
							<!-- articles-container --> 
							<div class="articlesborder-section pad30 relative-box border-block" style="border-color: #124645;">  
								<div class="articles-categorybox">
									<span class="font-FlamaB bg-green font14" style="color: #fddd8f;">MEDLEM</span>  
									<div class="articategory-content font-FlamaR">Lønnsoppgjøret</div>
								</div> 
								<br> 
								 	

								<div class="row">
									<div class="col-md-7 colBorder" style="border-right-color: #000;">
										<div class="imgbox mb30"> 
											<a class="imgpopup" href="images/artiP43-banner.jpg" data-fancybox="p43" data-caption="SIGNERER PROTOKOLL: Therese Høyer Grimstad i Finans Norge og Arne Fredrik Håstein fra Finansforbundet er enige i årets lønnsoppgjør. FOTO: FINANSFORBUNDET"> 
												<img src="images/artiP43-banner.jpg" alt="">
											</a> 
											<div class="images-caption">
												<strong>SIGNERER PROTOKOLL:</strong> Therese Høyer Grimstad i Finans Norge og Arne Fredrik Håstein fra Finansforbundet er enige i årets lønnsoppgjør. FOTO: FINANSFORBUNDET
											</div>
										</div>

										<h2 class="articles-heading font-FlamaB font30">
											2,4 % til ansatte i inkasso
										</h2>  
										<p class="noIndent">
											Finansforbundet og Finans Norge inkasso er kommet til enighet om resultatet i lønnsoppgjøret for inkassoansatte. Det gis et generelt tillegg på 2,4 prosent.
										</p>
										<p>	
											– Dette er et resultat vi er fornøyde med. Det er innenfor en total ramme på 5,2 prosent, som er i tråd med frontfaget. Det er en ramme som vil gi de fleste av våre medlemmer reallønnsvekst, sier forhandlingsleder og nestleder i Finansforbundet Arne Fredrik Håstein.
										</p>
										<p>	
											– Inkasso er en bransje som for tiden opplever tøffe tider. Resultatet i lønnsoppgjøret reflekterer dette, men vi har likevel klart å forhandle frem et godt generelt tillegg som ivaretar våre medlemmer, sier Håstein.
										</p>
										<p>	
											Nå forventer han at bedriftene følger opp med gode lokale oppgjør.
										</p>
										<p>	
											Forhandlingene omfatter 615 medlemmer i inkassoselskapene Alektrum, Axactor, Intrum, Kredinor, Lowell og Visma Amili.
									  	</p>
									</div>  
									<div class="col-md-5 md-mt30">
										<div class="imgbox mb30"> 
											<a class="imgpopup" href="images/artiP43-sub1.jpg" data-fancybox="p43" data-caption="FULL ENIGHET: Per Christian Larsen, spesialrådgiver i Finansforbundet, og Mona Sørensen, hovedtillitsvalgt, Norges Bank, forhandlet på vegne av Finansforbundet."> 
												<img src="images/artiP43-sub1.jpg" alt="">
											</a> 
											<div class="images-caption">
												<strong>FULL ENIGHET:</strong> Per Christian Larsen, spesialrådgiver i Finansforbundet, og Mona Sørensen, hovedtillitsvalgt, Norges Bank, forhandlet på vegne av Finansforbundet. FOTO: FINANSFORBUNDET
											</div>
										</div>

										<h2 class="articles-heading font-FlamaB font30">
											13 650 til alle i Spekter
										</h2>  
										<p class="noIndent">
											Lønnsoppgjøret i Spekter A-del er i mål. Det gis et lønnstillegg på minimum kr 13 650 til alle. Nå starter de lokale B-delsforhandlingene i den enkelte bedrift. Her avgjøres de endelige lønnstilleggene. 
											<br><br>

											<span class="font-FlamaB">Spekter-området omfatter i overkant av 260 medlemmer i følgende bedrifter:</span>
									  	</p>

									  	<ul>
									  		<li>
									  			Innovasjon Norge
											</li>
											<li>
												Kommunalbanken
											</li>
											<li>
												Norges
											</li>
											<li>
												Vertikal Helseassistanse AS
									  		</li>
									  	</ul>
									  	<br>

									  	<p>
									  		Les mer om Spekter-forhandlingene på <a href="https://ys.no" target="_blank">ys.no</a>
									  	</p>
									</div>  
								</div> 

								<hr style="border-top-color: #000; margin: 30px 0;">

								<div class="row">
									<div class="col-md-6">
										<div class="imgbox mb30"> 
											<a class="imgpopup" href="images/artiP43-sub2.jpg" data-fancybox="p43" data-caption="BLE ENIGE: Daniel Fundingsrud og Astrid Flesland fra Virke, nestleder Arne Fredrik Håstein og spesialrådgiver Vegard Thorbjørnsen fra Finansforbundet. (Foto: Finansforbundet)"> 
												<img src="images/artiP43-sub2.jpg" alt="">
											</a> 
											<div class="images-caption">
												<strong>BLE ENIGE:</strong> Daniel Fundingsrud og Astrid Flesland fra Virke, nestleder Arne Fredrik Håstein og spesialrådgiver Vegard Thorbjørnsen fra Finansforbundet. (Foto: Finansforbundet)
											</div>
										</div>
									</div>  
									<div class="col-md-6 md-mt30"> 
										<h2 class="articles-heading font-FlamaB font30">
											Virke-oppgjøret i havn
										</h2>  
										<p class="noIndent">
											Finansforbundet og Virke er enige i årets lønnsforhandlinger. Oppgjøret har en total ramme på 5,2 prosent. 
										</p>
										<p>
											Det gis sentrale tillegg fra to datoer; 1. februar (garantilønnsjusteringen) og 1. april. Det vil nå være lokale lønnsoppgjør i forlengelsen.
									  	</p>

									  	<div class="articles-subtitle">DETTE BLE RESULTATET:</div>
									  	<p>
									  		Det gis et generelt tillegg på kr 4,- per time med virkning fra 1.april. Minstelønnssatsene økes tilsvarende.
										</p>
										<p>	
											Minstelønnssatsene på lønnstrinn 1–4 heves med ytterligere kr 2,- med virkning fra 1. april.
										</p>
										<p>	
											Garantilønnjustering (fra 1. februar 2024): Trinn 6 gis et tillegg på kr 8,45 per time og trinn 5 gis et tillegg på kr 5,- per time.
									  	</p>
									</div>  
								</div> 
							</div>
						</div>
					</div>
					<!--row-->
				</section> 
				<div class="spcr40"></div>
			</div>
			<!-- //PAGE -->

			<!-- PAGE -->
			<div id="24" data-history="53-prosent-onsker-tidligpensjon" class="swiper-slide">
				<title>53 prosent ønsker tidligpensjon</title>  
				<section id="team" class="topSpacer">
					<div class="wrapper articles-contentsection">
						<div class="container inner_container">
							<!-- articles-container --> 
							<div class="articlesborder-section pad30 relative-box border-block" style="border-color: #124645;">  
								<div class="articles-categorybox">
									<span class="font-FlamaB bg-green font14" style="color: #fddd8f;">MEDLEM</span>  
									<div class="articategory-content font-FlamaR">Seniorundersøkelse</div>
								</div> 
								<br>

								<h2 class="articles-heading font-FlamaB font50 color-green">
									53 prosent ønsker tidligpensjon
								</h2>  
								<h3 class="intro-text content-social">
									Et flertall av 60-åringene i finansnæringen vil ikke
									jobbe til de fyller 67. Nå kan Finansfokus avsløre
									hva som må til for å få seniorene til å jobbe lenger.
									Stikkordene er mer anerkjennelse, fleksibel
									arbeidstid og interessante arbeidsoppgaver.
								</h3>

								<div class="articles-textfotobox">
									Tekst:<span class="font-FlamaB"> Svein Åge Eriksen</span>
									Illustrasjonsfoto: <span class="font-FlamaB"> Shutterstock</span>
								</div>  
								<br>

								<figure class="search-thumbs">
									<img class="img-social" src="images/artiP44-banner.jpg" alt="bannerimg">
								</figure>
								<div class="images-caption">
									<span class="font-FlamaB">TRAPPER NED:</span> Altfor mange seniorer ønsker ikke å arbeide etter fylte 62 år.
								</div>
								<br>	

								<div class="row">
									<div class="col-md-8 colBorder"> 
										<p class="noIndent">
											<span class="drop-txt font-FlamaT color-green">N</span> ylig la Finansforbundet frem
											en undersøkelse som viser at
											et flertall av de ansatte over 60
											år, ikke ønsker å jobbe til de
											blir 67 år.
										</p>
										<p>
											– I et arbeidsliv hvor vi ønsker at flere
											skal jobbe lenger, og det er behov for
											mer arbeidskraft, er det en utfordring
											at over halvparten ønsker tidligpensjon,
											sier forbundsleder Vigdis Mathisen.
										</p>
										<p>
											Viktige forutsetninger for å få seniorene
											til å jobbe lenger er
											aksept og anerkjennelse,
											fleksibel arbeidstid og
											-sted samt interessante
											arbeidsoppgaver. Eldre
											arbeidstakere ønsker å bli
											sett på som en ressurs, og
											at de får mulighet for
											videreutvikling. Deltakelse
											i prosjekter nevnes
											som motiverende.
										</p> 

										<div class="articles-subtitle">DISKUTERER LØSNINGER</div>
										<p class="noIndent"> 
											Storebrands hovedkontor på Lysaker
											grytidlig en torsdag morgen. Bærekraftnettverket
											No17 og Anne Jorunn Ravndal
											inviterer til en fremtidssamtale hos
											Storebrand. Målet er å finne ut hva som
											skal til for at seniorene skal stå lenger i
											jobb. Til å svare på spørsmålet er det
											invitert kunnskapsrike innledere fra
											politikk, det offentlige og næringslivet
											for å diskutere løsninger for fremtidens
											arbeidsliv. Fra finansnæringen deltar
											Arne Fredrik Håstein, nestleder i Finansforbundet
											og Tove Selnes, konserndirektør
											People i Storebrand.
										</p>
										<p>
											Tidligere stortingspresident Tone
											Wilhelmsen Trøen (H) og leder av helseog
											omsorgskomiteen på Stortinget,
											innleder til debatt.
										</p>
									</div>

									<div class="col-md-4 md-mt30">
										<div class="quate-box font-FlamaB mt15 mb0"> 
											<span class="color-green">”</span>Seniorer
											har stor
											verdi for
											bedriftene.<span class="color-green">”</span> 
											<div class="name-onquate font-FlamaL">
												Arne Fredrik Håstein
											</div>
										</div>
									</div>

									<div class="col-md-8 colBorder">
									  	<div class="articles-subtitle">IKKE BÆREKRAFTIG</div>
										<p class="noIndent">
											– Hvis vi skal sikre et bærekraftig pensjonssystem,
											så må flere stå i jobb lenger.
											Hvis pensjonsalderen
											står stille, forventet levealder
											øker samtidig som
											færre står i jobb, så går
											det rett og slett ikke opp.
											Et slikt pensjonssystem
											er ikke bærekraftig over
											tid, sier Trøen til en lydhør
											forsamling.
										</p>
										<p>
											Hun peker på at det
											ikke bare er Stortinget og
											politikerne som skal løse alt. Arbeidslivet
											må også legge til rette for at seniorene
											kan stå lenger i arbeid. Derfor må
											vi snakke om holdningene og kulturen
											i næringslivet. Hvordan tar vi vare på
											de ansatte og den verdifulle kompetansen
											de har fått gjennom mange år?
										</p>
										<p>
											– Det vi vet, er at veldig mange opplever
											aldersdiskriminering. Sakte, men
											sikkert mister du dine arbeidsoppgaver.
											Du er ikke med i de samme prosjektene
											lenger. Til slutt kommer spørsmålet: Når
											tenker du å gå av med pensjon? Dette
											er en holdning vi er nødt til å endre,
											understreker Tone Wilhelmsen Trøen.
										</p> 

										<div class="articles-subtitle">FLEKSIBLE LØSNINGER</div>
										<p class="noIndent">
											Storebrand er opptatt av at likestillingsarbeidet
											skal komme alle til gode.
											Derfor jobber de med å skape en organisasjon
											der alle blir involvert og hørt.
											Selskapet jobber spesielt med å etablere
											psykologisk trygghet. Likestillingsarbeidet
											er ikke rettet mot aldersbestemte
											grupper. I stedet har forsikringsselskapet
											satset på fleksible løsninger for å
											skape et bærekraftig arbeidsliv.
										</p>
										<p>
											– I Storebrand skal vi investere i din
											kompetanse, og du må lene deg frem
											og bidra sterkt. Det handler ikke om
											alder. Her fikk vi litt tilsnakk fra de tillitsvalgte
											for noen år siden: “Dere snakker
											bare om unge og digital kompetanse
											som om vi som har jobbet her lenge,
											ikke betyr noe”. Digital kompetanse har
											ingen aldersgrense. Kompetanse er den
											viktigste nøkkelen, og vi skal hjelpe til 
											slik at individene kan bidra her, sier
											Selnes.
										</p>
										<p>
											Konserndirektøren er opptatt av å få
											flest mulige til å arbeide lenger, men
											egentlig er det ikke 70- eller 72-års grensen
											som er utfordringen. Den aller største
											utfordringen er å få flere til å jobbe
											lenger enn 62.
										</p>
									</div>  


									<div class="col-md-8 colBorder"> 
										<div class="articles-subtitle">70 DET NYE 50</div>
										<p class="noIndent">
											– Likestilling, mangfold og inkludering
											er viktig for å skape en effektiv, innovativ
											og produktiv organisasjon med store
											overskudd som kan deles med de
											ansatte, sier nestleder Arne Fredrik
											Håstein i Finansforbundet.
										</p>
										<p>
											Han viser til hvilken kraft som ligger
											i trepartssamarbeidet mellom arbeidstakere,
											arbeidsgivere og myndighetene
											for å finne gode løsninger for å få flere
											til å arbeide lenger. Det er helt avgjørende
											at arbeidsgiverne sier tydelig fra
											at de trenger nettopp din arbeidskraft.
										</p>
										<p>
											– Det er for lengst opplest og vedtatt
											at vi må få flere til å stå lenger i arbeidet
											for å finansiere pensjonssystemet, slik
											at vi alle har noe å leve av den dagen vi
											blir pensjonister. Derfor trenger vi alle
											hender vi kan få. Vi må slutte å stigmatisere
											seniorene. 70 er jo det nye 50.
										</p>
									</div>

									<div class="col-md-4 md-mt30">
										<div class="quate-box font-FlamaB mt15 mb0"> 
											<span class="color-green">”</span>Digital
											kompetanse
											har ingen
											aldersgrense.<span class="color-green">”</span> 
											<div class="name-onquate font-FlamaL">
												Tove Selnes
											</div>
										</div>
									</div>


									<div class="col-md-8 colBorder">
										<div class="articles-subtitle">MER ANERKJENNELSE</div>
										<p class="noIndent">
											Finansforbundet og
											Finans Norge spurte i
											2017 seniorene om hva
											som skal til for at de
											står lenger i jobb. En
											ny seniorundersøkelse
											ble gjennomført i mars
											i år. Men svaret på det
											viktige spørsmålet har
											ikke endret seg.
										</p>
										<p>
											– Vi må anerkjenne
											at seniorer har stor
											verdi for at bedriftene skal kunne levere
											gode resultater. Seniorene ønsker fleksibel
											arbeidstid og interessante arbeidsoppgaver
											i et godt arbeidsmiljø. De må 
											ha en psykologisk trygghet for arbeidsplassen,
											slik at de slipper å holde deg
											fast i pulten, understreker Håstein.
										</p>
										<p>
											Han peker på at det aller viktigste er
											at alle ansatte bygger opp egen kompetanse
											gjennom hele livet. Finansforbundet
											og Finans Norge har fått 45 millioner
											kroner over tre år for å utvikle et bransjeprogram
											for finansiell
											sektor. Her får de
											ansatte tilbud om
											korte og gratis kurs
											spesielt innen bærekraft
											og teknologi.
											Det nye tilbudet
											kommer i tillegg til
											Finansforbundets
											totale satsing på
											kompetanse der det
											er satt av 18 millioner
											over tre år til gratis kursplasser og studier
											for medlemmene.
										</p>
									</div>
								</div> 
							</div>
						</div>
					</div>
					<!--row-->
				</section> 
				<div class="spcr40"></div>
			</div>
			<!-- //PAGE -->


			 
			 
		</div>
		<!-- //swiper-wrapper -->
		<div class="swiper-button-next"></div>
		<div class="swiper-button-prev"></div>
	</div>	
	<!-- //Swiper container -->



	<!-- Button trigger modal --> 
	<div class="modal fade" id="shareContentModal" tabindex="-1" role="dialog" aria-labelledby="shareContentModalTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">  
					<img class="close" src="images/innhold/close.png"  data-dismiss="modal" aria-label="Close">

					<h5 class="modal-title share-modal-heading">Del Artikkel</h5>			        	
				</div>
				<div class="modal-body share-modal-container">
					<div class="del-pop-btmsection social-share-group"> 
						<div class="del-pop">  
							<div class="share-social"> 
								<ul> 
									<li>
										<a class="social-icon-facebook" href="#" target="_blank" style="color: #0866ff;" data-toggle = "tooltip" title = "Facebook">
											<i class="fab fa-facebook-f"></i>
										</a>
									</li>
									<li>
										<a class="social-icon-twitter" href="#" target="_blank" data-toggle = "tooltip" title = "Twitter">
											<i class="fa-brands fa-x-twitter"></i>
										</a>
									</li>
									<li>
										<a class="social-icon-linkedin" href="#" target="_blank" style="color: #0a66c2" data-toggle = "tooltip" title = "LinkedIn">
											<i class="fab fa-linkedin-in"></i>
										</a>
									</li>
									<li>
										<a class="social-icon-envelope" href="#" style="color: #f4366b;" data-toggle = "tooltip" title = "Email">
											<i class="far fa-envelope"></i>
										</a>
									</li>
								</ul>
							</div> 
							<div class="copy-url">
								<small>Eller kopier lenken</small>
								<span class="url-holder text-holder"></span>
								<button class="btn" data-clipboard-action="copy" data-clipboard-target=".url-holder">Copy</button> 
							</div>
						</div>
					</div>
				</div> 
			</div>
		</div>
	</div>


	<!-- Script links -->
	<script src="<?=$url;?>js/jquery.js"></script> 	
	<script src="<?=$url;?>js/bootstrap.min.js"></script>	    
	<script src="<?=$url;?>js/jquery.fancybox.min.js"></script>
	<script type="application/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.10.1/html2pdf.bundle.min.js"></script>
	<script src="<?=$url;?>js/swiper.min.js"></script>
	<script src="<?=$url;?>js/main.js"></script>
	<script src="<?=$url;?>js/clipboard.min.js"></script>
	<script src="<?=$url;?>js/share.js"></script>
     


</body>
</html>
