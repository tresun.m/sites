var clipboard = new ClipboardJS('.btn');

$(document).ready(function(){
	$('.share').click(function(){
		$('.url-holder').html('');
		var id = $('.swiper-slide.swiper-slide-active').attr('id');
		// var id = $(this).parents('.swiper-slide').attr('id');
		var content = $('#'+id).html();

		var headerLogo = $('#header #menu .navbar-header').find('a#ghar').html();
		var header = '<div id="header"><nav id="menu" class="navbar main-nav">\
		<div class="wrapper"><div class="navbar-header">\
		<a id="ghar" class="scroll navbar-brand">'+headerLogo+'</a></div></div></nav></div>';
		var metaurl = $('meta[property="og:url"]').attr("content");
		// var metaimage = $('meta[property="og:image"]').attr("content");\
		var basehref = $('base').attr('href');
		console.log(basehref);
		var metaimage = basehref+$('#'+id+' img.img-social').attr('src');
		// var metaimage = basehref+$('#'+id+' .social-image').val();
		// console.log(metaimage);
		// var metatitle = $('meta[property="og:title"]').attr("content");
		var metatitle = $('#'+id+' title').html();
		console.log('title');
		console.log(metatitle);
		// var metadescription = $('meta[property="og:description"]').attr("content");
		var metadescription = $('#'+id+' .content-social').html();
		console.log(metadescription);


		content = header.concat(content);
		// console.log(content);

		var path = window.location.pathname;
		var path = path.split('/');

		var imageBaseURl = window.location.origin+'/';

		if(imageBaseURl != 'https://dittmagasin.no/'){
			for (i = 1; i < path.length - 1; i++) {
				imageBaseURl += path[i] + "/";
			}
		}else{
			imageBaseURl = $('base').attr('href');
		}




		var email = 'globdig@gmail.com';
		var url = "https://dittmagasin.no/api/share-url/store";
		
		var settings = {
			"url": url,
			"method": "POST",
			"async": "false",
			"data": {email: 'globdig@gmail.com', metaurl:metaurl,metaimage:metaimage,metatitle:metatitle,metadescription:metadescription,content: content, imageBaseURl: imageBaseURl,base_url:basehref}
		};

		$.ajax(settings).done(function (response) {
			if(response.status == 'success'){
				var url = "https://dittmagasin.no/magasin/share-url/" + response.short_url_code;
				var fbShareUrl = 'https://facebook.com/sharer/sharer.php?u='+url;
				var twitterShareUrl = 'http://twitter.com/share?url='+url;
				var linkedinShareUrl = 'https://www.linkedin.com/shareArticle?mini=true&url='+url;
				var mailShareUrl = 'mailto:?subject=Finansfokus: '+metatitle+'&body='+url;
				$('.url-holder').html(url);
				$('.social-icon-facebook').attr('href', fbShareUrl);
				$('.social-icon-twitter').attr('href', twitterShareUrl);
				$('.social-icon-linkedin').attr('href', linkedinShareUrl);
				$('.social-icon-envelope').attr('href', mailShareUrl);
			}
		});
	});
});