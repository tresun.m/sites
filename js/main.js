$(document).on('click','#print', function(e) {
	var id = $('.swiper-slide.swiper-slide-active').attr('id');
	var element = document.getElementById(id);

	var opt = {
		margin:       1,
		filename:     id + '.pdf',
		image:        { type: 'jpeg', quality: 1 },
		html2canvas:  { scale: 1 },
		jsPDF: { format: 'a2'}
	};
	// New Promise-based usage:
	html2pdf().set(opt).from(element).save();
	// window.print();
});

var swiper = new Swiper('.swiper-container', {
	pagination: '.swiper-pagination',
	paginationClickable: true,
	spaceBetween: 0,
	autoHeight: true,
	   hashNavigation: true,
	navigation: {
	  nextEl: '.swiper-button-next',
	  prevEl: '.swiper-button-prev',
	},
	//history: {
	//	key: '',
	  //}
});	

// function resizeSwiperContainer() {
// 	var swiper = new Swiper('.swiper-container', {
// 		pagination: '.swiper-pagination',
// 		paginationClickable: true,
// 		spaceBetween: 0,
// 		autoHeight: true,
// 		   hashNavigation: true,
// 		navigation: {
// 		  nextEl: '.swiper-button-next',
// 		  prevEl: '.swiper-button-prev',
// 		},
// 	});	
// }

// $(document).ready( function(e) {
// 	resizeSwiperContainer();
// })

$(document).on('click','#menu li a',function(){
    if($(this).parent().hasClass('dropdown'))
    {
        return false;
    }
    $lists=$('#menu li a');
    var curPos = 0;
    var dropdowncount=0;
    $object=this;
    for($i=0;$i<$lists.length;$i++)
    {
      if(curPos==0)
      {
        if($($lists[$i]).parent().hasClass('dropdown'))
        {
            dropdowncount++;
        }
      }
      if($lists[$i]==$object)
      {
        curPos=$i;
      }
    }
   $('#collapse-menu-box').removeClass('in');
    curPos-=dropdowncount;
    swiper.slideTo(curPos,300,function(){});

    window.scrollTo(0,0);
 });
/*======================================
nav toggle js
=======================================*/

$('.inhold').click(function(){
	$("#nav-sidebar").addClass("nav-sidebar-open"); 
	$("body").addClass("fixed-body"); 
	$('.inhold-overlay').css("visibility", "visible");
});

$('.nav-slider-close, .slider-thumb-container').click(function(){
	$("#nav-sidebar").removeClass("nav-sidebar-open"); 
	$("body").removeClass("fixed-body"); 
	$('.inhold-overlay').css("visibility", "hidden");
});
$('.more-option').click(function(){
	$(".more-container").toggleClass("more-option-show");  
}); 
$('.inhold').click(function(){
	$(".more-container").removeClass("more-option-show");  
}); 
 
/*======================================
site navigation display
=======================================*/
$('.indexLinktoPage').click(function(){
	$('.site-navigation').css("display", "none");
	$('.toogle-icon').removeClass('open');
});

/*======================================
fancybox
=======================================*/
$('imgpopup').fancybox({
	transitionEffect: "zoom-in-out",
	loop: true
});

/*======================================
body css setTimeout
=======================================*/	 
setTimeout('$("body").css("visibility","visible");', 1000);
	

/*======================================
swiper css
=======================================*/	
// var swiper = new Swiper('.swiper-container', {
// 	pagination: '.swiper-pagination',
// 	paginationClickable: true,
// 	spaceBetween: 0,
// 	autoHeight: true,
//    	hashNavigation: true,
// 	navigation: {
//       nextEl: '.swiper-button-next',
//       prevEl: '.swiper-button-prev',
// 	},
// 	//history: {
// 	//	key: '',
// 	  //}
// });	


/*======================================
swiper scrolltop js
=======================================*/
swiper.on('slideChange', function () {
	// alert('a')
	document.documentElement.scrollTop = window.pageYOffset = document.body.scrollTop = 0;
	// var slides = $('#' + String(parseInt(swiper.realIndex) + 1));
    // var title = slides.attr('data-history');
    // var docTitle = $(slides).children('title').contents()[0]['textContent'];
    // $(document).prop('title', (docTitle) ? docTitle : title);

    // gtag('event', 'page_view', {
    //     page_title: (docTitle) ? docTitle : title,
    //     page_location: '/#' + slides.attr('data-history'),
    //     page_path: '/#' + slides.attr('data-history')
    // })
});

/*======================================
page allocated js
=======================================*/
$url=window.location.href.substr(window.location.href.indexOf('=')+1);
if(parseInt($url)>1)
{
	swiper.slideTo(parseInt($url-1),300,function(){});
}
	
/*======================================
logo clickable js
=======================================*/
$(document).on('click','#ghar',function(){
	swiper.slideTo(0,300,function(){});
});

/*======================================
page linking js
=======================================*/
$(document).on('click','.indexLinktoPage',function(){
	$id=$(this).parents('.modalbox').attr('id');
	$('#'+$id).css('display','none');
	$pagenumber=parseInt($(this).attr('page-number'));
	swiper.slideTo($pagenumber,300,function(){});
});  
 
/*=================================================================
articles share pop up js
==================================================================*/   
$('.social-share-group-button').click(function(){
	// var id = $(this).parents('.swiper-slide').attr('id');
	var url = window.location.href;
	var fbShareUrl = 'https://facebook.com/sharer/sharer.php?u='+url;
	var twitterShareUrl = 'http://twitter.com/share?url='+url;
	var linkedinShareUrl = 'https://www.linkedin.com/shareArticle?mini=true&url='+url;
	var mailShareUrl = 'mailto:?subject=share&body='+url;
	// $('#pageUrl').val(url);
	$('.url-holder').html(url);
	$('.social-icon-facebook').attr('href', fbShareUrl);
	$('.social-icon-twitter').attr('href', twitterShareUrl);
	$('.social-icon-linkedin').attr('href', linkedinShareUrl);
	$('.social-icon-envelope').attr('href', mailShareUrl);
}); 

/*======================================
fancy box share
=======================================*/
$('[data-fancybox="p48"]').fancybox({
  	buttons : ['share', 'close'],
  	hash : false,
  	share: {
	  	url: function(instance, item) {
	    return (
	    	(!instance.currentHash && !(item.type === "inline" || item.type === "html") ? item.origSrc || item.src : false) || window.location
	    );

	},
	tpl:
	    '<div class="fancybox-share">' +
	    "<h1>{{SHARE}}</h1>" +
	    "<p>" +
	    '<a class="fancybox-share__button fancybox-share__button--fb" href="https://www.facebook.com/sharer/sharer.php?u={{url}}">' +
	    '<svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m287 456v-299c0-21 6-35 35-35h38v-63c-7-1-29-3-55-3-54 0-91 33-91 94v306m143-254h-205v72h196" /></svg>' +
 
	    "</a>" +
	    '<a class="fancybox-share__button fancybox-share__button--tw" href="https://twitter.com/intent/tweet?url={{url}}&text={{descr}}">' +
	    '<svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m456 133c-14 7-31 11-47 13 17-10 30-27 37-46-15 10-34 16-52 20-61-62-157-7-141 75-68-3-129-35-169-85-22 37-11 86 26 109-13 0-26-4-37-9 0 39 28 72 65 80-12 3-25 4-37 2 10 33 41 57 77 57-42 30-77 38-122 34 170 111 378-32 359-208 16-11 30-25 41-42z" /></svg>' +
 
	    "</a>" + 
	    '<a class="fancybox-share__buttonemail fancybox-share__button--pt" href="mailto:?subject=Check out &body=Check out {{url}}">' +
	   	'<i class="fas fa-envelope email"></i>' +
	    
	    "</a>" +
	    "</p>" +
	    '<a class="fancy-sharetext" href="{{url_raw}}" target="_blank"><input class="fancybox-share__input" type="text" value="{{url_raw}}" /></a>' +
	    "</div>"
  	}
}); 
 

$(document).on('click', 'a[href^="#"]', function (event) {
    event.preventDefault();

    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top-100
    }, 500);
});


// text t0 speech and fontsize
function stripHtml(html)
 {
	 let tmp = document.createElement("DIV");
	 tmp.innerHTML = html;
	 return tmp.textContent || tmp.innerText || "";
 }    
 $(document).on('click','#speaker-mission',function(){
	var id = $('.swiper-slide.swiper-slide-active').attr('id');
	var content = $('#'+id).html();
	let utter = new SpeechSynthesisUtterance();
	utter.lang = 'no-NO';
	const voices = speechSynthesis.getVoices();
	utter.text = "";
	utter.volume = 0.5;
	utter.pitch = 1.5
	utter.text = stripHtml(content) + ' slutten av innholdet.';
	window.speechSynthesis.speak(utter);
	$('#speaker-mission-off').css('display','block');
	$('#speaker-mission').css('display','none');
 });
 $(document).on('click','#speaker-mission-off',function(e){
	 e.preventDefault();
	window.speechSynthesis.cancel();
	$('#speaker-mission-off').css('display','none');
	$('#speaker-mission').css('display','block'); 
}); 

 function adjustFontSize(increase) {
	// var classAttr = ['.articles-contentsection',
	//  'h4',
	//  'h5',
	//  'h6',
	//  '.quate-box',
	//  '.images-caption',
	//  '.articles-subtitle',
	//  '.intro-text',
	//  '.lead',
	//  '.articles-textfotobox',
	//  '.textInfoBox p',
	//  'table',
	//  '.inner_container > p',
	// //  '.inner_container div',
	//  '.inner_container li',
	//  '.news-subtitle',
	//  '.person-ceontactinfo'];
	//  classAttr.forEach(ele => {
	// 	var fontSize = parseInt($(ele).css("font-size"));
	// 	var lineHeight = parseInt($(ele).css("line-height"));

	// 	fontSize = increase ? fontSize + 8 : fontSize - 8;
	// 	lineHeight = increase ? lineHeight + 8 : lineHeight - 8;

	// 	var style = {
	// 		'font-size': fontSize + "px",
	// 		'line-height': lineHeight + "px"
	// 	};
	// 	$(ele).css(style);
		$('body').toggleClass('largefont');
	//  });

    resizeSwiperContainer();
}

$('#large-fontsize, #small-fontsize').on('click', function (e) {
    e.preventDefault();
    adjustFontSize(this.id === 'large-fontsize');
    $('#large-fontsize, #small-fontsize').toggle();
});




$(document).on('click','.favorite', function(e) {
	e.preventDefault();
	$(".favorite-added-msg").css("visibility", "visible");  
	var swiperId = $('.swiper-slide.swiper-slide-active').attr('id');
	var magazineName = window.location.href.split('utgivelser')[0].split('https://dittmagasin.no/')[1].replace('/','');
	if(window.location.href.includes(magazineName)) {
		var id = localStorage.getItem(magazineName + '_release_id');
		var url = window.location.href;
		var token = localStorage.getItem(magazineName + 'token');
		// var postUrl = "https://dittmagasin.no/"+ localStorage.getItem('magazine_name') +"/api/set-favorite-release";
		var postUrl = window.location.href.split('utgivelser')[0] +"api/set-favorite-release";
		var imgSelector = $('.swiper-slide.swiper-slide-active .img-social');
		var titleSelector = $('.swiper-slide.swiper-slide-active title');
        var image = (imgSelector && imgSelector != undefined)? imgSelector[0].src:'';
		var title = (titleSelector && titleSelector != undefined)? titleSelector[0].innerText:'';
		var settings = {
			"url": postUrl,
			"method": "POST",
			"async": "false",
			"data": {
				url: url,
				image: image,
				token: token,
				title: title,
				id: id,
			}
		};

		$.ajax(settings).done(function (response) {
			if(JSON.parse(response).status == 'ok'){
				$(".favorite-added-msg.msg-success").addClass("favorite-msg-show");  
				$(".favorite-added-msg.msg-error").removeClass("favorite-msg-show");  
				$(".favorite-msg-container h2.success_message").html(JSON.parse(response).message);  
				setTimeout(function() {
					$(".favorite-added-msg.msg-error").removeClass("favorite-msg-show"); 
					$(".favorite-added-msg.msg-success").removeClass("favorite-msg-show"); 
				}, 1500);
			} else {
				$(".favorite-added-msg.msg-success").removeClass("favorite-msg-show"); 
				$(".favorite-added-msg.msg-error").addClass("favorite-msg-show");     
				$(".favorite-msg-container h2.error_message").html(JSON.parse(response).message);  
				setTimeout(function() {
					$(".favorite-added-msg.msg-error").removeClass("favorite-msg-show"); 
					$(".favorite-added-msg.msg-success").removeClass("favorite-msg-show"); 
				}, 1500);
			}
		});
	}
})

$('.mag-tool-btn.home').on('click', function(e){
    e.preventDefault();
    var url = window.location.href.split('utgivelser')[0];
    window.location.href = window.location.href.split('utgivelser')[0];
})

$(document).on('load','.favorite', function(e) {
	e.preventDefault();
	var magazineName = window.location.href.split('utgivelser')[0].split('https://dittmagasin.no/')[1].replace('/','');
	if(window.location.href.includes(magazineName) && localStorage.getItem(magazineName + 'token')) {
		$(".mag-tool-btn.favorite").css("display", "none"); 
	}
})

$(window).on("beforeunload", function() { 
    window.speechSynthesis.cancel();
});
$(window).on("beforeunload", function() {
            window.speechSynthesis.cancel();
        });

$('.mag-tool-btn.home').on('click', function(e) {
     window.speechSynthesis.cancel();
});

swiper.on('slideChange', function () {
   window.speechSynthesis.cancel();
   $('#speaker-mission-off').css('display','none');
   $('#speaker-mission').css('display','block'); 
}); 